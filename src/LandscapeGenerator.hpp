#pragma once

#include <Urho3D/Scene/Scene.h>
#include <Urho3D/Graphics/Model.h>
#include <Urho3D/Graphics/GraphicsDefs.h>
#include <Urho3D/Graphics/IndexBuffer.h>
#include <Urho3D/Graphics/VertexBuffer.h>
#include <Urho3D/Math/Vector2.h>
#include <Urho3D/Math/Vector3.h>
#include <Urho3D/Container/Str.h>
#include <Urho3D/IO/Log.h>
#include <vector>
#include <memory>
#include <stdexcept>

#include <FastNoise.h>


#include "OverwritableLimitedContainer.hpp"
#include "Utils/SimpleVector.hpp"



//#define URHOWORLDS_NOIZES_CACHE_PRINT_STATS

struct NoizesCache {
    NoizesCache()
        : cache() {
    }

#ifdef URHOWORLDS_NOIZES_CACHE_PRINT_STATS
    ~NoizesCache() {
        URHO3D_LOGINFO("hits: " + Urho3D::String(hits));
        URHO3D_LOGINFO("misses: " + Urho3D::String(misses));
    }
#endif // URHOWORLDS_NOIZES_CACHE_PRINT_STATS

    struct cache_element {
        int coord_x;
        int coord_y;
        float coord_z;
    };

    OverwritableArray<cache_element, 2> cache;

#ifdef URHOWORLDS_NOIZES_CACHE_PRINT_STATS
    size_t hits = 0;
    size_t misses = 0;
#endif // URHOWORLDS_NOIZES_CACHE_PRINT_STATS
};


class Noizes {
public:
    Noizes ();

    float get_noize (int x, int y, NoizesCache *cache = nullptr) const;
    float get_noize_cached(const int x, const int y, NoizesCache* cache) const;

    float get_color_noize1 (float x, float y) const;
    float get_color_noize2 (float x, float y) const;
    float get_color_noize3 (float x, float y) const;
    float get_forest_noize (const float x, const float y) const;

    FastNoise color1, color2, color3, forest, noize_color;
    FastNoise _noize1;
    FastNoise _noize2;
    FastNoise _noize3;

    float _ampl1 = 1.f;
    float _ampl2 = 1.f;
    float _ampl3 = 1.f;

    float _exp1 = 1.f;
    float _exp2 = 1.f;
    float _exp3 = 1.f;

    float _color_noise_amt = .1f;
    float _color_dither_amt = .1f;
};


struct UrhoworldsIndexElement {
  unsigned short v1, v2, v3;
};

struct UrhoworldsColorVertexElement {
  unsigned char r, g, b, a;
};


struct UrhoworldsLandscapeVertexElement {
  Urho3D::Vector3 coord;
  Urho3D::Vector3 normal;
  UrhoworldsColorVertexElement color;
  static ea::vector<Urho3D::VertexElement>
  getVertexDescription() {
      ea::vector<Urho3D::VertexElement> descr;
      descr.push_back(Urho3D::VertexElement(Urho3D::TYPE_VECTOR3, Urho3D::SEM_POSITION));
      descr.push_back(Urho3D::VertexElement(Urho3D::TYPE_VECTOR3, Urho3D::SEM_NORMAL));
      descr.push_back(Urho3D::VertexElement(Urho3D::TYPE_UBYTE4, Urho3D::SEM_COLOR));
      return descr;
  }
};

struct UrhoworldsTexturedVertexElement {
    Urho3D::Vector3 coord;
    Urho3D::Vector3 normal;
    Urho3D::Vector2 texcoord;
    static ea::vector<Urho3D::VertexElement>
    getVertexDescription() {
        ea::vector<Urho3D::VertexElement> descr;
        descr.push_back(Urho3D::VertexElement(Urho3D::TYPE_VECTOR3, Urho3D::SEM_POSITION));
        descr.push_back(Urho3D::VertexElement(Urho3D::TYPE_VECTOR3, Urho3D::SEM_NORMAL));
        descr.push_back(Urho3D::VertexElement(Urho3D::TYPE_VECTOR2, Urho3D::SEM_TEXCOORD));
        return descr;
    }
};

struct UrhoworldsTexturedNormalMappedVertexElement {
    Urho3D::Vector3 coord;
    Urho3D::Vector3 normal;
    Urho3D::Vector3 tangent;
    Urho3D::Vector2 texcoord;
    static ea::vector<Urho3D::VertexElement>
    getVertexDescription() {
        ea::vector<Urho3D::VertexElement> descr;
        descr.push_back(Urho3D::VertexElement(Urho3D::TYPE_VECTOR3, Urho3D::SEM_POSITION));
        descr.push_back(Urho3D::VertexElement(Urho3D::TYPE_VECTOR3, Urho3D::SEM_NORMAL));
        descr.push_back(Urho3D::VertexElement(Urho3D::TYPE_VECTOR3, Urho3D::SEM_TANGENT));
        descr.push_back(Urho3D::VertexElement(Urho3D::TYPE_VECTOR2, Urho3D::SEM_TEXCOORD));
        return descr;
    }
};


class GeometryConstructionData {
public:
    virtual ea::vector<Urho3D::VertexElement>
    getVertexDescription () const = 0;

    virtual void *getVertexData () const = 0;
    virtual unsigned getVertexDataSize () const = 0;

    virtual void *getIndexData () const = 0;
    virtual unsigned getIndexDataSize () const = 0;

    virtual ~GeometryConstructionData () {}
};

template <typename VertexElementType>
struct UrhoworldsGeometryConstructionData {
    UrhoworldsGeometryConstructionData () {
    }
    UrhoworldsGeometryConstructionData (Urho3D::Context *pContext,
                                        const unsigned short pIndexBufferSize,
                                        const unsigned short pVertexBufferSize)
        : indexBuffer (new Urho3D::IndexBuffer(pContext)),
          vertexBuffer (new Urho3D::VertexBuffer(pContext)) {
        indexBuffer->SetShadowed(true);
        indexBuffer->SetSize(pIndexBufferSize, false);

        vertexBuffer->SetShadowed(true);
        vertexBuffer->SetSize(pVertexBufferSize, getVertexDescription());

    }
    ea::vector<Urho3D::VertexElement>
    getVertexDescription () const {
        return VertexElementType::getVertexDescription();
    }

    Urho3D::SharedPtr<Urho3D::IndexBuffer> indexBuffer;
    Urho3D::SharedPtr<Urho3D::VertexBuffer> vertexBuffer;
};

// TODO4: remove? switch to UrhoworldsGeometryConstructionData
template <typename VertexElementType>
class UrhoworldsPlaneConstructionDataBase : public GeometryConstructionData {
public:
    ea::vector<Urho3D::VertexElement>
    getVertexDescription () const final override {
        return VertexElementType::getVertexDescription();
    }

    void *getVertexData () const final override { return vertex_data->data(); }
    unsigned getVertexDataSize () const override {
        return (unsigned) vertex_data->size();
    }

    void *getIndexData () const final override { return index_data->data(); }
    unsigned getIndexDataSize () const final override {
        constexpr size_t sz =
            sizeof(UrhoworldsIndexElement) / sizeof(UrhoworldsIndexElement::v1);
        return (unsigned) (index_data->size() * sz);
    }

    SimpleVector<UrhoworldsIndexElement> *index_data;
    SimpleVector<VertexElementType> *vertex_data;
};

template <typename VertexElementType>
class UrhoworldsPlaneConstructionData final
    : public UrhoworldsPlaneConstructionDataBase<VertexElementType> {
public:
    UrhoworldsPlaneConstructionData ()
        : index_data_storage
              (std::make_unique<SimpleVector<UrhoworldsIndexElement>>()),
          vertex_data_storage
              (std::make_unique<SimpleVector<VertexElementType>>()) {
        UrhoworldsPlaneConstructionDataBase<VertexElementType>::index_data
            = index_data_storage.get();
        UrhoworldsPlaneConstructionDataBase<VertexElementType>::vertex_data
            = vertex_data_storage.get();
    }

    std::unique_ptr<SimpleVector<UrhoworldsIndexElement>> index_data_storage;
    std::unique_ptr<SimpleVector<VertexElementType>> vertex_data_storage;
};


void add_plane (Urho3D::Scene *pScene, Urho3D::Context *pContext, int res_x, int res_y);
Urho3D::SharedPtr<Urho3D::Model> create_plane (Urho3D::Context *pContext,
                                               int res_x,
                                               int res_y,
                                               Urho3D::IntVector2 pos);

template<typename T>
T create_lod_1_plane_get_outer_circle_num_vertex
(const T res_x, const T res_y) {
    return (res_x + 1) * 2 + (res_y - 1) * 2;
}

template<typename T>
T create_lod_1_plane_get_inner_num_vertex
(const T res_x, const T res_y) {
    return (res_x / 2) * (res_y / 2);
}

template<typename T>
T create_lod_1_plane_get_num_index
(const T res_x, const T res_y) {
    const T half_x = res_x / 2 - 1;
    const T half_y = res_y / 2 - 1;

    return 2                   // top left
        + 3 * half_x           // top stripe
        + 3                    // top right
        + 3                    // bottom left
        + 3 * half_x           // bottom stripe
        + 4                    // bottom right
        + 3 * half_y           // left stripe
        + 3 * half_y           // right stripe
        + 2 * half_x * half_y; // inner part
}


void create_lod_0_plane (const Noizes& nz,
                         unsigned char *const pVertexData,
                         unsigned char *const pIndexData,
                         float &rHeightMin,
                         float &rHeightMax,
                         float xy_scale,
                         unsigned res_x,
                         unsigned res_y,
                         Urho3D::IntVector2 pos);

void create_lod_1_plane
    (const Noizes &nz,
     SimpleVector<UrhoworldsLandscapeVertexElement> *const pVertexData,
     SimpleVector<UrhoworldsIndexElement> *const pIndexData,
     unsigned short res_x,
     unsigned short res_y,
     Urho3D::IntVector2 pos);

Urho3D::SharedPtr<Urho3D::Model>
create_2lod_plane (Urho3D::Context* pContext,
                   const GeometryConstructionData &lod0,
                   const GeometryConstructionData &lod1,
                   const Urho3D::Vector3 &bounds0,
                   const Urho3D::Vector3 &bounds1);

Urho3D::SharedPtr<Urho3D::Model>
create_1lod_plane (Urho3D::Context* pContext,
                   const GeometryConstructionData &lod0,
                   const Urho3D::Vector3 &bounds0,
                   const Urho3D::Vector3 &bounds1,
                   const Urho3D::PrimitiveType pType = Urho3D::TRIANGLE_LIST);


Urho3D::SharedPtr<Urho3D::Model> create_1lod_plane (
    Urho3D::Context* pContext,
    const UrhoworldsGeometryConstructionData<UrhoworldsLandscapeVertexElement> &lod0,
    const Urho3D::Vector3 &bounds0,
    const Urho3D::Vector3 &bounds1,
    const Urho3D::PrimitiveType pType = Urho3D::TRIANGLE_LIST);

Urho3D::SharedPtr<Urho3D::Model> create_2lod_plane (
    Urho3D::Context* pContext,
    const UrhoworldsGeometryConstructionData<UrhoworldsLandscapeVertexElement> &lod0,
    const UrhoworldsGeometryConstructionData<UrhoworldsLandscapeVertexElement> &lod1,
    const Urho3D::Vector3 &bounds0,
    const Urho3D::Vector3 &bounds1);

