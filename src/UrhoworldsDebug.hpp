#pragma once


#include <cstddef>


namespace UrhoworldsDebug {

void record_simple_vector_reallocations ();
size_t get_simple_vector_reallocations ();

}
