// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com
#include "LandscapeGenerator.hpp"

#include <Urho3D/Core/Context.h>
#include <Urho3D/Graphics/Model.h>
#include <Urho3D/Graphics/VertexBuffer.h>
#include <Urho3D/Graphics/IndexBuffer.h>
#include <Urho3D/Graphics/Geometry.h>
#include <Urho3D/Graphics/Material.h>
#include <Urho3D/Graphics/StaticModel.h>
#include <Urho3D/Core/Profiler.h>

#include <Urho3D/IO/Log.h>

#include <vector>
#include <utility>
#include <tuple>
#include <algorithm>
#include <array>

#include <random>
#include <stdexcept>

#include "VcppBits/MathUtils/MathUtils.hpp"

using namespace VcppBits::MathUtils;
using namespace Urho3D;

    Noizes::Noizes () {
        color1.SetSeed(8077);
        color1.SetFrequency(0.35f);
        color2.SetSeed(9283);
        color2.SetFrequency(0.31f);
        color3.SetSeed(1239);
        color3.SetFrequency(0.37f);

        forest.SetNoiseType(FastNoise::SimplexFractal);
        forest.SetFrequency(0.005f);
        forest.SetFractalOctaves(2);
        forest.SetFractalGain(1);

        // TODO2 hardcode:
        noize_color.SetSeed(1239);
        noize_color.SetFrequency(0.37f);
    }

    float Noizes::get_noize (const int x,
                             const int y,
                             NoizesCache *cache) const {
        float n1 = _noize1.GetNoise(float(x), float(y));
        float n2 = _noize2.GetNoise(float(x), float(y));
        float n3 = _noize3.GetNoise(float(x), float(y));

        const float ret = pow(n1, _exp1) * _ampl1
            + pow(n2, _exp2) * _ampl2
            + pow(n3, _exp3) * _ampl3;

        if (cache) {
            cache->cache.push_back({x, y, ret});
        }

        return ret;
    }

    float Noizes::get_noize_cached (const int x,
                                    const int y,
                                    NoizesCache *cache) const {

        if (cache) {
            for (auto el: cache->cache.getUnderlyingContainer()) {
                if (el.coord_x == x && el.coord_y == y) {
#ifdef URHOWORLDS_NOIZES_CACHE_PRINT_STATS
                    ++(cache->hits);
#endif // URHOWORLDS_NOIZES_CACHE_PRINT_STATS
                    return el.coord_z;
                }
            }
#ifdef URHOWORLDS_NOIZES_CACHE_PRINT_STATS
            ++(cache->misses);
#endif // URHOWORLDS_NOIZES_CACHE_PRINT_STATS
        }

        return get_noize(x, y);
    }

    float  Noizes::get_color_noize1 (float x, float y) const {
        return (color1.GetNoise(x, y) + 1.0f) / 2.0f;
    }

    float  Noizes::get_color_noize2 (float x, float y) const {
        return (color2.GetNoise(x, y) + 1.0f) / 2.0f;
    }

    float Noizes::get_color_noize3 (float x, float y) const {
        return (color3.GetNoise(x, y) + 1.0f) / 2.0f;
    }

    float Noizes::get_forest_noize (const float x, const float y) const {
        return forest.GetNoise(x, y);
    }








class GridPlaneBuilder {
public:
  GridPlaneBuilder (unsigned res_x,
                    unsigned res_y,
                    unsigned char *const pVertexData,
                    unsigned char *const pIndexData) :
    _resX (res_x),
    _resY (res_y),
    _indexData(reinterpret_cast<UrhoworldsIndexElement*>(pIndexData)),
    _vertexData(reinterpret_cast<UrhoworldsLandscapeVertexElement*>(pVertexData)) {

    size_t pos = 0;

    for (unsigned i = 0; i < _resY - 1; ++i) {
        for (unsigned j = 0; j < _resX - 1; ++j) {
            unsigned short point0_num = (i * res_x) + j;
            unsigned short point1_num = point0_num + _resX;
            unsigned short point2_num = point0_num + 1;

            _indexData[pos++] = {point0_num, point1_num, point2_num};

            unsigned short point5_num = point1_num + 1;

            _indexData[pos++] = {point2_num, point1_num, point5_num};
          }
    }
  }

  void addPoint(const UrhoworldsLandscapeVertexElement &el) {
	  _vertexData[_pos++] = el;
  }

  unsigned short * getIndexData() {
    return reinterpret_cast<unsigned short*>(_indexData);
  }

  float * getVertexData() {
    return reinterpret_cast<float*>(_vertexData);
  }

  UrhoworldsLandscapeVertexElement &getVertex (const size_t pNum) {
    return _vertexData[pNum];
  }

  UrhoworldsIndexElement &getIndex  (const size_t pNum) {
    return _indexData[pNum];
  }

private:
  const unsigned _resX, _resY;

  UrhoworldsIndexElement *const _indexData;
  UrhoworldsLandscapeVertexElement *const _vertexData;
  size_t _pos = 0;
};

// TODO 8: unit test this
// packs 8 floats of 0.0..1.0 into 4 bytes, where each byte contains 2 of 0..15
//
// for example: {0.1, 0.5, 1.0, 0.0, ...} => { 1, 8, 15, ...}
//                ... or actually .. => { 8 | 1 << 4, 0 | 15 << 4 ...} 
UrhoworldsColorVertexElement encode_stuff2 (const float *k) {
    UrhoworldsColorVertexElement ret;

    std::array<unsigned char*, 4> ret_ptrs = { &ret.r, &ret.g, &ret.b, &ret.a };

    for (size_t i = 0; i < ret_ptrs.size(); ++i) {
        const unsigned char k1_u = (unsigned char)(k[i * 2] * 15.f);
        const unsigned char k2_u = (unsigned char)(k[i * 2 + 1] * 15.f);

        *(ret_ptrs[i]) = k2_u | (k1_u << 4);
    }

    return ret;
}

//
//     [  col1 ]   [  col2 ]      [ col 3 ][noize ch sel]
// [ *  *  *  *  *  *  *  * ] [ *  *  *  *  *  *  *  *  ]
//
//  [ col1 amt ][ col2 amt ]   [      8 spare         ]
// [ *  *  *  *  *  *  *  * ] [ *  *  *  *  *  *  *  *  ]


UrhoworldsColorVertexElement encode_stuff3 (const unsigned char tex1_num,
                                            const float tex1_amt,
                                            const unsigned char tex2_num,
                                            const float tex2_amt,
                                            const unsigned char tex3_num,
                                            // const float tex3_amt, --
                                            // calculated on gpu
                                            const float noize_channel_sel) {
    UrhoworldsColorVertexElement ret = { 0, 0, 0, 0 };

    ret.r = tex2_num | (tex1_num << 4);
    // TODO: bad things happen if it becomes more than 15...
    unsigned char noize_sel = (unsigned char)(clamp(noize_channel_sel, 0.f, 1.f) * 15.f);
    ret.g = noize_sel | (tex3_num << 4);
    unsigned char tex1_amt_ = (unsigned char)(tex1_amt * 15.f);
    unsigned char tex2_amt_ = (unsigned char)(tex2_amt * 15.f);
    ret.b = tex2_amt_ | (tex1_amt_ << 4);

    return ret;
}


UrhoworldsLandscapeVertexElement get_vert (const Noizes &n,
                                           IntVector2 wp /*world pos*/,
                                           const int x,
                                           const int y,
                                           NoizesCache *cache = nullptr) {
  const int wx = x + wp.x_;
	const int wy = y + wp.y_;

  float height = n.get_noize_cached(wx, wy, cache); // supposed to be from -1 to 1, but some
                                      // settings may cause significant
                                      // deviation from that

  auto R = n.get_noize(wx, wy + 1);
  auto L = n.get_noize(wx, wy - 1);
  auto B = n.get_noize_cached(wx - 1, wy, cache);
  auto T = n.get_noize(wx + 1, wy, cache);
  // https://stackoverflow.com/questions/49640250/calculate-normals-from-heightmap
  const Vector3 normal = Vector3(2.f * (B - T), 4.f, 2.f * (L - R)).Normalized();

  const float dither_amt = n._color_dither_amt;
  float dz = n.get_color_noize1(wx, wy) * dither_amt; // 0..1
  dz = 1.f + dz - dither_amt;
  float h = clamp(height / 250.f * dz, 0.f, 1.f);

  const float nz = n.get_color_noize2(wx, wy);
  const float nz_amt = n._color_noise_amt;

  constexpr size_t num_tex = 7;

  // so that both lowest and highest points have more mixing going in them
  h = clamp(h, 0.5f/float(num_tex), 1.f);

  h = clamp(h + (nz * 2.f - 1.f) * nz_amt, 0.f, 1.f);


  const float overflow = 0.4f;

  auto calc_tex_amount =
      [&](unsigned char nth) {
          return std::max(1.f - std::abs(float(nth) - h * (num_tex - 1)) * (1 - overflow),
                          0.f);
      };

  const unsigned char nth = (unsigned char)(std::round(h * (num_tex - 1)));


  // main k



  float tex_amt = calc_tex_amount(nth);

  float prev_amt;
  unsigned char prev_tex;
  float next_amt;
  unsigned char next_tex;
  // curr_tex is 'nth'
  // curr amt is k

  if (nth >= 1) { // take previous texture and calc it's amount 
      prev_tex = nth - 1;
  } else { // or if nth == 0, take texture number 2 as our "previous"
      prev_tex = nth + 2;
  }
  prev_amt = calc_tex_amount(prev_tex);

  if (size_t(nth + 1) < num_tex) { // if nth is not last, take next texture
      next_tex = nth + 1;
  } else { // or if nth = num_tex, take (nth-2)
      next_tex = nth - 2;
  }
  next_amt = calc_tex_amount(next_tex);

  const float sum = tex_amt + prev_amt + next_amt;
  tex_amt /= sum;
  prev_amt /= sum;
  next_amt /= sum;

  const float noize = (n.noize_color.GetNoise(wx, wy) + 1.f) / 2.f;


  return { Vector3(x, height, y),
           normal,
           encode_stuff3(nth, tex_amt, prev_tex, prev_amt, next_tex, noize)
  };
}

UrhoworldsLandscapeVertexElement get_ivert (const Noizes &n,
                                   IntVector2 wp /*world pos*/,
                                   const int x,
                                            const int y) {
    return get_vert(n, wp, static_cast<float>(x), static_cast<float>(y));
}


void create_lod_0_plane (const Noizes &nz,
                         unsigned char *const pVertexData,
                         unsigned char *const pIndexData,
                         float &rHeightMin,
                         float &rHeightMax,
                         float xy_scaling,
                         unsigned res_x,
                         unsigned res_y,
                         IntVector2 pos) {
  rHeightMin = 0.f;
  rHeightMax = 0.f;
  GridPlaneBuilder m (res_x + 1,
                      res_y + 1,
                      pVertexData,
                      pIndexData);

  NoizesCache cache;
  cache.cache.getUnderlyingContainer()[0] = { std::numeric_limits<int>::max(),
                                            std::numeric_limits<int>::max(),
                                            std::numeric_limits<float>::infinity() };
  cache.cache.getUnderlyingContainer()[1] = { std::numeric_limits<int>::max(),
                                            std::numeric_limits<int>::max(),
                                            std::numeric_limits<float>::infinity() };
  for (unsigned i = 0; i <= res_y; ++i) {
    for (unsigned j = 0; j <= res_x; ++j) {
      UrhoworldsLandscapeVertexElement vert;
      if (int(xy_scaling) == 1) {
          vert = get_vert(nz, pos, j * xy_scaling, i * xy_scaling, &cache);
      } else {
          vert = get_vert(nz, pos, j * xy_scaling, i * xy_scaling);
      }
      rHeightMin = std::min(vert.coord.y_, rHeightMin);
      rHeightMax = std::max(vert.coord.y_, rHeightMax);
      m.addPoint(vert);
    }
  }
}

void create_lod_1_plane
    (const Noizes &nz,
     SimpleVector<UrhoworldsLandscapeVertexElement> *const pVertexData,
     SimpleVector<UrhoworldsIndexElement> *const pIndexData,
     unsigned short res_x,
     unsigned short res_y,
     IntVector2 pos) {

  SimpleVector<UrhoworldsIndexElement> &index_data = *pIndexData;
  SimpleVector<UrhoworldsLandscapeVertexElement> &vertex_data = *pVertexData;

  if (res_x % 2 != 1 || res_y % 2 != 1) {
    throw std::runtime_error("grid plane resolutions must be odd");
  }

  const auto outer_circle_vertex_num =
      create_lod_1_plane_get_outer_circle_num_vertex(res_x, res_y);
  const auto inner_vertex_num =
      create_lod_1_plane_get_inner_num_vertex(res_x, res_y);
  const auto total_vertex_num = outer_circle_vertex_num + inner_vertex_num;

  // top row
  for (int i = 0; i <= res_x; ++i) {
    vertex_data.push_back(get_ivert(nz, pos, i, 0));
  }

  // mid rows
  int ii = 1;
  for (int i = res_x + 1; i <= res_x + (res_y - 1) * 2; i+=2) {
	  vertex_data.push_back(get_ivert(nz, pos, 0, ii));
    vertex_data.push_back(get_ivert(nz, pos, res_x, ii));
    ++ii;
  }

  // bottom row
  for (int i = 0; i <= res_x; ++i) {
    vertex_data.push_back(get_ivert(nz, pos, i, res_y));
  }

  // inner area
  for (int i = 0; i < res_y / 2; ++i)  {
    const float pos_y = 1.5f + i * 2.f;
    for (int j = 0; j < res_x / 2; ++j) {
      const float pos_x = 1.5f + j * 2.f;
      vertex_data.push_back(get_vert(nz, pos, pos_x, pos_y));
    }
  }

  // top left
  index_data.push_back({0,(unsigned short) (res_x + 1), 1});
  index_data.push_back({1, (unsigned short)(res_x + 1), (unsigned short) outer_circle_vertex_num});

  // top stripe
  for (int i = 0; i < res_x / 2 - 1; ++i) {
    const unsigned short inner_vert = i + outer_circle_vertex_num;
    const unsigned short top_left = i * 2 + 1;
    const unsigned short top_right = top_left + 1;

    index_data.push_back({top_left, inner_vert, top_right});

    const unsigned short inner_next = inner_vert + 1;
    const unsigned short top_next = top_right + 1;

    index_data.push_back({top_right, inner_vert, inner_next});
    index_data.push_back({top_right, inner_next, top_next});
  }

  // top right
  {
    const unsigned short inner = res_x / 2 - 1 + outer_circle_vertex_num;
    const unsigned short top_left = res_x - 2;
    const unsigned short top_right = res_x - 1;
    index_data.push_back({top_left, inner, top_right});

    const unsigned short top_rightmost = res_x;
    const unsigned short next_rightmost = res_x + 2;

    index_data.push_back({top_right, inner, next_rightmost});
    index_data.push_back({top_right, next_rightmost, top_rightmost});
  }

  // bottom left
  {
    const unsigned short inner = total_vertex_num - res_x / 2;
    const unsigned short top_left = res_x + 1 + (res_y - 2) * 2;
    const unsigned short bottom_left = top_left + 2;
    const unsigned short bottom_right = bottom_left + 1;
    const unsigned short left_upmost = top_left - 2;
    index_data.push_back({top_left, bottom_left, bottom_right});
    index_data.push_back({inner, top_left, bottom_right});
    index_data.push_back({left_upmost, top_left, inner});
  }

  // bottom stripe
  for (int i = 0; i < res_x / 2 - 1; ++i) {
    const unsigned short inner = total_vertex_num - res_x / 2 + i;
    const unsigned short bottom_left = outer_circle_vertex_num - res_x + i * 2;
    const unsigned short bottom_right = bottom_left + 1;
    index_data.push_back({inner, bottom_left, bottom_right});

    const unsigned short bottom_right_next = bottom_right + 1;
    const unsigned short inner_next = inner + 1;
    index_data.push_back({inner, bottom_right, bottom_right_next });
    index_data.push_back({inner, bottom_right_next, inner_next });
  }

  // bottom right
  {
    const unsigned short inner = total_vertex_num - 1;
    const unsigned short bottom_left = outer_circle_vertex_num - 2;
    const unsigned short bottom_right = outer_circle_vertex_num - 1;
    const unsigned short top_right = res_x + 2 + (res_y - 2) * 2;
    const unsigned short leftmost = bottom_left - 1;
    const unsigned short upmost = top_right - 2;

    index_data.push_back({inner, leftmost, bottom_left});
    index_data.push_back({inner, bottom_left, top_right});
    index_data.push_back({top_right, bottom_left, bottom_right});
    index_data.push_back({upmost, inner, top_right});
  }

  // left stripe
  constexpr unsigned short four = 4;
  for (int i = 0; i < res_y / 2 - 1; ++i) {
    const unsigned short inner = outer_circle_vertex_num + i * (res_x / 2);
    const unsigned short inner_next = inner + (res_x / 2);
    const unsigned short bottom_left = res_x + 1 + i * four;
    const unsigned short bottom_right = bottom_left + 2;
    const unsigned short bottom_right_next = bottom_right + 2;

    index_data.push_back({inner, bottom_left, bottom_right});
    index_data.push_back({inner, bottom_right, bottom_right_next});
    index_data.push_back({inner, bottom_right_next, inner_next});
  }

  // right stripe
  for (int i = 0; i < res_y / 2 - 1; ++i) {
    const unsigned short inner = res_x / 2 - 1 + outer_circle_vertex_num + i * (res_x / 2);
    const unsigned short inner_next = inner + (res_x / 2);
    const unsigned short bottom_right = res_x + 2 + i * four;
    const unsigned short bottom_left = bottom_right + 2;
    const unsigned short bottom_left_next = bottom_left + 2;

    index_data.push_back({inner, bottom_left, bottom_right});
    index_data.push_back({inner, inner_next, bottom_left_next});
    index_data.push_back({inner, bottom_left_next, bottom_left});
  }


  // inner part
  int jj = outer_circle_vertex_num;
  for (int i = 0; i < res_x / 2 - 1; ++i) {
    for (int j = 0; j < res_y / 2 - 1; ++j) {
      unsigned short point0_num = (i * (res_x / 2)) + j + jj;
      unsigned short point1_num = point0_num + (res_x / 2);
      unsigned short point2_num = point0_num + 1;

      index_data.push_back({point0_num, point1_num, point2_num});
      unsigned short point5_num = point1_num + 1;

      index_data.push_back({point2_num, point1_num, point5_num});
    }
  }


  //  unsigned num_indices = static_cast<unsigned>(index_data.size() * 3);
  //  unsigned num_verts = static_cast<unsigned>(vertex_data.size());
}



std::tuple<SharedPtr<Geometry>,
           SharedPtr<VertexBuffer>,
           SharedPtr<IndexBuffer>>
create_geometry (Context* pContext,
                 const GeometryConstructionData &data,
                 const Urho3D::PrimitiveType pType = Urho3D::TRIANGLE_LIST) {
  SharedPtr<Geometry> geom(new Geometry(pContext));
  SharedPtr<VertexBuffer> vb(new VertexBuffer(pContext, false));
  SharedPtr<IndexBuffer> ib(new IndexBuffer(pContext, false));

  // Shadowed buffer needed for raycasts to work, and so that data can be
  // automatically restored on device loss
  vb->SetShadowed(true);
  // We could use the "legacy" element bitmask to define elements for more
  // compact code, but let's demonstrate defining the vertex elements explicitly
  // to allow any element types and order

  //  PODVector<VertexElement> elements;
  vb->SetSize(data.getVertexDataSize(), data.getVertexDescription());
  vb->SetData(data.getVertexData());

  ib->SetShadowed(true);
  ib->SetSize(data.getIndexDataSize(), false);
  ib->SetData(data.getIndexData());

  geom->SetVertexBuffer(0, vb);
  geom->SetIndexBuffer(ib);
  geom->SetDrawRange(pType, 0, data.getIndexDataSize());

  return { geom, vb, ib};
}

SharedPtr<Model> create_2lod_plane (Context* pContext,
                                    const GeometryConstructionData &lod0,
                                    const GeometryConstructionData &lod1,
                                    const Vector3 &bounds0,
                                    const Vector3 &bounds1) {
  // bounds0: 0, -1, 0
  // bounds1: Vector3(static_cast<float>(res_x), 1, static_cast<float>(res_y)))

  SharedPtr<Model> fromScratchModel(new Model(pContext));
  ea::vector<SharedPtr<VertexBuffer> > vertexBuffers;
  ea::vector<SharedPtr<IndexBuffer> > indexBuffers;

  ea::vector<unsigned> morphRangeStarts;
  ea::vector<unsigned> morphRangeCounts;
  morphRangeStarts.push_back(0);
  morphRangeCounts.push_back(0);

  fromScratchModel->SetNumGeometries(1);
  fromScratchModel->SetNumGeometryLodLevels(0, 2);

  SharedPtr<Geometry> geom;
  SharedPtr<VertexBuffer> vb;
  SharedPtr<IndexBuffer> ib;

  std::tie(geom, vb, ib) = create_geometry(pContext, lod0);
  vertexBuffers.push_back(vb);
  indexBuffers.push_back(ib);
  geom->SetLodDistance(5.f);
  fromScratchModel->SetGeometry(0, 0, geom);

  std::tie(geom, vb, ib) = create_geometry(pContext, lod1);
  vertexBuffers.push_back(vb);
  indexBuffers.push_back(ib);
  geom->SetLodDistance(10.f);
  fromScratchModel->SetGeometry(0, 1, geom);

  fromScratchModel->SetBoundingBox(BoundingBox(bounds0, bounds1));
  fromScratchModel->SetVertexBuffers(vertexBuffers, morphRangeStarts, morphRangeCounts);
  fromScratchModel->SetIndexBuffers(indexBuffers);

  return fromScratchModel;
}

SharedPtr<Model> create_1lod_plane (Context* pContext,
                                    const GeometryConstructionData &lod0,
                                    const Vector3 &bounds0,
                                    const Vector3 &bounds1,
                                    const Urho3D::PrimitiveType pType
                                        /* = Urho3D::TriangleList */) {
  SharedPtr<Model> fromScratchModel(new Model(pContext));
  ea::vector<SharedPtr<VertexBuffer> > vertexBuffers;
  ea::vector<SharedPtr<IndexBuffer> > indexBuffers;

  ea::vector<unsigned> morphRangeStarts;
  ea::vector<unsigned> morphRangeCounts;
  morphRangeStarts.push_back(0);
  morphRangeCounts.push_back(0);

  fromScratchModel->SetNumGeometries(1);
  fromScratchModel->SetNumGeometryLodLevels(0, 1);

  SharedPtr<Geometry> geom;
  SharedPtr<VertexBuffer> vb;
  SharedPtr<IndexBuffer> ib;

  std::tie(geom, vb, ib) = create_geometry(pContext, lod0, pType);
  vertexBuffers.push_back(vb);
  indexBuffers.push_back(ib);
  geom->SetLodDistance(5.f);
  fromScratchModel->SetGeometry(0, 0, geom);


  fromScratchModel->SetBoundingBox(BoundingBox(bounds0, bounds1));
  fromScratchModel->SetVertexBuffers(vertexBuffers, morphRangeStarts, morphRangeCounts);
  fromScratchModel->SetIndexBuffers(indexBuffers);

  return fromScratchModel;
}


// NEW STUFF:

SharedPtr<Model> create_2lod_plane (
    Context* pContext,
    const UrhoworldsGeometryConstructionData<UrhoworldsLandscapeVertexElement> &lod0,
    const UrhoworldsGeometryConstructionData<UrhoworldsLandscapeVertexElement> &lod1,
    const Vector3 &bounds0,
    const Vector3 &bounds1) {
  // bounds0: 0, -1, 0
  // bounds1: Vector3(static_cast<float>(res_x), 1, static_cast<float>(res_y)))

  SharedPtr<Model> fromScratchModel(new Model(pContext));
  ea::vector<SharedPtr<VertexBuffer> > vertexBuffers;
  ea::vector<SharedPtr<IndexBuffer> > indexBuffers;

  ea::vector<unsigned> morphRangeStarts;
  ea::vector<unsigned> morphRangeCounts;
  morphRangeStarts.push_back(0);
  morphRangeCounts.push_back(0);

  fromScratchModel->SetNumGeometries(1);
  fromScratchModel->SetNumGeometryLodLevels(0, 2);

  // a whacky (in terms of code clarity)  way to update data to gpu
  // TODO: try to switch to buffer locking (before writing in it) and then
  // unlocking here (see if performance is affected that way)
  lod0.indexBuffer->SetData(lod0.indexBuffer->GetShadowData());
  lod0.vertexBuffer->SetData(lod0.vertexBuffer->GetShadowData());
  lod1.indexBuffer->SetData(lod1.indexBuffer->GetShadowData());
  lod1.vertexBuffer->SetData(lod1.vertexBuffer->GetShadowData());


  SharedPtr<Geometry> geom (new Geometry(pContext));
  geom->SetIndexBuffer(lod0.indexBuffer);
  geom->SetVertexBuffer(0, lod0.vertexBuffer);
  geom->SetDrawRange(Urho3D::TRIANGLE_LIST, 0, lod0.indexBuffer->GetIndexCount());

  vertexBuffers.push_back(lod0.vertexBuffer);
  indexBuffers.push_back(lod0.indexBuffer);
  geom->SetLodDistance(5.f);
  fromScratchModel->SetGeometry(0, 0, geom);

  geom = new Geometry(pContext);
  geom->SetIndexBuffer(lod1.indexBuffer);
  geom->SetVertexBuffer(0, lod1.vertexBuffer);
  geom->SetDrawRange(Urho3D::TRIANGLE_LIST, 0, lod1.indexBuffer->GetIndexCount());

  vertexBuffers.push_back(lod1.vertexBuffer);
  indexBuffers.push_back(lod1.indexBuffer);
  geom->SetLodDistance(10.f);
  fromScratchModel->SetGeometry(0, 1, geom);

  fromScratchModel->SetBoundingBox(BoundingBox(bounds0, bounds1));
  fromScratchModel->SetVertexBuffers(vertexBuffers, morphRangeStarts, morphRangeCounts);
  fromScratchModel->SetIndexBuffers(indexBuffers);

  return fromScratchModel;
}

SharedPtr<Model> create_1lod_plane (
    Context* pContext,
    const UrhoworldsGeometryConstructionData<UrhoworldsLandscapeVertexElement> &lod0,
    const Vector3 &bounds0,
    const Vector3 &bounds1,
    const Urho3D::PrimitiveType pType /* = Urho3D::TriangleList */) {
  SharedPtr<Model> fromScratchModel(new Model(pContext));
  ea::vector<SharedPtr<VertexBuffer> > vertexBuffers;
  ea::vector<SharedPtr<IndexBuffer> > indexBuffers;

  ea::vector<unsigned> morphRangeStarts;
  ea::vector<unsigned> morphRangeCounts;
  morphRangeStarts.push_back(0);
  morphRangeCounts.push_back(0);

  fromScratchModel->SetNumGeometries(1);
  fromScratchModel->SetNumGeometryLodLevels(0, 1);

  // a whacky (in terms of code clarity)  way to update data to gpu
  // TODO: try to switch to buffer locking (before writing in it) and then
  // unlocking here (see if performance is affected that way)
  lod0.indexBuffer->SetData(lod0.indexBuffer->GetShadowData());
  lod0.vertexBuffer->SetData(lod0.vertexBuffer->GetShadowData());

  SharedPtr<Geometry> geom (new Geometry(pContext));
  geom->SetIndexBuffer(lod0.indexBuffer);
  geom->SetVertexBuffer(0, lod0.vertexBuffer);
  geom->SetDrawRange(pType, 0, lod0.indexBuffer->GetIndexCount());

  vertexBuffers.push_back(lod0.vertexBuffer);
  indexBuffers.push_back(lod0.indexBuffer);
  geom->SetLodDistance(5.f);
  fromScratchModel->SetGeometry(0, 0, geom);

  fromScratchModel->SetBoundingBox(BoundingBox(bounds0, bounds1));
  fromScratchModel->SetVertexBuffers(vertexBuffers, morphRangeStarts, morphRangeCounts);
  fromScratchModel->SetIndexBuffers(indexBuffers);

  return fromScratchModel;
}
