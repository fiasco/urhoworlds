// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com
#include <Impostor.hpp>

#include <Urho3D/Core/Context.h>
#include <Urho3D/Core/CoreEvents.h>
#include <Urho3D/Scene/Scene.h>
#include <Urho3D/Scene/SceneEvents.h>
#include <Urho3D/Graphics/Octree.h>
#include <Urho3D/Graphics/Renderer.h>
#include <Urho3D/Graphics/StaticModel.h>
#include <Urho3D/Resource/ResourceCache.h>
#include <Urho3D/Graphics/Material.h>
#include <Urho3D/Graphics/Viewport.h>
#include <Urho3D/Graphics/Camera.h>
#include <Urho3D/Graphics/Texture2D.h>
#include <Urho3D/Graphics/Graphics.h>
#include <Urho3D/Graphics/Technique.h>
#include <Urho3D/Graphics/Octree.h>
#include <Urho3D/Graphics/RenderPath.h>
#include <Urho3D/Graphics/Model.h>
#include <Urho3D/Graphics/GraphicsEvents.h>
#include <Urho3D/Graphics/View.h>
#include <Urho3D/Graphics/OcclusionBuffer.h>
#include <Urho3D/UI/UI.h> // for XMLFile hmm?
#include <Urho3D/IO/Log.h>
#include <Urho3D/IO/FileSystem.h>
#include <Urho3D/Core/Profiler.h>

#include "LandscapeGenerator.hpp"

#include <algorithm>
#include <unordered_map>
#include <set>


namespace Urho3D {
constexpr int impostor_resolution = 5;
constexpr size_t impostor_segment_size = 512;
    constexpr size_t impostor_texture_resolution = impostor_segment_size * impostor_resolution;
constexpr float impostor_distance = 5;


class ImpostorBaker : public Object {
    URHO3D_OBJECT(ImpostorBaker, Object);
public:
    explicit ImpostorBaker (Context *context)
        : Object (context),
          buffer_ (new Image(context)) {
        screenTexture_ = new Texture2D(context_);
        screenTextureBump_ = new Texture2D(context_);
        screenTextureDepth_ = new Texture2D(context_);
        material_ = GetSubsystem<ResourceCache>()->GetResource<Material>("Misc/OctahedralImpostor.xml")->Clone();
        material_->SetName("OctahedralImpostor" + ea::to_string(size_t(this)));
        material_->SetShaderParameter("ImpostorResolution", impostor_resolution);
        material_->SetShaderParameter("ImpostorDrawDistance", impostor_distance);
    }


    void Initialize (Node* node) {
        node_ = node;

        CreateImpostor();

        if (!impResultImage_) {
            impResultImage_ = new Image(context_);
            impResultImage_->SetSize(impostor_texture_resolution,
                impostor_texture_resolution,
                impRenderTexture_->GetComponents());
        }

        if (!impResultImageBump_) {
            impResultImageBump_ = new Image(context_);
            impResultImageBump_->SetSize(impostor_texture_resolution,
                impostor_texture_resolution,
                impRenderTextureBump_->GetComponents());
        }

        if (!impResultImageDepth_) {
            impResultImageDepth_ = new Image(context_);
            impResultImageDepth_->SetSize(impostor_texture_resolution,
                                          impostor_texture_resolution,
                                          1);
        }

        material_->SetTexture(TU_DIFFUSE, screenTexture_);
        material_->SetTexture(TU_NORMAL, screenTextureBump_);
        material_->SetTexture(TU_EMISSIVE, screenTextureDepth_);

        plane_ = CreatePlane(1.f, Vector2(0, 0), .5f, 0.f);
    }

    Model* GetPlane() {
        return plane_;
    }

    Material* GetMaterial() {
        return material_;
    }

    float GetMarginMultiplier() const {
        return impostorMarginMultiplier;
    }

    BoundingBox GetBounds() const {
        return bounds_;
    }

    Texture2D* GetScreenTexture() {
        return screenTexture_;
    }

private:
    Texture2D* screenTexture_ = nullptr;
    Texture2D* screenTextureBump_ = nullptr;
    Texture2D* screenTextureDepth_ = nullptr;

    void CreateImpostor();
    void ReassembleModel();
    void StartImpostorRender();
    void SetCameraPosition();
    void HandleRenderDone(StringHash eventType,
                          VariantMap& eventData);

    SharedPtr<Model> CreatePlane (const float uv_incr,
                                  const Vector2 &base,
                                  const float scale,
                                  const float vert_offset);

    void GatherImpostorPart(Texture2D *tex,
                            Image* img,
                            const Vector2 &uv_coord,
                            const unsigned components_num_override = 0);

    void ExportImage(Image*, const ea::string &name);

    void ProcessImpostorPartRendered();

    Node* node_ = nullptr;

    SharedPtr<Model> plane_;
    SharedPtr<Material> material_;

    Image* impResultImage_ = nullptr;
    Image* impResultImageBump_ = nullptr;
    Image* impResultImageDepth_ = nullptr;

    // All the inner stuff there:
    Scene* impScene_ =  nullptr;
    Node* impCameraNode_ = nullptr;
    Node *impObjectNode_ = nullptr;

    Image* buffer_ = nullptr;

    Texture2D* impRenderTexture_ = nullptr;
    SharedPtr<Texture2D> impRenderTextureBump_;
    Texture2D* impRenderTextureDepth_ = nullptr;
    Viewport* impViewport_ = nullptr;
    RenderPath* impRenderPath_ = nullptr;

    bool isReady_ = false;
    BoundingBox bounds_;

    const float impostorMarginMultiplier = 1.4142136f;
    int impostorFrameNumber = 0;
};

Impostor::Impostor(Context *context)
    : Component(context),
      baker_(new ImpostorBaker(context))
{
    URHO3D_PROFILE("ImpostorImpostor");
}

// TODO3: delete all the relevant stuff (scene, octree, etc..)
Impostor::~Impostor()
{
}

void Impostor::RegisterObject(Context* context)
{
    context->RegisterFactory<Impostor>();
}

Impostor::Impostor (const Impostor &pOther)
    : Component (pOther.context_) {
    URHO3D_PROFILE("ImpostorCopy");
    distance_ = pOther.distance_;
    baker_ = pOther.baker_;
    amCopied_ = true;
}

Impostor* Impostor::Clone() {
    URHO3D_PROFILE("ImpostorClone");
    auto ret = new Impostor(*this);
    return ret;
}


void Impostor::OnNodeSet(Node* node)
{
    URHO3D_PROFILE("ImpostorOnNodeSet");
    if (node == nullptr) {
        CleanUp();
    } else {
        if (!amCopied_) {
            baker_->Initialize(node_);
        }

        // TODO no need?
        sceneCamera_ = node_->GetScene()->GetChild("Camera", true);

        SetupScreen();
        SubscribeToEvent (node_->GetScene(),
                          E_NODEREMOVED,
                          URHO3D_HANDLER(Impostor, HandleNodeRemoved));
    }
}

void Impostor::OnMarkedDirty(Node* /*node*/) {
    URHO3D_PROFILE("ImpostorOnMarkedDirty");
}

void Impostor::HandleNodeRemoved(StringHash,
                                 VariantMap& evtData) {
    if (evtData[NodeRemoved::P_NODE] == screenNode_
        && evtData[NodeRemoved::P_NODE] == node_) {
        //screenNode_ = nullptr;
    }
}

void Impostor::CleanUp() {
    URHO3D_PROFILE("ImpostorCleanup");
    if (screenNode_ && node_) {
        screenNode_->Remove();
    }
    UnsubscribeFromAllEvents();
}


// TODO handle cases where children has their own transform
void ImpostorBaker::ReassembleModel()
{
    ea::vector<StaticModel*> static_models;
    ea::vector<StaticModel*> static_models_nonrecursive;
    node_->GetComponents<StaticModel> (static_models, true /*recursive*/);
    node_->GetComponents<StaticModel> (static_models_nonrecursive, false);

    if (static_models_nonrecursive.size() > static_models.size()) {
        URHO3D_LOGWARNING("Impostor: child nodes assumed to have no"
                          " transform");
    }

    impObjectNode_ = impScene_->CreateChild();

    bounds_ = BoundingBox();
    for (StaticModel* s_m : static_models) {
        StaticModel* mdl = impObjectNode_->CreateComponent<StaticModel>();
        mdl->SetModel(s_m->GetModel());
        auto mat = s_m->GetMaterial();
        auto my_bump_pass = mat->GetTechnique(0)->CreatePass("my_bump_pass");
        my_bump_pass->SetPixelShader("Imp");
        my_bump_pass->SetVertexShader("Imp");

        mdl->SetMaterial(s_m->GetMaterial());

        bounds_.Merge(s_m->GetModel()->GetBoundingBox());
    }
}


float getClosestCoord (const float coord,
                       const int impostorResolution) {
    float res = (float) impostorResolution;
    float rounded = std::round(coord * res);
    return (float)rounded / res;
}

Vector2 getClosestCoord (const Vector2 &coord,
                         const int impostorResolution) {
    return Vector2(getClosestCoord(coord.x_, impostorResolution),
                   getClosestCoord(coord.y_, impostorResolution));
}


Vector2 getUvCoordByNum (const int num, const int impostorResolution) {
    const float base_offset = 1.f / ((float) impostorResolution);
    const float horizontal_offset =
        ((float)(num % impostorResolution)) * base_offset;
    const float vertical_offset =
        ((float)(num / impostorResolution)) * base_offset;

    Vector2 ret = Vector2(vertical_offset,
                          horizontal_offset);
    return ret;
}

Vector2 getCamCoordByNum (const int num, const int impostorResolution) {
    const float base_offset = 2.f / ((float)impostorResolution - 1);
    const float horizontal_offset =
        ((float)(num % impostorResolution)) * base_offset;
    const float vertical_offset =
        ((float)(num / impostorResolution)) * base_offset;

    Vector2 ret = Vector2(-1.f + vertical_offset,
                          -1.f + horizontal_offset);
    return ret;
}

float sign (const float val) {
    return (val > 0) ? 1.f : -1.f;
}

// input coordinates are ranged from -1..1, not 0..1
Vector3 UVtoPyramid(Vector2 uv) {
    float leftright_negation = 1 - std::abs(uv.x_);
    float frontback_negation = 1 - std::abs(uv.y_);

    float up = std::min(leftright_negation, frontback_negation);
    Vector3 position (uv.x_, up, uv.y_);

    return position.Normalized();
}

float sqr(float val) {
	return val * val;
}

float calc_simple_square_stretching(float x, float y) {
    // https://arxiv.org/ftp/arxiv/papers/1509/1509.06344.pdf
    // page 3, Disc to square mapping:
    if (sqr(x) >= sqr(y)) {
        return sign(x) * (std::sqrt(sqr(x) + sqr(y)));
    }
    return  sign(y) * x / y * (std::sqrt(sqr(x) + sqr(y)));
}

Vector2 calc_simple_square_stretching (Vector2 in) {
    return { calc_simple_square_stretching(in.x_, in.y_),
             calc_simple_square_stretching(in.y_, in.x_) };
}

// returns -1..1 coordinates
Vector2 PyramidUV(const Vector3 &direction) {
    Vector3 dir = direction.Normalized();
    Vector2 uv;

    uv.x_ = calc_simple_square_stretching(dir.x_, dir.z_);
    uv.y_ = calc_simple_square_stretching(dir.z_, dir.x_);

    return uv / (1 + dir.y_);
}

Vector2 GetCurrentUV (const Vector3 &camera_direction) {
    auto puv = PyramidUV(camera_direction);

    Vector2 pyr =
        (puv + Vector2(1.f, 1.f))
        * 0.5f;

    return pyr;
}

float get_next_coord (const float base,
                      const float offset,
                      const float imp_res) {
    return { base + 1.f / (imp_res) * (offset >= 0 ? 1.f : -1.f) };
}
struct ImpostorUvData {
    Vector2 offset1, offset2, offset3, offset4;
    float factor1, factor2, factor3, factor4;
};

ImpostorUvData GetCurrentUVs (const Vector2 &curr,
                              const int p_impostor_resolution) {
    const auto base = getClosestCoord(curr, p_impostor_resolution - 1);
    const auto offset = curr - base;

    Vector2 curr_amts = { 1.f - std::abs(offset.x_ * (p_impostor_resolution - 1)),
                          1.f - std::abs(offset.y_ * (p_impostor_resolution - 1)) };

    const Vector2 next_x =
        { get_next_coord(base.x_, offset.x_, p_impostor_resolution - 1),
          base.y_ };
    const Vector2 next_y =
        { base.x_,
          get_next_coord(base.y_, offset.y_, p_impostor_resolution - 1) };

    const Vector2 next_xy =
        { next_x.x_,
          next_y.y_, };

    float conv = (p_impostor_resolution - 1.f) / p_impostor_resolution;

    return { base * conv ,
             next_x * conv,
             next_y * conv,
             next_xy * conv,

             curr_amts.x_ * curr_amts.y_,
             curr_amts.y_ * (1.f - curr_amts.x_),
             curr_amts.x_ * (1.f - curr_amts.y_),
             (1.f - curr_amts.x_) * (1 - curr_amts.y_),
    };
}

void ImpostorBaker::SetCameraPosition() {
    Camera *cam = impCameraNode_->GetComponent<Camera>();

    const Vector3 obj_size = bounds_.Size() * impostorMarginMultiplier;

    float max_dimention = std::max(obj_size.x_,
                                   std::max(obj_size.y_,
                                            obj_size.z_));

    Vector2 cam_coord =
        getCamCoordByNum(impostorFrameNumber, impostor_resolution);

    Vector3 cam_pos_shift = UVtoPyramid(cam_coord);

    cam_pos_shift *= max_dimention;
    Vector3 cam_pos = bounds_.Center() + cam_pos_shift;

    cam->SetOrthographic(true);
    cam->SetOrthoSize(max_dimention);
    cam->SetAspectRatio(1.f);
    cam->SetNearClip(cam_pos_shift.Length() - max_dimention / 2.f);
    cam->SetFarClip(cam_pos_shift.Length() + max_dimention / 2.f);
    impCameraNode_->SetPosition(cam_pos);
    auto dbg_offset = Vector3(0.f, 1.f, 0.f) * 0.f; // TODO remove
    impCameraNode_->LookAt(bounds_.Center() + dbg_offset);
}

SharedPtr<Model> ImpostorBaker::CreatePlane (const float uv_incr,
                                             const Vector2 &uv_base,
                                             const float scale,
                                             const float vert_offset) {
    UrhoworldsPlaneConstructionData<
        UrhoworldsTexturedNormalMappedVertexElement> dt;

    Vector3 normal{0, 0, -1};
    Vector3 tangent{1, 0, 0};

    dt.vertex_data->push_back({ Vector3{ -1, -1 + vert_offset, 0 } * scale,
                                normal,
                                tangent,
                                uv_base + Vector2{ 0, uv_incr } });

    dt.vertex_data->push_back({ Vector3{ -1, 1 + vert_offset, 0 } * scale,
                                normal,
                                tangent,
                                uv_base });
    dt.vertex_data->push_back({ Vector3{ 1, -1 + vert_offset, 0 } * scale,
                                normal,
                                tangent,
                                uv_base + Vector2(uv_incr, uv_incr) });
    dt.vertex_data->push_back({ Vector3{ 1, 1 + vert_offset, 0 } * scale,
                                normal,
                                tangent,
                                uv_base + Vector2{ uv_incr, 0 } });

    const Vector3 bbmin (-1, -1, 0);
    const Vector3 bbmax (1, 1, 0);

    dt.index_data->push_back({0, 1, 2});
    dt.index_data->push_back({2, 1, 3});

    return create_1lod_plane(context_, dt, bbmin, bbmax);
}

void ImpostorBaker::StartImpostorRender()
{
    URHO3D_PROFILE("ImpostorBakerStartImpostorRender");

    impostorFrameNumber = 0;

    SetCameraPosition();

    SubscribeToEvent (E_ENDVIEWRENDER,
                      URHO3D_HANDLER(ImpostorBaker, HandleRenderDone));


    impRenderTexture_->GetRenderSurface()->QueueUpdate();

    ++impostorFrameNumber;
}

void ImpostorBaker::GatherImpostorPart(Texture2D *tex,
                                       Image* img,
                                       const Vector2 &uv_coord,
                                       const unsigned components_num_override) {
    auto components = tex->GetComponents();
    if (components_num_override) {
        components = components_num_override;
    }
    // TODO we should do it when start rendering...
    buffer_->SetSize(tex->GetWidth(),
                     tex->GetHeight(),
                     components);

    tex->GetData(0, buffer_->GetData());

    img->SetSubimage(buffer_,
                     IntRect(int(uv_coord.x_),
                             int(uv_coord.y_),
                             int(uv_coord.x_) + tex->GetWidth(),
                             int(uv_coord.y_) + tex->GetHeight()));
}

// TODO: move to Utils
void normalize_urho_image (Image* img) {
    assert(img->GetComponents() == 1);
    // calculate max and min
    unsigned char *dt = img->GetData();
    unsigned char color_min = 255;
    unsigned char color_max = 0;
    for (int i = 0; i < img->GetWidth() * img->GetHeight(); ++i) {
        if (dt[i] == 255) {
            continue; // ignore white (empty) part of image
        }
        color_min = std::min(color_min, dt[i]);
        color_max = std::max(color_max, dt[i]);
    }

    // substract min from every pixel and multiply by 255/(max-min)
    for (int i = 0; i < img->GetWidth() * img->GetHeight(); ++i) {
        dt[i] = (unsigned char)(float(dt[i] - color_min) * (255.f / float(color_max - color_min)));
    }
}

void ImpostorBaker::ProcessImpostorPartRendered() {
    if (impostorFrameNumber == 1) {

    }

        if (impostorFrameNumber <= impostor_resolution * impostor_resolution) {
            Vector2 uv_coord =
                getUvCoordByNum(impostorFrameNumber - 1, impostor_resolution)
                * float(impostor_texture_resolution);

            GatherImpostorPart(impRenderTexture_,
                               impResultImage_,
                               uv_coord);
            GatherImpostorPart(impRenderTextureBump_,
                               impResultImageBump_,
                               uv_coord);
            GatherImpostorPart(impRenderTextureDepth_,
                               impResultImageDepth_,
                               uv_coord,
                               1);

            SetCameraPosition();
            impRenderTexture_->GetRenderSurface()->QueueUpdate();
            ++impostorFrameNumber;
        }

        if (impostorFrameNumber == impostor_resolution * impostor_resolution + 1)  {
            normalize_urho_image(impResultImageDepth_);

            // emit signal or anything?
            //ExportImage(impResultImage_, "impostor");
            //ExportImage(impResultImageBump_, "impostor_bump");
            //ExportImage(impResultImageDepth_, "impostor_depth");

            screenTexture_->SetSize(impostor_texture_resolution,
                                    impostor_texture_resolution,
                                    Graphics::GetRGBAFormat(),
                                    Urho3D::TEXTURE_STATIC);
            screenTexture_->SetData(impResultImage_);


            screenTextureBump_->SetSize(impostor_texture_resolution,
                                        impostor_texture_resolution,
                                        Graphics::GetRGBAFormat(),
                                        Urho3D::TEXTURE_STATIC);
            screenTextureBump_->SetData(impResultImageBump_);

            screenTextureDepth_->SetSize(impostor_texture_resolution,
                                         impostor_texture_resolution,
                                         Graphics::GetReadableDepthFormat(),
                                         Urho3D::TEXTURE_STATIC);
            screenTextureDepth_->SetData(impResultImageDepth_);

            UnsubscribeFromEvent(E_ENDVIEWRENDER);
        }
}



void ImpostorBaker::HandleRenderDone (StringHash,
                                      VariantMap& eventData) {
    URHO3D_PROFILE("ImpostorBakerHandleRenderDone");
    if (impRenderTexture_ // TODO unneeded check?
        && eventData[EndViewRender::P_SURFACE].GetPtr()
           == impRenderTexture_->GetRenderSurface()) {
        ProcessImpostorPartRendered();
    }
}

void Impostor::SetNodeStaticModelsAutoHide() {
    URHO3D_PROFILE("ImpostorSetNodeStaticModelsAutoHide");
    ea::vector<StaticModel*> dst;
    node_->GetComponents<StaticModel>(dst);
    for (auto el : dst) {
        el->SetDrawDistance(impostor_distance);
    }
}

void ImpostorBaker::CreateImpostor()
{
    if (!impScene_) {
        impScene_ = new Scene(context_);
        impScene_->CreateComponent<Octree>();

        // add light (should probably set ambient color to 1 1 1)
        Node* lightNode = impScene_->CreateChild(); 
        lightNode->SetDirection(Vector3(1.f, -0.3f, 0.0f));

        Light* light = lightNode->CreateComponent<Light>();
        light->SetLightType(LIGHT_DIRECTIONAL);
        light->SetColor({ 0.01f, 0.01f, 0.01f });

        impCameraNode_ = impScene_->CreateChild();
        impCameraNode_->CreateComponent<Camera>();

        // create rgba texture
        const int segment_res
            = impostor_texture_resolution / impostor_resolution;
        impRenderTexture_ = new Texture2D(context_);
        impRenderTexture_->SetSize(segment_res,
                                   segment_res,
                                   Graphics::GetRGBAFormat(),
                                   Urho3D::TEXTURE_RENDERTARGET,
                                   1);

        impRenderTexture_->SetFilterMode(FILTER_NEAREST);

        auto cache = GetSubsystem<ResourceCache>();
        if (!cache->GetResource<Texture2D>("my_depth")) {
            impRenderTextureDepth_ = new Texture2D(context_);
            impRenderTextureDepth_->SetSize(segment_res,
                                            segment_res,
                                            Graphics::GetReadableDepthFormat(),
                                            Urho3D::TEXTURE_DEPTHSTENCIL,
                                            1);
            impRenderTextureDepth_->SetName("my_depth");
            cache->AddManualResource(impRenderTextureDepth_);
            impRenderTextureDepth_->SetFilterMode(FILTER_NEAREST);
        } else {
            impRenderTextureDepth_ = cache->GetResource<Texture2D>("my_depth");;
        }

        if (!cache->GetResource<Texture2D>("my_bump")) {
            impRenderTextureBump_ = new Texture2D(context_);
            impRenderTextureBump_->SetSize(segment_res,
                                           segment_res,
                                           Graphics::GetRGBAFormat(),
                                           Urho3D::TEXTURE_RENDERTARGET,
                                           1);
            impRenderTextureBump_->SetName("my_bump");
            cache->AddManualResource(impRenderTextureBump_);
        } else {
            impRenderTextureBump_ = cache->GetResource<Texture2D>("my_bump");;
        }

        impRenderPath_ = new RenderPath();
        impRenderPath_->Load(
            GetSubsystem<ResourceCache>()
            ->GetResource<XMLFile>("RenderPaths/ForwardDepthWithBumpBake.xml"));


        RenderSurface* renderSurface = impRenderTexture_->GetRenderSurface();
        //renderSurface->SetLinkedDepthStencil(
        //            impRenderTextureDepth_->GetRenderSurface());

        impViewport_ = new Viewport(context_,
                                    impScene_,
                                    impCameraNode_->GetComponent<Camera>());
        impViewport_->SetRenderPath(impRenderPath_);
        renderSurface->SetUpdateMode(Urho3D::SURFACE_MANUALUPDATE);
        //renderSurface->SetLinkedRenderTarget(
        //impRenderTextureDepth_->GetRenderSurface());
        //renderSurfaceDepth->SetViewport(1, impViewport_);
        renderSurface->SetViewport(0, impViewport_);
    } else {
        //impScene_->RemoveChild(impObjectNode_);
    }


    ReassembleModel();
    Zone* a_zone = new Zone(context_);
    impObjectNode_->GetComponent<StaticModel>()->SetZone(a_zone);
    impObjectNode_->GetComponent<StaticModel>()->GetZone()->SetAmbientColor({1.f, 1.f, 1.f});

    StartImpostorRender();
}

void Impostor::SetupScreen() {
    URHO3D_PROFILE("ImpostorSetupScreen");
    screenNode_ = node_->GetParent()->CreateChild();
    screenNode_->SetRotation(node_->GetWorldRotation());
    // TODO make sure  we update it if need

    screenNode_->SetWorldPosition(Vector3(0.f, baker_->GetBounds().Center().y_, 0.f) + node_->GetWorldPosition());
    
    const Vector3 obj_size = baker_->GetBounds().Size() * baker_->GetMarginMultiplier();

    float max_dimention = std::max(obj_size.x_,
                                   std::max(obj_size.y_,
                                            obj_size.z_));

    // I am not good with 3d-math so I resorted to using a proxy parent node to
    // make my life easier...
    screenNode_->SetScale(Vector3(max_dimention, max_dimention, 1));
    StaticModel* screenObject = screenNode_->CreateComponent<StaticModel>();

    screenObject->SetModel(baker_->GetPlane());

    // material_->SetTexture(TU_DIFFUSE,
    //                       cache->GetResource<Texture2D>("Utils/Checker.png"));

    screenObject->SetMaterial(baker_->GetMaterial());

    SetNodeStaticModelsAutoHide();
}

void ImpostorBaker::ExportImage(Image* img, const ea::string &name) {
    ea::string pathName(GetSubsystem<FileSystem>()->GetProgramDir());
    ea::string filePath(pathName +
        ea::to_string(size_t(this)) +
                    "_" +
        ea::to_string(impostorFrameNumber) +
                    name +
                    ".png");

    img->SavePNG(filePath);
}

Material* Impostor::GetMaterial() {
    return baker_->GetMaterial();
}

} // namespace Urho3D
