#ifndef RACINGGAME_SETTINGS_EXCEPTION_HPP__
#define RACINGGAME_SETTINGS_EXCEPTION_HPP__

#include <string>

#include "Utils/AppException.hpp"
#include "Utils/StringUtils.hpp"

class SettingsException : public AppException {
public:
    enum TYPE { ALREADY_LOADED, NOT_FOUND, OUT_OF_RANGE };
    SettingsException (const std::string &setting_name, TYPE t)
        : settingName(setting_name),
          type (t) {
    }
    ~SettingsException () throw () {}

    const std::string getFullDescription () const {
        return std::string("Setting Error ")
            + StringUtils::toString(this->type) + ": \""
                           + settingName + "\"";
    }
    const std::string settingName;
    const TYPE type;
};

#endif // RACINGGAME_SETTINGS_EXCEPTION_HPP__
