// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com
#include "SettingsGui.hpp"

#include <Urho3D/Core/CoreEvents.h>
#include <Urho3D/Engine/Engine.h>
#include <Urho3D/Graphics/Graphics.h>
#include <Urho3D/Graphics/Texture2D.h>
#include <Urho3D/Input/Input.h>
#include <Urho3D/Resource/ResourceCache.h>
#include <Urho3D/UI/Button.h>
#include <Urho3D/UI/CheckBox.h>
#include <Urho3D/UI/LineEdit.h>
#include <Urho3D/UI/Slider.h>
#include <Urho3D/UI/Text.h>
#include <Urho3D/UI/ToolTip.h>
#include <Urho3D/UI/UI.h>
#include <Urho3D/UI/UIEvents.h>
#include <Urho3D/UI/UIElement.h>
#include <Urho3D/UI/Window.h>
#include <Urho3D/UI/ScrollBar.h>
#include <Urho3D/UI/ScrollView.h>
#include <Urho3D/UI/ListView.h>
#include <Urho3D/IO/Log.h>


#include "Settings/Settings.hpp"
#include "Settings/SettingsException.hpp"

#include <VcppBits/MathUtils/MathUtils.hpp>

using namespace VcppBits::MathUtils;
using namespace Urho3D;

class UrhoworldsSlider : public Slider {
public:
    explicit UrhoworldsSlider (Context* pContext)
        : Slider (pContext) {
    }
    bool isDragging () const {
        return Slider::dragSlider_;
    }
};

SettingsGui::SettingsGui (Urho3D::Context *pContext)
    : Urho3D::Object (pContext),
      uiRoot_(GetSubsystem<UI>()->GetRoot()){
}

void SettingsGui::setAlignment (const Urho3D::HorizontalAlignment pHoriz,
                                const Urho3D::VerticalAlignment pVert) {
    _horizontalAlignment = pHoriz;
    _verticalAlignment = pVert;
}


void SettingsGui::init (Settings *pSettings) {
    _settings = pSettings;
}

void SettingsGui::load () {
    _scrollView = nullptr;
    _settingsTabs.clear();

    GetSubsystem<Input>()->SetMouseVisible(true);

    window_ = new Window(context_);
    window_->SetMinSize(300, 500);

    uiRoot_->AddChild(window_);

    // Set Window size and layout settings
    window_->SetMinWidth(384);
    window_->SetLayout(LM_VERTICAL, 6, IntRect(6, 6, 6, 6));
    window_->SetAlignment(_horizontalAlignment, _verticalAlignment);
    window_->SetName("Window");
    window_->SetMaxHeight(700);

    // Create Window 'titlebar' container
    auto* titleBar = new UIElement(context_);
    titleBar->SetMinSize(0, 24);
    titleBar->SetVerticalAlignment(VA_TOP);
    titleBar->SetLayoutMode(LM_HORIZONTAL);
    titleBar->SetMaxHeight(24);

    // Create the Window title Text
    auto* windowTitle = new Text(context_);
    windowTitle->SetName("WindowTitle");
    windowTitle->SetText("Hello GUI!");

    // Create the Window's close button
    auto* buttonClose = new Button(context_);
    buttonClose->SetName("CloseButton");

    // Add the controls to the title bar
    titleBar->AddChild(windowTitle);
    titleBar->AddChild(buttonClose);

    // Add the title bar to the Window
    window_->AddChild(titleBar);


    // Apply styles
    window_->SetStyleAuto();
    windowTitle->SetStyleAuto();
    buttonClose->SetStyle("CloseButton");

    auto *tabs_frame = window_->CreateChild<UIElement>();
    tabs_frame->SetStyleAuto();
    tabs_frame->SetLayoutMode(LM_HORIZONTAL);
    tabs_frame->SetMaxHeight(24);

    auto cat_it = _settings->getSettingsCategoriesIterator();

    while (cat_it.isElement()) {
        ea::string cat_name (cat_it.getName().c_str());

        if (!cat_name.size()) {
            cat_name = "General";
        }

        auto* tab_el = tabs_frame->CreateChild<Button>();
        tab_el->SetStyleAuto();
        auto* text = tab_el->CreateChild<Text>();
        text->SetStyleAuto();
        text->SetText(cat_name);
        text->SetAlignment(HA_CENTER, VA_CENTER);

        SubscribeToEvent(tab_el,
                         E_RELEASED,
                         URHO3D_HANDLER(SettingsGui,
                                        tabButtonPressed));

        auto settings_area = new UIElement(context_);
        settings_area->SetStyleAuto();
        settings_area->SetLayout(LM_VERTICAL, 6, IntRect(6, 6, 6, 6));

        auto* cont = new ScrollView(context_);
        cont->SetStyleAuto();
        cont->SetContentElement(settings_area);

        _settingsTabs.insert(std::make_pair(tab_el, cont));

        window_->AddChild(cont);
        cont->SetVisible(false);

        if (!_scrollView) {
            _scrollView = cont;
            _scrollView->SetVisible(true);
        }

        {
            auto it = cat_it.getSettingsIterator();
            while (it.isElement()) {

                auto set = it.getCurrent();
                ea::string name(set.getName().c_str());

                auto *frame = new UIElement(context_);
                frame->SetStyleAuto();
                //frame->SetMinSize(24, 24);
                frame->SetLayoutMode(LM_HORIZONTAL);

                auto* el = new Text(context_);
                el->SetName(name + "_title");
                el->SetText(name);
                el->SetStyleAuto();

                const auto curr_set_text =
                    ea::string(set.getAsString().c_str());

                auto *el_edit = new LineEdit(context_);
                el_edit->SetName(name);
                el_edit->SetText(curr_set_text);
                el_edit->SetStyleAuto();
                el_edit->SetMinSize(100, 16);
                //el_edit->SetFixedWidth(40);

                frame->AddChild(el);
                frame->AddChild(el_edit);
                settings_area->AddChild(frame);

                SubscribeToEvent(el_edit,
                                 E_TEXTFINISHED,
                                 URHO3D_HANDLER(SettingsGui,
                                                processTextFinished));


                if (set.getType() == Setting::S_INTEGER_BOUNDED
                    || set.getType() == Setting::S_FLOATINGPOINT_BOUNDED) {
                    float range = 1;

                    frame->AddChild(el);

                    settings_area->AddChild(frame);

                    if (set.getType() == Setting::S_INTEGER_BOUNDED) {
                        range = float(set.getIntUp() - set.getIntDown());
                    } else if (set.getType() == Setting::S_FLOATINGPOINT_BOUNDED) {
                        range = set.getFloatUp() - set.getFloatDown();
                    }

                    // something arbitrarily bigger than any screen
                    constexpr int slider_max_size = 16 * 16 * 16 * 16;

                    UrhoworldsSlider* slider(new UrhoworldsSlider(context_));
                    settings_area->AddChild(slider);
                    slider->SetStyleAuto();
                    slider->SetAlignment(HA_LEFT, VA_TOP);
                    slider->SetName(name);
                    slider->SetRange(range);
                    slider->SetMaxSize(slider_max_size, 16);
                    slider->SetMinSize(0, 16);
                    if (set.getType() == Setting::S_INTEGER_BOUNDED) {
                        slider->SetValue(float(set.getInt()));
                    }
                    else if (set.getType() == Setting::S_FLOATINGPOINT_BOUNDED) {
                        slider->SetValue(set.getFloat());
                    }
                    slider->SetVar(name+"_value", static_cast<void *>(el_edit));

                    slider->SetMaxSize(slider_max_size, 16);
                    SubscribeToEvent(slider, E_SLIDERCHANGED, URHO3D_HANDLER(SettingsGui, processSliderChanged));

                } else {
                }
                it.peekNext();
            }

        }
        cat_it.peekNext();
    }
}


void SettingsGui::unload () {
    UnsubscribeFromAllEvents();
    uiRoot_->RemoveChild(window_);
}

void SettingsGui::processTextFinished (StringHash /*eventType*/,
                                       VariantMap& eventData) {
    auto el = (LineEdit*)eventData[TextFinished::P_ELEMENT].GetPtr();
    auto text = eventData[TextFinished::P_TEXT].GetString();

    auto &set = _settings->getSetting(el->GetName().c_str());
    try {
        set.setByString(text.c_str());
    } catch (SettingsException&) {
        el->SetText(ea::string(set.getAsString().c_str()));
    }
}

void SettingsGui::processSliderChanged (StringHash /*eventType*/,
                                        VariantMap& eventData) {

    UrhoworldsSlider* slider =
        static_cast<UrhoworldsSlider*>(eventData[SliderChanged::P_ELEMENT].GetPtr());
    assert(slider != NULL);

    float slider_value = eventData[SliderChanged::P_VALUE].GetFloat();


    auto slider_name = slider->GetName();
    const std::string slider_name_stdstring = slider_name.c_str();

    auto &set = _settings->getSetting(slider_name_stdstring);

    try {
        if (set.getType() == Setting::S_INTEGER_BOUNDED) {
            auto new_val = int(slider_value);
            if (new_val != set.getInt()) {
                set.setInt(int(slider_value));
            }
        } else if (set.getType() == Setting::S_FLOATINGPOINT_BOUNDED) {
            if (!compareFloats(slider_value, set.getFloat())) {
                set.setFloat(slider_value);
            }
        }
    } catch (SettingsException&) {
        if (set.getType() == Setting::S_INTEGER_BOUNDED) {
            slider->SetValue(float(set.getInt() - set.getIntDown()));
        } else if (set.getType() == Setting::S_FLOATINGPOINT_BOUNDED) {
            slider->SetValue(set.getFloat() - set.getFloatDown());
        }
    }


    LineEdit* valueText(static_cast<LineEdit*>(slider->GetVar(slider_name+"_value").GetVoidPtr()));
    if (valueText)
        valueText->SetText(ea::string(set.getAsString().c_str()));
}


void SettingsGui::tabButtonPressed (StringHash /*eventType*/,
                                    VariantMap& eventData) {
    Button* btn = dynamic_cast<Button*>(eventData[Released::P_ELEMENT].GetPtr());

    if (btn && _settingsTabs.count(btn)) {
        _scrollView->SetVisible(false);
        _scrollView = _settingsTabs[btn];
        _scrollView->SetVisible(true);
    }
}


