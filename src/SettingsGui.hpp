#pragma once


#include <map>

#include <Urho3D/Core/Object.h>
#include <Urho3D/Container/Ptr.h>
#include <Urho3D/UI/UIElement.h>
#include <Urho3D/UI/Button.h>
#include <Urho3D/UI/UIEvents.h>
#include <Urho3D/Input/Input.h>

#include "StateManager.hpp"

namespace Urho3D {
    class Context;
    class Window;
    class ScrollView;
    class ListView;
}

class Settings;


class SettingsGui : public Urho3D::Object {
    URHO3D_OBJECT(SettingsGui, Urho3D::Object);

public:
    explicit SettingsGui (Urho3D::Context *pContext);

    void setAlignment (const Urho3D::HorizontalAlignment pHoriz,
                       const Urho3D::VerticalAlignment pVert);
    void init (Settings *pSettings);

    void load ();
    void unload ();

    void processTextFinished (Urho3D::StringHash /*eventType*/,
                              Urho3D::VariantMap& eventData);
    void processSliderChanged (Urho3D::StringHash /*eventType*/,
                               Urho3D::VariantMap& eventData);
    void handleKeyDown(Urho3D::StringHash /*eventType*/,
                       Urho3D::VariantMap& eventData);
    void tabButtonPressed (Urho3D::StringHash /*eventType*/,
                           Urho3D::VariantMap& eventData);

    Urho3D::Window * getUiWindow () {
        return window_;
    }

private:
    Settings *_settings = nullptr;
    Urho3D::UIElement *uiRoot_ = nullptr;
    Urho3D::Window *window_ = nullptr;
    Urho3D::ScrollView *_scrollView = nullptr; // active area

    std::map<Urho3D::Button*, Urho3D::ScrollView*> _settingsTabs;

    Urho3D::HorizontalAlignment _horizontalAlignment = Urho3D::HA_CENTER;
    Urho3D::VerticalAlignment _verticalAlignment = Urho3D::VA_CENTER;
};


class SettingsGuiState : public IState,
                         public Urho3D::Object {
public:
    URHO3D_OBJECT(SettingsGuiState, Urho3D::Object);
    explicit SettingsGuiState (Urho3D::Context *pContext,
                               StateManager *pStateManager)
        : Urho3D::Object (pContext),
          _stateManager (pStateManager),
          _gui (pContext) {
    }
    void init (Settings *pSettings) {
        _settings = pSettings;
        _gui.init(pSettings);
    }
    void load () override {
        _gui.load();

        SubscribeToEvent(Urho3D::E_KEYDOWN, URHO3D_HANDLER(SettingsGuiState,
                                                           handleKeyDown));
    }
    void unload () override {
        _gui.unload();
        UnsubscribeFromAllEvents();
    }

    void handleKeyDown(Urho3D::StringHash /*eventType*/,
                       Urho3D::VariantMap& eventData) {
        int key = eventData[Urho3D::KeyUp::P_KEY].GetInt();

        if (key == Urho3D::KEY_ESCAPE) {
            _stateManager->popState();
        }
    }

    std::string getName () const override { return "SettingsGuiState"; }

    void setAlignment (const Urho3D::HorizontalAlignment pHoriz,
                       const Urho3D::VerticalAlignment pVert) {
        _gui.setAlignment(pHoriz, pVert);
    }

protected:
    StateManager *_stateManager;
    SettingsGui _gui;
    Settings *_settings = nullptr;
};
