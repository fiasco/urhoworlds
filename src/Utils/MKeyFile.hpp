// Copyright 2015 Vitalii Minnakhmetov <restlessmonkey@ya.ru>
//
// This file is part of LFSTop.
//
// LFSTop is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LFSTop is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LFSTop.  If not, see <http://www.gnu.org/licenses/>.


#ifndef RACINGGAME_M_KEY_FILE_HPP__
#define RACINGGAME_M_KEY_FILE_HPP__

#include <map>
#include <stdexcept>
#include <memory>


typedef std::map<std::string, std::string> MSettings;
typedef std::multimap<std::string, std::shared_ptr<MSettings>> MSections;

class MKeyFileOutOfRangeException {};
class MKeyFileSettingNotFoundException {};

class MSettingsIterator {
public:
    MSettingsIterator (MSettings::iterator b,
                       MSettings::iterator e,
                       MSettings &settings);

    void peekNext ();
    bool isElement () const;

    MSettings::value_type getSetting () const;
    std::string getName () const;
    std::string getValue () const;

    std::string findSetting (const std::string &name) const;

private:
    const MSettings::iterator mBegin;
    const MSettings::iterator mEnd;
    MSettings::iterator mCurrent;

    MSettings &mSettings;

    bool mCurrentIsElement;
};

class MSectionsIterator {
public:
    MSectionsIterator (const MSections::const_iterator b,
                       const MSections::const_iterator e);

    void peekNext ();
    bool isElement () const;

    std::string getName() const;

    MSettingsIterator getSettingsIterator () const;
private:
    const MSections::const_iterator mBegin;
    const MSections::const_iterator mEnd;
    MSections::const_iterator mCurrent;
    bool mCurrentIsElement;
};

class MKeyFile {
public:
    class file_not_found : public std::runtime_error {
    public:
        file_not_found (const std::string &message)
            : std::runtime_error (message.c_str()) {
        }
    };

    MKeyFile (const std::string &filename);
    MKeyFile() {
        std::shared_ptr<MSettings> current_settings(new MSettings());
        std::string current_section_name("");

        mSections.insert(
            MSections::value_type(current_section_name,
                                  current_settings));
    };
    ~MKeyFile ();

    MSectionsIterator getSectionsIterator () const;

    size_t sectionCount (const std::string &pSectionName) const;
    MSettingsIterator getLastSectionSettings (const std::string &pSectionName) const;

    void appendKey (const std::string &section,
                    const std::string &key,
                    const std::string &value);
    // key format here is section.key_name or just key_name
    void appendKey (const std::string &key,
                    const std::string &value);

    void writeToFile (const std::string &filename);
private:
    MSections mSections;
};

#endif /* RACINGGAME_M_KEY_FILE_HPP__ */
