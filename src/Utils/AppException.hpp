#ifndef APP_EXCEPTION_HPP__
#define APP_EXCEPTION_HPP__

#include <exception>
#include <string>

class AppException : public std::exception {
public:
    ~AppException () throw();
    virtual const std::string getFullDescription () const = 0;
    const char *what () const throw();
};

class SimpleException : public AppException {
public:
    SimpleException (const std::string &description);
    ~SimpleException () throw() {}
    const std::string getFullDescription () const;
private:
    const std::string description;
};

#endif // APP_EXCEPTION_HPP__
