// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

// Copyright 2015 Vitalii Minnakhmetov <restlessmonkey@ya.ru>
//
// This file is part of LFSTop.
//
// LFSTop is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LFSTop is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LFSTop.  If not, see <http://www.gnu.org/licenses/>.


#include "Utils/MKeyFile.hpp"

#include <cassert>
#include <fstream>

#include "Utils/StringUtils.hpp"

MSettingsIterator::MSettingsIterator (MSettings::iterator b,
                                      MSettings::iterator e,
                                      MSettings &settings)
    : mBegin (b),
      mEnd (e),
      mCurrent (b),
      mSettings (settings),
      mCurrentIsElement (!(mCurrent == mEnd)) {
}


void MSettingsIterator::peekNext () {
    if (mCurrentIsElement) {
        mCurrent++;

        if (mCurrent == mEnd) {
            mCurrentIsElement = false;
        }
    }
    else {
        throw MKeyFileOutOfRangeException();
    }
}


bool MSettingsIterator::isElement () const {
    return mCurrentIsElement;
}


MSettings::value_type MSettingsIterator::getSetting () const {
    if (!mCurrentIsElement) {
        throw MKeyFileOutOfRangeException();
    }

    return *mCurrent;
}


std::string MSettingsIterator::getName () const {
    if (!mCurrentIsElement) {
        throw MKeyFileOutOfRangeException();
    }

    return mCurrent->first;
}


std::string MSettingsIterator::getValue () const {
    if (!mCurrentIsElement) {
        throw MKeyFileOutOfRangeException();
    }
    return mCurrent->second;
}


std::string MSettingsIterator::findSetting (const std::string &name) const {
    if (!mSettings.count(name)) {
        throw MKeyFileSettingNotFoundException();
    }

    return mSettings[name];
}




MSectionsIterator::MSectionsIterator (MSections::const_iterator b,
                                      MSections::const_iterator e)
    : mBegin(b),
      mEnd(e),
      mCurrent(b) {
    mCurrentIsElement = !(mCurrent == mEnd);
}


void MSectionsIterator::peekNext () {
    if (mCurrentIsElement) {
        ++mCurrent;

        if (mCurrent == mEnd) {
            mCurrentIsElement = false;
        }
    }
    else {
        throw MKeyFileOutOfRangeException();
    }
}


bool MSectionsIterator::isElement () const {
    return mCurrentIsElement;
}


std::string MSectionsIterator::getName () const {
    if (!mCurrentIsElement) {
        throw MKeyFileOutOfRangeException();
    }

    return mCurrent->first;
}


MSettingsIterator MSectionsIterator::getSettingsIterator () const {
    if (!mCurrentIsElement) {
        throw MKeyFileOutOfRangeException();
    }

    return MSettingsIterator(mCurrent->second->begin(),
                             mCurrent->second->end(),
                             *mCurrent->second);
}



MKeyFile::MKeyFile (const std::string &filename) {
    std::ifstream file(filename.c_str());

    if (file.bad() || file.eof() || file.fail()) {
        throw file_not_found(std::string("MKeyFile: failed to load ")
                             + filename);
    }

    std::string str;
    std::shared_ptr<MSettings> current_settings(new MSettings());
    std::string current_section_name("");

    mSections.insert(
        MSections::value_type(current_section_name,
                              current_settings));

    while(!StringUtils::safeGetline(file, str).eof() || !file.fail()) {
        str = StringUtils::trim(str, " \t");

        if (str.length() > 0 && str.at(0) != '#') {
            if (str.at(0) == '[' && str.at(str.length() - 1) == ']') {
                current_section_name = str.substr(1, str.length() - 2);
                current_settings.reset(new MSettings());

                mSections.insert(
                            MSections::value_type(current_section_name,
                                                  current_settings));
            }
            else {
                std::string::size_type separator_pos =
                        str.find_first_of(" \n\0", 0);
                std::string val_name = str.substr(0, separator_pos);
                std::string val =
                        (separator_pos + 1 == std::string::npos
                         || separator_pos == std::string::npos)
                        ? ""
                        : str.substr(separator_pos + 1);

                current_settings->insert(
                            MSettings::value_type(val_name, val));
            }
        }
    }
}




MKeyFile::~MKeyFile () {
}


MSectionsIterator MKeyFile::getSectionsIterator () const{
    return MSectionsIterator(mSections.cbegin(), mSections.cend());
}

size_t MKeyFile::sectionCount (const std::string &pSectionName) const {
    return mSections.count(pSectionName);
}

MSettingsIterator
MKeyFile::getLastSectionSettings (const std::string &pSectionName) const {
    std::shared_ptr<MSettings> settings(
                (--mSections.upper_bound(pSectionName))->second);
    return MSettingsIterator(settings->begin(),
                             settings->end(),
                             *settings);
}

void MKeyFile::appendKey (const std::string &section,
                          const std::string &key,
                          const std::string &value) {
    if (mSections.count(section) > 1) {
        throw std::runtime_error(
            std::string("can't append key to ambiguous section ")
            + section);
    }

    std::shared_ptr<MSettings> settings;

    if (mSections.find(section) == mSections.end()) {
        settings.reset(new MSettings());

        mSections.insert(
            MSections::value_type(section, settings));
    }
    else {
        MSections::const_iterator it = mSections.find(section);
        settings = (*it).second;
    }

    (*settings)[key] = value;
}


void MKeyFile::appendKey (const std::string &key, const std::string &value) {
    assert (!key.empty());
    //assert (!value.empty());

    std::string::size_type separator_pos =
        key.find_first_of(".", 0);

    const std::string section_name =
        (separator_pos + 1 == std::string::npos
         || separator_pos == std::string::npos)
        ? ""
        : key.substr(0, separator_pos);

    const std::string key_name =
        (separator_pos < std::string::npos)
        ? key.substr(separator_pos + 1)
        : key;

    appendKey(section_name, key_name, value);
}


void MKeyFile::writeToFile (const std::string &filename) {
    std::ofstream file(filename.c_str());
    MSectionsIterator sections = getSectionsIterator();

    for (;sections.isElement(); sections.peekNext()) {
        if (!sections.getName().empty()) {
            file << "[" << sections.getName() << "]" << std::endl;
        }

        MSettingsIterator settings = sections.getSettingsIterator();

        for (;settings.isElement(); settings.peekNext()) {
            file << settings.getName() << " "
                 << settings.getValue()
                 << std::endl;
        }
    }
}
