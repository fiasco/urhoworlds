// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com
#include "AppException.hpp"

AppException::~AppException () throw() {
}


const char *AppException::what () const throw() {
    return this->getFullDescription().c_str();
}



SimpleException::SimpleException (const std::string &pDescription)
    : description (pDescription) {
}

const std::string SimpleException::getFullDescription () const {
    return this->description;
}
