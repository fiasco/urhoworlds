#pragma once


#include <Urho3D/Core/Object.h>
#include <Urho3D/SystemUI/Console.h>
#include <Urho3D/Graphics/GraphicsEvents.h>
#include <Urho3D/Graphics/CustomGeometry.h>
#include <Urho3D/Core/CoreEvents.h>
#include <Urho3D/Urho2D/Drawable2D.h>
#include <Urho3D/Graphics/DebugRenderer.h>



class FrameTimeGraph : public Urho3D::Object {
    URHO3D_OBJECT(FrameTimeGraph, Urho3D::Object);
public:
    explicit FrameTimeGraph (Urho3D::Context *pContext)
        : Urho3D::Object (pContext),
          _data(300, 0.f) {
    }

    void init (Urho3D::Node *pCameraNode,
               const bool pEnabled) {
        _cameraNode = pCameraNode;
        _dbgRenderer = _cameraNode->GetScene()->GetComponent<DebugRenderer>();

        setEnabled(pEnabled);

        _screenWidth = float(GetSubsystem<Graphics>()->GetWidth());
    }

    void render (float pVal) {
        _pos += 1;

        if (_pos >= _data.size()) {
            _pos = 0;
        }

        _data[_pos] = pVal;

        const float initial_pos =
            (_screenWidth / 2.f - _data.size() / 2.f) * PIXEL_SIZE;

        for (unsigned short i = 0; i < _data.size(); ++i) {
            const auto p0 = Vector3(initial_pos + i * PIXEL_SIZE,
                                    _data[i] * PIXEL_SIZE,
                                    0);
            const auto p1 = Vector3(initial_pos + i * PIXEL_SIZE,
                                    0,
                                    0);
            _dbgRenderer->AddLine(p0, p1, Color::WHITE);
        }

        const auto p0 = Vector3(initial_pos + _pos * PIXEL_SIZE,
                                _data[_pos] * PIXEL_SIZE,
                                0);
        const auto p1 = Vector3(initial_pos + _pos * PIXEL_SIZE,
                                0,
                                0);
        _dbgRenderer->AddLine(p0, p1, Color::RED);

        drawHorizLine(1000.f/144.f, Color::GREEN);
        drawHorizLine(1000.f/60.f, Color::YELLOW);
        drawHorizLine(1000.f/30.f, Color(1.f, 165.f/255.f, 0.f));
    }

    void drawHorizLine (const float pHeight, const Color &pColor) {
        const float initial_pos = (_screenWidth / 2.f - _data.size() / 2.f) * PIXEL_SIZE;
        _dbgRenderer->AddLine(Vector3(initial_pos,
                                      pHeight * PIXEL_SIZE,
                                      0),
                              Vector3(initial_pos + _data.size() * PIXEL_SIZE,
                                      pHeight * PIXEL_SIZE,
                                      0),
                              pColor);
    }

    void handlePostrenderUpdate (StringHash /*eventType*/,
                                 VariantMap &eventData) {
        if (!_enabled) {
            URHO3D_LOGWARNING("FrameTimeGraph::handlePostrenderUpdate is"
                              " called even though we unsubscribed?");
            return;
        }

        const float secs = eventData[Urho3D::PostRenderUpdate::P_TIMESTEP].GetFloat();
        render(secs * 1000.f);
    }


    void setEnabled (const bool pEnabled) {
        if (pEnabled == _enabled) {
            return;
        } else if (!pEnabled) {
            UnsubscribeFromEvent(E_POSTRENDERUPDATE);
        } else {
            SubscribeToEvent(E_POSTRENDERUPDATE,
                             URHO3D_HANDLER(FrameTimeGraph, handlePostrenderUpdate));
            SubscribeToEvent(E_SCREENMODE,
                             URHO3D_HANDLER(FrameTimeGraph, handleScreenMode));
        }
        _enabled = pEnabled;
    }

    void handleScreenMode (StringHash /*eventType*/, VariantMap& /*eventData*/) {
        _screenWidth = float(GetSubsystem<Graphics>()->GetWidth());
    }

private:
    Urho3D::Node *_cameraNode = nullptr;
    Urho3D::CustomGeometry *cg = nullptr;
    DebugRenderer* _dbgRenderer = nullptr;

    std::vector<float> _data;
    float _screenWidth = 0.f;

    unsigned short _pos = 0;
    bool _enabled = false;
};
