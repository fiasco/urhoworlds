 // This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com
#include "Landscape.hpp"

#include <random>

#include <Urho3D/Core/CoreEvents.h>
#include <Urho3D/Core/Profiler.h>
#include <Urho3D/Core/WorkQueue.h>
#include <Urho3D/Graphics/Material.h>
#include <Urho3D/Graphics/StaticModel.h>
#include <Urho3D/Graphics/StaticModelGroup.h>
#include <Urho3D/Graphics/Geometry.h>
#include <Urho3D/IO/Log.h>
#include <Urho3D/Resource/ResourceCache.h>
#include <Urho3D/UI/BorderImage.h>
#include <Urho3D/UI/Text.h>
#include <Urho3D/UI/UI.h>

#include <VcppBits/MathUtils/MathUtils.hpp>

#include "TreeGenerator.hpp"
#include "Impostor.hpp"
#include "UrhoworldsDebug.hpp"
#include "UrhoUtils.hpp"

using namespace Urho3D;

using namespace VcppBits::MathUtils;

namespace {


btIndexedMesh landscape_create_phys_mesh (const SingleModelLoadWorkerData *dt) {
    btIndexedMesh mesh;

    mesh.m_numTriangles        = int(dt->physics_data->index_data->size());
    mesh.m_triangleIndexBase   =
        (const unsigned char *) dt->physics_data->index_data->data();
    mesh.m_triangleIndexStride = 3 * sizeof(unsigned short);
    mesh.m_numVertices         = int(dt->physics_data->vertex_data->size());
    mesh.m_vertexBase          =
        (const unsigned char *)dt->physics_data->vertex_data->data();
    mesh.m_vertexStride        = sizeof(Urho3D::Vector3);

    return mesh;
}

void landscape_load_physics (SingleModelLoadWorkerData* dt) {
    dt->physics_data->index_data = &dt->lod0_index_view;

    dt->physics_data->vertex_data->reserve(dt->lod0_vertex_view.size());
    for (size_t i = 0; i < dt->lod0_vertex_view.size(); ++i) {
        auto el = dt->lod0_vertex_view[i];
        dt->physics_data->vertex_data->push_back(el.coord);
    }

    dt->physics_data->bt_mesh.addIndexedMesh(landscape_create_phys_mesh(dt),
                                             PHY_SHORT);
    dt->physics_data->bt_shape =
        std::make_unique<btBvhTriangleMeshShape>(&dt->physics_data->bt_mesh,
                                                 true);
}

void landscape_load_trees_part (SingleModelLoadWorkerData *dt, int i, int j) {
    constexpr int num_ii = 2; // up to 3 trees per 10 meters
    constexpr int num_jj = num_ii;

    constexpr float incr = 10.f / ((float) num_ii);
    constexpr float half_incr = incr / 2.f;

    constexpr float threshold = 0.25f;
    const int seed = int(dt->nz->get_noize(float(i) + dt->pos.x_, float(j) + dt->pos.y_) * 1000.f);
    std::mt19937 mt (seed);

    for (int ii = 0; ii < num_ii; ++ii) {
        for (int jj = 0; jj < num_jj; ++jj) {

            std::uniform_real_distribution<float> dist (-half_incr, half_incr);
            const float x_offset = i + ii * incr + dist(mt);
            const float actual_x = x_offset + dt->pos.x_;

            const float y_offset = j + jj * incr + dist(mt);
            const float actual_y = y_offset + dt->pos.y_;

            std::uniform_real_distribution<float> scale_dist (0.5f, 1.5f);
            const float scale = scale_dist(mt);

            float forest_noise = dt->nz->get_forest_noize(actual_x, actual_y);
            if (forest_noise * scale_dist(mt) < threshold) {
                continue;
            }

            std::uniform_real_distribution<float> scale_xy_dist (0.85f, 1.15f);
            const float scale_side1 = scale_xy_dist(mt);
            const float scale_side2 = scale_xy_dist(mt);

            std::uniform_real_distribution<float> rot_dist (0.f, 345.f);

            const float rot = rot_dist(mt);

            const Quaternion rott = Quaternion(rot, Vector3(0, 1, 0));

            const float z_coord = dt->nz->get_noize(actual_x, actual_y) + 1;

            Matrix3x4 trans;

            trans.SetTranslation(Vector3(x_offset, z_coord, y_offset));
            trans.SetScale(Vector3(scale_side1, scale, scale_side2));
            trans.SetRotation(rott.RotationMatrix());
            dt->trees.push_back(trans);
        }
    }
}

void landscape_load_trees (SingleModelLoadWorkerData *dt) {
    const int init_i = roundUp(dt->pos.x_, 10) - dt->pos.x_;
    const int init_j = roundUp(dt->pos.y_, 10) - dt->pos.y_;

    for (int i = init_i; i < dt->size; i += 10) {
        for(int j = init_j; j < dt->size; j += 10) {
            // for every 10x10 meters piece
            landscape_load_trees_part(dt, i, j);
        }
    }
}

void landscape_load_single_piece (SingleModelLoadWorkerData* dt,
                                  const bool &pCancel) {
    if (pCancel) { // cancel job, cleaned up in
        return;    // Landscape::processWorkItemCompleted
    }

    dt->bounds0 = { 0.f, 0.f, 0.f };
    dt->bounds1 =
        { static_cast<float>(dt->size), 0.f, static_cast<float>(dt->size) };
    dt->position =
        { static_cast<float>(dt->pos.x_), 0.0f, static_cast<float>(dt->pos.y_) };

    create_lod_0_plane(*(dt->nz),
                       (unsigned char *)dt->lod0_vertex_view.data(),
                       (unsigned char *)dt->lod0_index_view.data(),
                       dt->bounds0.y_,
                       dt->bounds1.y_,
                       static_cast<float>(dt->size)/dt->resolution,
                       dt->resolution,
                       dt->resolution,
                       dt->pos);

    if (pCancel) { // cancel job, cleaned up in
        return;    // Landscape::processWorkItemCompleted
    }

    if (dt->resolution == dt->size) {
        create_lod_1_plane(*(dt->nz),
                           &dt->lod1_vertex_view,
                           &dt->lod1_index_view,
                           dt->size,
                           dt->size,
                           dt->pos);
        landscape_load_physics(dt);
        landscape_load_trees(dt);
    }
}

void landscape_load_piece (const WorkItem* item, unsigned /*threadIndex*/) {
    ModelLoadWorkerData *dt = static_cast<ModelLoadWorkerData*>(item->start_);

    if (dt->type == ModelLoadWorkerData::TYPE::Single) {
        landscape_load_single_piece((SingleModelLoadWorkerData*)dt->ptr,
                                    dt->cancel);
    } else {
        auto* multi_dt = (MultiModelLoadWorkerData*)dt->ptr;
        landscape_load_single_piece(multi_dt->tasks[0], dt->cancel);
        landscape_load_single_piece(multi_dt->tasks[1], dt->cancel);
        landscape_load_single_piece(multi_dt->tasks[2], dt->cancel);
        landscape_load_single_piece(multi_dt->tasks[3], dt->cancel);
    }
}




} // namespace (anonymous)


Landscape::Landscape (Urho3D::Context *pContext)
    : Urho3D::Object(pContext) {
}

void Landscape::init (Settings *pSettings,
                      Settings2 *pWorldConfig,
                      Node *pCameraNode) {
    _cfg = pSettings;
    _worldCfg = pWorldConfig;
    _cameraNode = pCameraNode;
    SubscribeToEvent(E_UPDATE, URHO3D_HANDLER(Landscape, processUpdateEvent));

    debugDrawSettingUpdated(); // sets material debug draw to sync with config

    SubscribeToEvent(E_WORKITEMCOMPLETED,
                     URHO3D_HANDLER(Landscape, processWorkItemCompleted));

    initSettings();
}



void Landscape::initSettings () {
    auto listener_func = std::bind(&Landscape::landscapeSettingUpdated, this);
    for (auto &set : _worldCfg->getSection("lscp")) {
        auto &a = set.second;
        a->addSimpleUpdateHandler(this, listener_func);
    }
    _cfg->getSetting("debug_draw")
        .addUpdateHandler(this,
                          std::bind(&Landscape::debugDrawSettingUpdated, this));

    landscapeSettingUpdated();
}

void validate_setting_to_be_odd_int (Setting2& set) {
    const int num = set.get<V2::IntValue>();
    if ((num % 2) != 1) {
        set.set<V2::IntValue>(num - 1);
        URHO3D_LOGWARNING("tile size must be odd number, resetting to "
                          + ea::to_string(num - 1));
    }
}

void Landscape::landscapeSettingUpdated () {
    validate_setting_to_be_odd_int(_worldCfg->getSetting("lscp.tile_size"));

    for (auto &el : _currentWorks) {
        el.second.cancel = true;
    }

    _mapTileSize = size_t(_worldCfg->get<V2::IntValue>("lscp.tile_size"));

    auto num_outer_tiles = _worldCfg->get<V2::IntValue>("lscp.outer_tiles_num");
    const size_t num_outer_levels =
        static_cast<size_t>(_worldCfg->get<V2::IntValue>("lscp.outer_levels_num"));

    auto num_tiles = _worldCfg->get<V2::IntValue>("lscp.tiles_num");

    if (_landscapeData.size() < num_outer_levels + 1) {
        _landscapeData.resize(num_outer_levels + 1);
    }

    size_t prev_level_tiles_num = 0;
    for (auto &el : _landscapeData) {
        for (auto &position : el.positions_data) {
            _cameraNode->GetScene()->RemoveChild(position.second.first);
        }

        el.positions_data.clear();

        // we could avoid setting up the unneeded managers if we did two loops,
        // but these operations are not too expensive, so to hell with that...
        const size_t num_t = (prev_level_tiles_num / 2 + size_t(num_outer_tiles)) / 2 * 2;
        el.manager.setNumTilesToRender(num_t);
        el.manager.setNumExcludedTilesToRender(prev_level_tiles_num / 2);
        el.manager.reinit();

        prev_level_tiles_num = num_t;
    }

    _landscapeData[0].manager.setNumTilesToRender(size_t(num_tiles));
    _landscapeData[0].manager.setNumExcludedTilesToRender(0);

    _landscapeData.resize(num_outer_levels + 1);

    const std::vector<std::tuple<FastNoise*, std::string, float*, float*>> noizes_to_process 
        = { { &_nz._noize1, "lscp.noize1", &_nz._ampl1, &_nz._exp1 },
            { &_nz._noize2, "lscp.noize2", &_nz._ampl2, &_nz._exp2 },
            { &_nz._noize3, "lscp.noize3", &_nz._ampl3, &_nz._exp3 },
            { &_nz.color1, "lscp.noize_color1", nullptr, nullptr },
            { &_nz.color2, "lscp.noize_color2", nullptr, nullptr },
            { &_nz.color3, "lscp.noize_color3", nullptr, nullptr },
    };

    for (auto el : noizes_to_process) {
        auto set_prefix = std::get<1>(el);
        auto noize = std::get<0>(el);
        auto ampl = std::get<2>(el);
        auto exp_ = std::get<3>(el);
        noize->SetFrequency(_worldCfg->get<V2::FloatValue>(set_prefix + "_freq"));
        const auto t = static_cast<FastNoise::NoiseType>(_worldCfg->getSetting(set_prefix + "_type").getEnumPos<V2::EnumStringValue>());
        noize->SetNoiseType(t);

        const auto interp = static_cast<FastNoise::Interp>(_worldCfg->getSetting(set_prefix + "_interp").getEnumPos<V2::EnumStringValue>());
        noize->SetInterp(interp);

        const auto frac_type = static_cast<FastNoise::FractalType>(_worldCfg->getSetting(set_prefix + "_frac_type").getEnumPos<V2::EnumStringValue>());
        noize->SetFractalType(frac_type);

        noize->SetFractalOctaves(_worldCfg->get<V2::IntValue>(set_prefix + "_frac_octaves"));
        noize->SetFractalLacunarity(_worldCfg->get<V2::FloatValue>(set_prefix + "_frac_lacunarity"));
        noize->SetFractalGain(_worldCfg->get<V2::FloatValue>(set_prefix + "_frac_gain"));

        exp_ && (*exp_ = float(_worldCfg->get<V2::IntValue>(set_prefix + "_exponent")));

        ampl && (*ampl = _worldCfg->get<V2::FloatValue>(set_prefix + "_ampl"));
    }

    _nz._color_noise_amt = _worldCfg->get<V2::FloatValue>("lscp.color_noise_amt");
    _nz._color_dither_amt = _worldCfg->get<V2::FloatValue>("lscp.color_dither_amt");

    _landscapePhysicsData.clear();

    start_load_time = GetSubsystem<Time>()->GetElapsedTime();
    first_load_time = 0.f;

    _waitingToFinishTasksBeforeRestart = true;
}

void Landscape::debugDrawSettingUpdated () {
    Urho3D::FillMode mode =
        _cfg->getSetting("debug_draw").getBool() ? FILL_WIREFRAME : FILL_SOLID;

    GetSubsystem<ResourceCache>()
        ->GetResource<Material>("Landscape/Landscape.xml")->SetFillMode(mode);

    for (auto &el : _landscapeData) {
        for (auto &position : el.positions_data) {
            auto *node_ptr = position.second.first;
            if (_cfg->getSetting("debug_draw").getBool()) {
                if (!node_ptr->GetChildren().size()) {
                    UrhoUtils::displayTextOnNode(node_ptr,
                                                 node_ptr->GetName(),
                                                 "dbg_name");
                }
            } else {
                node_ptr->RemoveChild(node_ptr->GetChild("dbg_name"));
            }
        }
    }
}


bool Landscape::isUpdateNeeded () {
    for (auto &el : _landscapeData) {
        if (el.manager.getAllElementsToLoad().size() != 0
            || el.manager.getAllElementsToUnload().size() != 0) {
            return true;
        }
    }
    return false;
}


void Landscape::processUpdateEvent (StringHash /*eventType*/,
                                    VariantMap& /*eventData*/) {
    URHO3D_PROFILE("LandscapeTilesUpdate");

    if (_waitingToFinishTasksBeforeRestart) {
        if (_currentWorks.size()) {
            return;
        } else {
            _waitingToFinishTasksBeforeRestart = false;
        }
    }

    for (size_t i = 0; i < _landscapeData.size(); ++i) {
        auto &el = _landscapeData[i];
        const size_t tile_size = pow(2, i) * _mapTileSize;

        auto pos = _cameraNode->GetPosition();
        pos.x_ = landscape_position_divide(pos.x_, tile_size);
        pos.z_ = landscape_position_divide(pos.z_, tile_size);

        el.manager.setCurrentCameraTransform(pos, Vector3());
    }


    if (isUpdateNeeded()) {
        fireUpTileUpgradeTasks();
        if (_currentWorksMulti.empty()) {
            removeTilesOutsideRange();
            updateCurrentWorksTiles();
            startJobsForNeededTiles();
        }
    } else {
        if (compareFloats(first_load_time, 0.f)) {
            first_load_time = GetSubsystem<Time>()->GetElapsedTime()
                - start_load_time;
            URHO3D_LOGINFO("Load time: " + ea::to_string(first_load_time) + "\n");
            URHO3D_LOGINFO("UrhoworldsMemory realllocations: "
                           + ea::to_string(UrhoworldsDebug::get_simple_vector_reallocations()) + "\n");
        }
    }
}

void Landscape::fireUpTileUpgradeTasks () {
    constexpr size_t max_updates_ongoing = 20;
    size_t tasks_started = 0;

    for (size_t i = 0; i < _landscapeData.size(); ++i) {
        auto &el = _landscapeData[i];
        const size_t tile_size = pow(2, i) * _mapTileSize;
        const size_t dest_tile_size = tile_size / 2;

        auto &to_exclude = el.manager.getAllExcludedElementsToUnload();

        for (auto unl_el: to_exclude) {
            if (tasks_started >= max_updates_ongoing) {
                return;
            }

            auto base_pos = unl_el * tile_size;

            if (_currentWorks.find(base_pos) != _currentWorks.end()) {
                // already being loaded
                continue;
            }

            auto& prev_mgr = _landscapeData[i - 1].manager;

            bool not_needed =
                prev_mgr.getTileIsNeeded(base_pos / dest_tile_size)
                != LandscapeManager::TileIsNeeded::needed;
            bool already_loaded =
                prev_mgr.getTileIsLoaded(base_pos / dest_tile_size);

            if (not_needed || already_loaded) {
                continue;
            }

            const auto pos2 = base_pos + Urho3D::IntVector2(0, dest_tile_size);
            const auto pos3 = base_pos + Urho3D::IntVector2(dest_tile_size, 0);
            const auto pos4 = base_pos + Urho3D::IntVector2(dest_tile_size,
                                                            dest_tile_size);

            if (prev_mgr.getTileIsNeeded(pos2 / dest_tile_size)
                != LandscapeManager::TileIsNeeded::needed) {
                continue;
            }
            if (prev_mgr.getTileIsNeeded(pos3 / dest_tile_size)
                != LandscapeManager::TileIsNeeded::needed) {
                continue;
            }
            if (prev_mgr.getTileIsNeeded(pos4 / dest_tile_size)
                != LandscapeManager::TileIsNeeded::needed) {
                continue;
            }

            MultiModelLoadWorkerData &dt =
                (*_currentWorksMulti.emplace(
                    std::make_pair(base_pos,
                                   MultiModelLoadWorkerData()))).second;

            const unsigned resolution = _mapTileSize;

            dt.tasks[0] = allocateSingleModelLoadWorkerData(
                context_, base_pos, dest_tile_size, resolution);
            dt.tasks[1] = allocateSingleModelLoadWorkerData(
                context_, pos2, dest_tile_size, resolution);
            dt.tasks[2] = allocateSingleModelLoadWorkerData(
                context_, pos3, dest_tile_size, resolution);
            dt.tasks[3] = allocateSingleModelLoadWorkerData(
                context_, pos4, dest_tile_size, resolution);

            auto &curr_work_item =
                (*_currentWorks.emplace(
                    std::make_pair(base_pos,
                                   ModelLoadWorkerData()))).second;
            curr_work_item.type = ModelLoadWorkerData::TYPE::Multi;
            curr_work_item.pos = base_pos;

            curr_work_item.ptr = &dt;
            curr_work_item.level = i - 1;

            SharedPtr<WorkItem> itm(new WorkItem());

            itm->sendEvent_ = true;
            itm->workFunction_ = landscape_load_piece;
            itm->start_ = &curr_work_item;
            itm->aux_ = context_;
            itm->priority_ = 0;

            GetSubsystem<WorkQueue>()->AddWorkItem(itm);

            ++tasks_started;
        }
    }
}

void Landscape::unloadTile (const size_t pLevel,
                            const Urho3D::IntVector2& pWorldPos,
                            const bool pUpdateLandscapeManager) {
    auto &pos_data = _landscapeData[pLevel].positions_data;
    const size_t tile_size = pow(2, pLevel) * _mapTileSize;

    _cameraNode->GetScene()->RemoveChild(pos_data.at(pWorldPos).first);

    const auto &work_item = pos_data.at(pWorldPos).second;
    if (work_item.lod1.indexBuffer.NotNull()) {
        _freeWorksSingleWithPhysics.push_back(
            std::move(pos_data.at(pWorldPos).second));
    } else {
        _freeWorksSingle.push_back(
            std::move(pos_data.at(pWorldPos).second));
    }
    pos_data.erase(pWorldPos);

    if (pLevel == 0) {
        _landscapePhysicsData.erase(pWorldPos);
    }

    if (pUpdateLandscapeManager) {
        _landscapeData[pLevel].manager.setTileIsUnloaded(pWorldPos / tile_size);
    }
}

void Landscape::removeTilesOutsideRange () {
    // clean up the map frum the positions that are too far away
    for (size_t i = 0; i < _landscapeData.size(); ++i) {
        auto &el = _landscapeData[i];
        const size_t tile_size = pow(2, i) * _mapTileSize;

        auto to_unload = el.manager.getAllElementsToUnload();

        for (auto unl_el : to_unload) {
            unloadTile(i, unl_el * tile_size);
        }
        el.manager.clearAllTilesToUnload();
    }
}

// TODO: figure out a way to uncomment actual cancelling here
void Landscape::updateCurrentWorksTiles () {
    for (auto& el : _currentWorks) {
        // TODO: also cancel MultiModel jobs?
        if (el.second.type == ModelLoadWorkerData::TYPE::Single) {
            SingleModelLoadWorkerData* dtt =
                (SingleModelLoadWorkerData*) el.second.ptr;

            size_t tile_size = pow(2, el.second.level) * _mapTileSize;

            if (dtt->size == tile_size) {
                if (_landscapeData[el.second.level].manager.getTileIsNeeded(
                        dtt->pos / int(tile_size))
                    != LandscapeManager::TileIsNeeded::needed) {
                    el.second.cancel = true;
                }
            }
        }
    }
}

constexpr size_t _tasks_to_keep = 6;
constexpr size_t _tasks_to_fire_up = 1;

void Landscape::startJobsForNeededTiles () {
    size_t sent_to_load = 0;

    for (size_t i = 0; i < _landscapeData.size(); ++i) {
        auto &ell = _landscapeData[i];
        const size_t tile_size = pow(2, i) * _mapTileSize;
        const auto tiles = ell.manager.getAllElementsToLoad();
        // TODO add LandscapManager::getNextElementToLoad() and use that
        for (auto el : tiles) {
            auto pos = el * tile_size;
            // may remove the first check?
            if ((_currentWorks.find(pos) != _currentWorks.end())
                || (_currentWorksSingle.find(pos) != _currentWorksSingle.end())) {
                continue;
            }
            // fire up the jobs

            if (_currentWorks.size() < _tasks_to_keep) {
                createSingleWorkItem(pos, tile_size, _mapTileSize, 0);
            } else {
                break;
            }
            if (sent_to_load > _tasks_to_fire_up) {
                break;
            }
            ++sent_to_load;
        }
    }

}

SingleModelLoadWorkerData*
Landscape::allocateSingleModelLoadWorkerData (Urho3D::Context* pContext,
                                              const Urho3D::IntVector2& pos_i,
                                              const unsigned pSize,
                                              const unsigned pResolution) {
    // TODO: consider cancelling a running work, or what?
    if (_currentWorksSingle.find(pos_i) != _currentWorksSingle.end()) {
        // so, we already created a task for this tile, but now it's obsolete
        // somehow?
    }

    SingleModelLoadWorkerData *curr_work {};

    // TODO20: if tile size shrinks, we have a problem

    if (pSize == pResolution && _freeWorksSingleWithPhysics.size()) {
        curr_work = &_currentWorksSingle.emplace(
                pos_i, std::move(_freeWorksSingleWithPhysics.back()))->second;
        _freeWorksSingleWithPhysics.pop_back();
    }

    if (pSize > pResolution && _freeWorksSingle.size()) {
        curr_work = &_currentWorksSingle.emplace(
                pos_i, std::move(_freeWorksSingle.back()))->second;
        _freeWorksSingle.pop_back();
    }

    if (curr_work) {
        curr_work->size = pSize;
        curr_work->resolution = pResolution;
        curr_work->pos = pos_i;

        //curr_work->lod0_index_view.nullify();
        //curr_work->lod0_vertex_view.nullify();
        curr_work->lod1_index_view.nullify();
        curr_work->lod1_vertex_view.nullify();
    } else {
        curr_work =
            &(*_currentWorksSingle.emplace(
                  pos_i,
                   SingleModelLoadWorkerData(& _nz,
                                             pContext,
                                             pSize,
                                             pResolution,
                                             pos_i ))).second;
    }

    if (pResolution == pSize) {
        curr_work->physics_data = std::make_unique<UrhoworldsPhysicsData>();
        curr_work->physics_data->vertex_data = cachePhysicsVertex.createElement();
        curr_work->physics_data->vertex_data->resize(0);
    }

    return curr_work;
}

void Landscape::createSingleWorkItem (const Urho3D::IntVector2& pos_i,
                                      const unsigned pSize,
                                      const unsigned pResolution,
                                      const unsigned priority) {

    // TODO: cleanup
    if (_currentWorks.find(pos_i) != _currentWorks.end()) {
        throw std::runtime_error("Landscape::createSingleWorkItem(): "
                                 "work is already in process");
    }

    auto &curr_work_item =
        (*_currentWorks.emplace(std::make_pair(pos_i, ModelLoadWorkerData()))).second;
    curr_work_item.type = ModelLoadWorkerData::TYPE::Single;
    curr_work_item.pos = pos_i;
    auto *curr_work = allocateSingleModelLoadWorkerData(context_,
                                                        pos_i,
                                                        pSize,
                                                        pResolution);
    curr_work_item.ptr = curr_work;

    SharedPtr<WorkItem> itm(new WorkItem());

    itm->sendEvent_ = true;
    itm->workFunction_ = landscape_load_piece;
    itm->start_ = &curr_work_item;
    itm->aux_ = context_;
    itm->priority_ = priority;

    GetSubsystem<WorkQueue>()->AddWorkItem(itm);
}


Urho3D::Node* Landscape::loadTile (SingleModelLoadWorkerData* dt,
                                   const size_t pLevel) {
    auto model =
        create_2lod_plane(context_, dt->lod0, dt->lod1, dt->bounds0, dt->bounds1);

    auto node = _cameraNode->GetScene()->CreateChild(dt->pos.ToString());

    node->SetPosition(dt->position);

    auto object = node->CreateComponent<StaticModel>();
    object->SetModel(model);

    if (_cfg->getSetting("debug_draw").getBool()) {
        UrhoUtils::displayTextOnNode(node,
                                     ea::to_string(pLevel)
                                     + " "
                                     + dt->pos.ToString(),
                                     "dbg_name");
    }

    object->SetMaterial(GetSubsystem<ResourceCache>()
                        ->GetResource<Material>("Landscape/Landscape.xml"));

    RigidBody* body = node->CreateComponent<RigidBody>();

    body->GetBody()->setCollisionShape(dt->physics_data->bt_shape.get());

    _landscapePhysicsData[dt->pos] = std::move(dt->physics_data);


    if (!_cfg->get<bool>("dbg.notrees")) {
        if (false) {
            // Group optimization doesn't seem to bring much benefi eh
            Node* treesNode = node->CreateChild("TreeGroup");
            auto treesGroup = treesNode->CreateComponent<StaticModelGroup>();
            auto crownsGroup = treesNode->CreateComponent<StaticModelGroup>();

            auto tree = buildTree(treesNode);
            treesGroup->SetModel(std::get<0>(tree));
            crownsGroup->SetModel(std::get<1>(tree));
            crownsGroup->SetMaterial(std::get<2>(tree));

            for (auto &el : dt->trees) {
                Node* aNode = node->CreateChild("Tree");
                aNode->SetTransform(el);

                //boxNodes_.Push(SharedPtr<Node>(boxNode));
                treesGroup->AddInstanceNode(aNode);
                crownsGroup->AddInstanceNode(aNode);
            }
        } else {
            auto tree = buildTree(node);

            for (auto& el : dt->trees) {
                auto aNode = node->CreateChild("Tree");
                aNode->SetTransform(el);
                auto tree_mdl = aNode->CreateComponent<StaticModel>();
                tree_mdl->SetModel(std::get<0>(tree));
                tree_mdl->SetMaterial(std::get<2>(tree));
                auto crown_mdl = aNode->CreateComponent<StaticModel>();
                crown_mdl->SetModel(std::get<1>(tree));
                crown_mdl->SetMaterial(std::get<3>(tree));

                if (!_tree_imp) {
                    _tree_imp = aNode->CreateComponent<Impostor>();
                    auto material = _tree_imp->GetMaterial();
                    material->SetShaderParameter(
                        "AmbientColor",
                        std::get<3>(tree)->GetShaderParameter("AmbientColor"));
                    material->SetShaderParameter(
                        "MatSpecColor",
                        std::get<3>(tree)->GetShaderParameter("MatSpecColor"));
                    material->SetShaderParameter(
                        "MatDiffColor",
                        std::get<3>(tree)->GetShaderParameter("MatDiffColor"));

                    // _tree_imp->GetMaterial()
                    //     ->SetShaderParameter("MatSpecColor",
                    //                          Urho3D::Color(0.4, 0.4, 0.2, 100.0));
                }
                else {
                    aNode->AddComponent(_tree_imp->Clone(), 0, REPLICATED);
                }
            }
        }
    }

    return node;
}

Urho3D::Node* Landscape::loadOuterTile (SingleModelLoadWorkerData* dt,
                                        const size_t pLevel) {
    auto model = create_1lod_plane(context_,
                                   dt->lod0,
                                   dt->bounds0,
                                   dt->bounds1);

    auto node =
        _cameraNode->GetScene()->CreateChild(dt->pos.ToString());

    node->SetPosition(dt->position);

    auto object = node->CreateComponent<StaticModel>();
    object->SetModel(model);

    if (_cfg->getSetting("debug_draw").getBool()) {
        UrhoUtils::displayTextOnNode(node,
                                     ea::to_string(pLevel)
                                     + " "
                                     + dt->pos.ToString(),
                                     "dbg_name");
    }

    object->SetMaterial(GetSubsystem<ResourceCache>()
                        ->GetResource<Material>("Landscape/Landscape.xml"));

    return node;
}

bool Landscape::upgradeTilesAreStillNeeded (const LandscapeLevelData &pLd,
                                            const MultiModelLoadWorkerData *pDt,
                                            const size_t pTileSize) const {
    for (const auto &task : pDt->tasks) {
        if ((pLd.manager.getTileIsNeeded(task->pos / pTileSize)
             != LandscapeManager::TileIsNeeded::needed)) {
            return false;
        }

        if (pLd.positions_data.count(task->pos)) {
            // TODO: find the cause of this and fix it
            URHO3D_LOGINFO("WARNING: redundant upgrade");
            return false;
        }
    }
    return true;
}

void Landscape::loadTileUpgrade (const MultiModelLoadWorkerData *pDt,
                                 const size_t pTileSize,
                                 const size_t pLevel) {
    auto &pos_data = _landscapeData[pLevel].positions_data;
    auto &mgr = _landscapeData[pLevel].manager;

    for (auto &el : pDt->tasks) {
        auto new_ptr = moveWorkItemToPositionsList(el, pLevel);

        if (pTileSize == _mapTileSize) {
            pos_data.at(new_ptr->pos).first = loadTile(new_ptr, pLevel);
        } else {
            pos_data.at(new_ptr->pos).first = loadOuterTile(new_ptr, pLevel);
        }

        mgr.setTileIsLoaded(new_ptr->pos / pTileSize);
    }
}

void Landscape::processWorkItemCompleted (StringHash /*eventType*/,
                                          VariantMap& eventData) {
    WorkItem *item =
        static_cast<WorkItem*>(eventData[WorkItemCompleted::P_ITEM].GetPtr());
    ModelLoadWorkerData *dt = static_cast<ModelLoadWorkerData*>(item->start_);

    if (!dt->cancel) {
        if (dt->type == ModelLoadWorkerData::TYPE::Single) {
            SingleModelLoadWorkerData* dtt =
                (SingleModelLoadWorkerData*) dt->ptr;

            for (size_t i = 0; i < _landscapeData.size(); ++i) {
                auto &el = _landscapeData[i];
                const size_t tile_size = pow(2, i) * _mapTileSize;

                if (el.manager.getTileIsNeeded(dt->pos / tile_size)
                    == LandscapeManager::TileIsNeeded::needed) {
                    if (dtt->size == tile_size) {
                        auto new_ptr = moveWorkItemToPositionsList(dtt, i);
                        if (tile_size == _mapTileSize) {
                            el.positions_data.at(dt->pos).first =
                                loadTile(new_ptr, i);
                        } else {
                            el.positions_data.at(dt->pos).first =
                                loadOuterTile(new_ptr, i);
                        }

                        el.manager.setTileIsLoaded(dt->pos / tile_size);
                        ++_numTilesLoadedCounter;

                        break;
                    }
                }
            }
        } else if (dt->type == ModelLoadWorkerData::TYPE::Multi) {
            auto* mdt = (MultiModelLoadWorkerData*) dt->ptr;
            auto &landscape_data = _landscapeData[dt->level];
            const size_t tile_size = pow(2, dt->level) * _mapTileSize;
            auto &prev_pos_data = _landscapeData[dt->level + 1].positions_data;

            if (upgradeTilesAreStillNeeded(landscape_data, mdt, tile_size)) {
                if (prev_pos_data.find(mdt->tasks[0]->pos)
                    != prev_pos_data.end()) {
                    // must unload before loading upgrades, otherwise we can
                    // overwrite stuff in our positions_data list list
                    unloadTile(dt->level + 1, mdt->tasks[0]->pos, true);
                }

                loadTileUpgrade(mdt, tile_size, dt->level);
                _numTilesLoadedCounter +=4;
            } else {
                for (auto &el : mdt->tasks) {
                    _currentWorksSingle.erase(el->pos);
                }
            }
        }
    }

    eraseCurrentWork(dt);
}

void Landscape::eraseCurrentWork (const ModelLoadWorkerData *dt) {
    auto els = _currentWorks.equal_range(dt->pos);
    for (auto it = els.first; it != els.second; ++it) {
        if (&it->second == dt) {
            if (dt->type == ModelLoadWorkerData::TYPE::Single) {
                // if it was loaded, then it would have been moved from
                // _currentWorksSingle to _landscapeData[level].positions_data

                if (dt->cancel) {
                    _currentWorksSingle.erase(dt->pos);
                }
            } else if (dt->type == ModelLoadWorkerData::TYPE::Multi) {
                _currentWorksMulti.erase(dt->pos);
            }
            _currentWorks.erase(it);
            return;
        }
    }
}

SingleModelLoadWorkerData*
Landscape::moveWorkItemToPositionsList (
    SingleModelLoadWorkerData* pWorkItem,
    const size_t pLevel) {

    for (auto it = _currentWorksSingle.begin();
         it != _currentWorksSingle.end();
         ++it) {
        if (pWorkItem == &it->second) {
            auto &pos_data = _landscapeData[pLevel].positions_data;
            //pos_data[it->first].second = std::move(it->second);
            pos_data.emplace(std::make_pair(it->first,
                                            std::make_pair(nullptr,
                                                           std::move(it->second))));
            auto ret = &(pos_data[it->first].second);

            _currentWorksSingle.erase(it);

            return ret;
        }
    }
    throw std::runtime_error("dude"); // TODO remove?
}
