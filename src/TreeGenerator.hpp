#pragma once


#include <vector>
#include <utility>
#include <deque>
#include <random>


#include <Urho3D/Math/Vector3.h>
#include <Urho3D/Graphics/Model.h>
#include <Urho3D/Container/Ptr.h>
#include <Urho3D/Core/Context.h>
#include <Urho3D/IO/FileSystem.h>


#include "Settings/Settings.hpp"


namespace Urho3D {
class Context;
class Scene;
class ResourceCache;
class Model;
class Material;
class StaticModel;
}


class TreeGenerator {
public:
    struct Segment {
        Urho3D::Vector3 vec;
        float endRadius;
    };

    struct Branch {
        Urho3D::Vector3 rootPos;
        float rootRadius;
        std::vector<Segment> segments;
    };

    struct Leaf {
        Urho3D::Vector3 pos;
        Urho3D::Vector3 vec;
        Urho3D::Vector3 norm;
    };

    TreeGenerator ();

    Urho3D::Vector3 getBranchInitialDirection (const int pLevel,
                                               const Urho3D::Vector3 &vec,
                                               const float pOutwardAngle);
    std::tuple<Urho3D::Vector3, Urho3D::Vector3, float>
    find_child_pos (const Branch * pParent, const float pPosition) const;

    Urho3D::Vector3
    find_child_vec (const Branch * pParent,
                    const float pPosition,
                    const float pOutwardAngle,
                    const int pParentLevel);



    Branch* createBranch (const Branch * pParent,
                          const float pPosition,
                          const float pLength,
                          const float pOutwardAngle,
                          const int pParentLevel);
    void fillChildren (const Branch *pBranch,
                       const float pParentLength,
                       const int pParentLevel);

    Leaf* createLeaf (const Branch * pParent,
                      const float pPosition,
                      const float pOutwardAngle,
                      const int pParentLevel);
    void fillLeaves (const Branch * pBranch,
                     const float pLength,      // we pass branch length so that
                     const int pParentLevel);  // we can distribute leaves
                                               // properly


    void init (Urho3D::Context *pContext,
               Urho3D::Scene* pScene,
               Urho3D::ResourceCache *pCache, const bool debugRender);

    void settingUpdated ();

    Settings * getConfig () { return &_cfg; }

    void pauseUpdates (const bool pPause) {
        _pauseUpdates = pPause;
    }

    std::pair<Urho3D::SharedPtr<Urho3D::Model>,
              Urho3D::SharedPtr<Urho3D::Model>> getModels () {
        return { _model, _leafModel };
    }

    bool isInitialized () const { return _isInitialized; }

private:
    void generate();
    void smoothBranch (Branch *pBranch);
    float calcBranchLength (const int pLevel,
                            const float pParentLength,
                            const float pChildPosition) const;

    Urho3D::Node *_sceneNode = nullptr;
    Urho3D::StaticModel *modl;
    Urho3D::StaticModel *leaf_modl;
    
    Urho3D::SharedPtr<Urho3D::Model> _model;
    Urho3D::SharedPtr<Urho3D::Model> _leafModel;
    std::deque<Branch> tree_data;
    std::deque<Leaf> leaf_data;
    std::mt19937 _mt;
    std::mt19937 _mt_lf;

    Urho3D::Context *_context = nullptr;
    Urho3D::Scene* _scene = nullptr;
    Urho3D::ResourceCache *_cache = nullptr;

    Settings _cfg;

    Urho3D::Vector3 _treeCrownCenter;

    bool _pauseUpdates = false;

    Urho3D::BoundingBox bb;

    bool _debugRender = false;
    bool _isInitialized = false;
};


std::tuple<Urho3D::Model*, Urho3D::Model*, Urho3D::Material*, Urho3D::Material*>
buildTree (Urho3D::Node *pNode);

inline void loadTreePresetsList (Urho3D::Context *pContext,
                                 ea::vector<ea::string> &pPresets)  {
    pPresets.clear();
    pContext->GetSubsystem<Urho3D::FileSystem>()->ScanDir(pPresets,
                                                  ".",
                                                  "*.tree.ini",
                                                  Urho3D::SCAN_FILES,
                                                  false);
}
