#pragma once


#include <memory>
#include <vector>


#include "Sample.h"
#include "Console.hpp"
#include "Settings/Settings.hpp"
#include "Landscape.hpp"
#include "FrameTimeGraph.hpp"
#include "StateManager.hpp"
#include "SettingsGui.hpp"
#include "GameplayState.hpp"
#include "UrhoUtils.hpp"

#include "ShaderParameterConfigurator.hpp"


#include <Urho3D/UI/Window.h>
#include <Urho3D/UI/Button.h>
#include <Urho3D/UI/Text.h>
#include <Urho3D/UI/UIEvents.h>
#include <Urho3D/Input/InputEvents.h>


#include "Settings.hpp"


class TreeGenerator;

class ConsoleState : public IState,
                     public Urho3D::Object {
public:
    URHO3D_OBJECT(ConsoleState, Urho3D::Object);

    ConsoleState (Urho3D::Context* pContext,
                  StateManager *pStateManager)
        : Urho3D::Object (pContext),
          _stateManager (pStateManager) {
    }

    void load () {
        SubscribeToEvent(E_KEYDOWN, URHO3D_HANDLER(ConsoleState,
                                                   handleKeyDown));
        GetSubsystem<Input>()->SetMouseVisible(true);
        GetSubsystem<Console>()->SetVisible(true);
    }

    void unload () {
        UnsubscribeFromAllEvents();
        GetSubsystem<Console>()->SetVisible(false);
    }

    void handleKeyDown(StringHash /*eventType*/, VariantMap& eventData) {
        int key = eventData[KeyUp::P_KEY].GetInt();

        if (key == KEY_ESCAPE) {
            _stateManager->popState();
        }
    }

    std::string getName () const {
        return "ConsoleState";
    }

private:
    StateManager *_stateManager;
};


class MainMenuState : public IState,
                      public Urho3D::Object {
    URHO3D_OBJECT(MainMenuState, Urho3D::Object);
public:
    MainMenuState (Urho3D::Context* pContext,
                   StateManager *pStateManager)
        : Urho3D::Object (pContext),
          _stateManager(pStateManager),
          _uiRoot (GetSubsystem<UI>()->GetRoot()) {
    }

    void addMenuElement (const ea::string &pName,
                         IState *pState) {
        _menuElements.insert(std::make_pair(pName, pState));
    }

    void load () {
        // Load XML file containing default UI style sheet
        auto* cache = GetSubsystem<ResourceCache>();
        auto* style = cache->GetResource<XMLFile>("UI/DefaultStyle.xml");

        // Set the loaded style as default style
        _uiRoot->SetDefaultStyle(style);

        _window = new Window(context_);
        _uiRoot->AddChild(_window);

        // Set Window size and layout settings
        _window->SetMinWidth(384);
        _window->SetStyleAuto();
        _window->SetLayout(LM_VERTICAL, 6, IntRect(6, 6, 6, 6));
        _window->SetAlignment(HA_RIGHT, VA_CENTER);
        _window->SetName("MainMenuWindow");


        //SubscribeToEvent(button,
                 //        E_TEXTFINISHED,
                //         URHO3D_HANDLER(SettingsGui, processTextFinished));
        GetSubsystem<Input>()->SetMouseVisible(true);
        GetSubsystem<Input>()->SetMouseMode(MM_ABSOLUTE);

        for (auto el: _menuElements) {
            auto button = UrhoUtils::createButton(_window, el.first);
            button->SetName(el.first);
            SubscribeToEvent(button, E_RELEASED, URHO3D_HANDLER(MainMenuState,
                                                                menuButtonPressed));
        }


        auto exit = UrhoUtils::createButton(_window, "Exit");
        SubscribeToEvent(exit, E_RELEASED, URHO3D_HANDLER(MainMenuState,
                                                          exitButtonPressed));
    }

    void menuButtonPressed (StringHash /*eventType*/, VariantMap& eventData) {
        auto el = (UIElement*)eventData[Pressed::P_ELEMENT].GetPtr();
        auto name = el->GetName();
        _stateManager->pushState(_menuElements[name]);
    }

    void exitButtonPressed (StringHash /*eventType*/, VariantMap& /*eventData*/) {
        _stateManager->popState();
    }

    void resume () {
        GetSubsystem<Input>()->SetMouseVisible(true);
        _window->SetVisible(true);
    }

    void freeze (IState * /*pPushedState*/) {
        _window->SetVisible(false);
    }

    void unload () {
        _uiRoot->RemoveChild(_window);
    }
    std::string getName () const {
        return "MainMenu";
    }

private:
    StateManager *_stateManager;
    UIElement *_uiRoot;
    Window* _window = nullptr;

    std::map<ea::string, IState*> _menuElements;
};


class UrhoWorlds : public Sample
{
  URHO3D_OBJECT(UrhoWorlds, Sample);

public:
    explicit UrhoWorlds(Context* context);

    void Start() override;

protected:
    /// Return XML patch instructions for screen joystick layout for a specific sample app, if any.
    ea::string GetScreenJoystickPatchString() const override { return
        "<patch>"
        "    <add sel=\"/element/element[./attribute[@name='Name' and @value='Hat0']]\">"
        "        <attribute name=\"Is Visible\" value=\"false\" />"
        "    </add>"
        "</patch>";
    }

    void debugFrameTimeChartSettingUpdated ();

private:
  void createScene();

  void subscribeToEvents();

  void handleKeyDown(StringHash /*eventType*/, VariantMap& eventData);

  void handleUpdate(StringHash eventType, VariantMap& eventData);


  void setupViewport();

  void createInstructions ();

  void initSettings ();

  void adjustOverlayLayerScreenSize ();

  void handleScreenResize (StringHash eventType, VariantMap& eventData);


  Settings _cfg;
  UrhoConsole _console;
  FrameTimeGraph _frameTimeGraph;
  Urho3D::Node *_orthoCameraNode = nullptr;

  TreeGenerator *_tree = nullptr;

    MainMenuState _mainMenu;
    StateManager _stateManager;
    ConsoleState _consoleState;
    SettingsGuiState _appSettingsState;
    GameplayState _gameplayState;

    Text *_fpsUiElement;
    float _fpsTimer = 0;
    int _fpsCounter = 0;

    Settings2 _worldSettings;

    ShaderParameterConfigurator _shaderParameterConfigurator;
};
