#pragma once

#include <VcppBits/Settings2/Settings2.hpp>
#include <UrhoBits/SettingsTypes/Vector3.hpp>

enum class SettingTypeEnum : std::size_t { BOOL,
                                           INT,
                                           FLOAT,
                                           ENUM_INT,
                                           ENUM_FLOAT,
                                           STRING,
                                           ENUM_STRING,
                                           VECTOR3 };

using Setting2 = V2::SettingImpl<SettingTypeEnum,
                                 V2::BoolValue,
                                 V2::IntValue,
                                 V2::FloatValue,
                                 V2::EnumIntValue,
                                 V2::EnumFloatValue,
                                 V2::StringValue,
                                 V2::EnumStringValue,
                                 Vector3Value>;

using Settings2 = V2::SettingsImpl<Setting2>;
