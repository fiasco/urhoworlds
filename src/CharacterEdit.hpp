#pragma once

class CharacterEditState : public IState,
                           public Urho3D::Object {
public:

    URHO3D_OBJECT(CharacterEditState, Urho3D::Object);

    CharacterEditState (Urho3D::Context* pContext,
                        StateManager *pStateManager);

    void load ();
    void unload ();

    void freeze (IState *pPreviouState) override;
    void resume ();

    std::string getName () const { return "CharacterEditState"; }
};
