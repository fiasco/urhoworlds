#pragma once


#include <memory>
#include <vector>
#include <unordered_map>
#include <array>
#include <deque>

#include <Urho3D/Math/Vector3.h>
#include <Urho3D/Scene/Node.h>
#include <Urho3D/Math/Vector2.h>
#include <Urho3D/Core/Object.h>
#include <Urho3D/Core/Context.h>
#include <Urho3D/Container/Str.h>
#include <Urho3D/Math/StringHash.h>
#include <Urho3D/Math/Color.h>
#include <Urho3D/UI/UIElement.h>
#include <Urho3D/Graphics/IndexBuffer.h>
#include <Urho3D/Graphics/VertexBuffer.h>


#include "UrhoBulletIncludeFix.hpp"
#include "Settings/Settings.hpp"
#include "LandscapeGenerator.hpp"
#include "LandscapeManager.hpp"
#include "Impostor.hpp"
#include "Utils/SimpleVector.hpp"

#include "Settings.hpp"

template<typename T>
class DataCache;

template<typename T>
class DataCacheElement {
public:
    DataCacheElement ()
        : _owner (nullptr) {
    }

    DataCacheElement (std::unique_ptr<T> &&pEl, DataCache<T>* pOwner)
        : _el(std::move(pEl)),
        _owner(pOwner) {
    }

    ~DataCacheElement () {
        if (_owner && _el.get()) {
            _owner->_returnElement(std::move(_el));
        }
    }

    DataCacheElement& operator= (DataCacheElement &pOther)  = delete;
    DataCacheElement (DataCacheElement &pOther) = delete;


    DataCacheElement& operator= (DataCacheElement &&pOther) {
        _el = std::move(pOther._el);
        _owner = pOther._owner;

        return *this;
    }

    DataCacheElement (DataCacheElement &&pOther) {
        _el = std::move(pOther._el);
        _owner = pOther._owner;
    }


    T* operator-> () {
        return _el.get();
    }

    T* get () {
        return _el.get();
    }

private:
    std::unique_ptr<T> _el;
    DataCache<T>* _owner;
};

template<typename T>
class DataCache {
public:
    DataCacheElement<T> createElement() {
        if (_data.size()) {
            auto ret = std::move(_data.back());
            _data.pop_back();
            return DataCacheElement<T>(std::move(ret), this);
        }

        return DataCacheElement<T>(std::make_unique<T>(), this);
    }

    void _returnElement (std::unique_ptr<T> &&pEl) {
        _data.push_back(std::move(pEl));
    }

private:
    std::vector<std::unique_ptr<T>> _data;
};

struct UrhoworldsPhysicsData {
    SimpleVector<UrhoworldsIndexElement> *index_data;
    DataCacheElement<SimpleVector<Urho3D::Vector3>> vertex_data;
    std::unique_ptr<btBvhTriangleMeshShape> bt_shape;
    btTriangleIndexVertexArray bt_mesh;
};


struct ModelLoadWorkerData {
    enum class TYPE { Single, Multi }; // TODO add Outer

    Urho3D::IntVector2 pos;

    void* ptr = nullptr;
    size_t level;
    TYPE type = TYPE::Single;
    bool cancel = false;
};


struct SingleModelLoadWorkerData {
    Noizes *  nz = nullptr;
    std::unique_ptr<UrhoworldsPhysicsData> physics_data;
    // TODO needed only for SingleModelLoadWorkerData*
    // Landscape::moveWorkItemToPositionsList to work
    SingleModelLoadWorkerData () {
        throw;
    }

    SingleModelLoadWorkerData& operator= (SingleModelLoadWorkerData&) = delete;

    SingleModelLoadWorkerData (SingleModelLoadWorkerData&& pOther) = default;

    SingleModelLoadWorkerData& operator= (SingleModelLoadWorkerData&&) = default;

    // TODO: eliminate the need of this
    SingleModelLoadWorkerData (const SingleModelLoadWorkerData& /*pOther*/) {
        throw;
    }


    SingleModelLoadWorkerData (Noizes * pNoizes,
                               Urho3D::Context *pContext,
                               const unsigned short pSize,
                               const unsigned short pResolution,
                               const Urho3D::IntVector2 pPosition)
        : nz (pNoizes),
          lod0 (pContext,
                getRequiredLod0IndexBufferLength(pResolution) * 3,
                getRequiredLod0VertexBufferLength(pResolution)),
          size (pSize),
          resolution (pResolution),
          pos (pPosition) {

        // TODO max resolution is 104
        // (more will overflow unsigned max): 105*105*2*3
        const size_t res_index =
            getRequiredLod0IndexBufferLength(pResolution);
        const size_t res_vertex =
            getRequiredLod0VertexBufferLength(pResolution);

        lod0_index_view.initializeByMemory(
            (UrhoworldsIndexElement*)lod0.indexBuffer->GetShadowData(),
            res_index,
            res_index);
        lod0_vertex_view.initializeByMemory(
            (UrhoworldsLandscapeVertexElement*)lod0.vertexBuffer->GetShadowData(),
            res_vertex,
            res_vertex);

        if (pResolution == pSize) {
            const size_t num_indices =
                getRequiredLod1IndexBufferLength(pResolution);
            const size_t num_vertices =
                getRequiredLod1VertexBufferLength(pResolution);
            lod1 = UrhoworldsGeometryConstructionData<
                UrhoworldsLandscapeVertexElement>(pContext,
                                                  num_indices * 3,
                                                  num_vertices);

            lod1_index_view.initializeByMemory(
                (UrhoworldsIndexElement*)lod1.indexBuffer->GetShadowData(),
                num_indices,
                0);
            lod1_vertex_view.initializeByMemory(
                (UrhoworldsLandscapeVertexElement*)lod1.vertexBuffer->GetShadowData(),
                num_vertices,
                0);
        }
    }

    static size_t getRequiredLod0IndexBufferLength (
        const size_t pResolution) {
        return pResolution * pResolution * 2;
    }

    static size_t getRequiredLod0VertexBufferLength (
        const size_t pResolution) {
        return (pResolution + 1) * (pResolution + 1);
    }

    static size_t getRequiredLod1IndexBufferLength (
        const size_t pResolution) {
        return create_lod_1_plane_get_num_index(pResolution, pResolution);
    }

    static size_t getRequiredLod1VertexBufferLength (
        const size_t pResolution) {
        return create_lod_1_plane_get_outer_circle_num_vertex(
            pResolution, pResolution)
            + create_lod_1_plane_get_inner_num_vertex(pResolution,
                                                      pResolution);
    }

    UrhoworldsGeometryConstructionData<UrhoworldsLandscapeVertexElement> lod0;
    UrhoworldsGeometryConstructionData<UrhoworldsLandscapeVertexElement> lod1;

    SimpleVector<UrhoworldsIndexElement> lod0_index_view;
    SimpleVector<UrhoworldsLandscapeVertexElement> lod0_vertex_view;

    SimpleVector<UrhoworldsIndexElement> lod1_index_view;
    SimpleVector<UrhoworldsLandscapeVertexElement> lod1_vertex_view;

    Urho3D::IntVector2 pos;
    Urho3D::Vector3 position;
    Urho3D::Vector3 bounds0;
    Urho3D::Vector3 bounds1;

    std::vector<Urho3D::Matrix3x4> trees;

    unsigned short size = 0;
    unsigned short resolution = 0;
};

struct MultiModelLoadWorkerData {
    std::array<SingleModelLoadWorkerData*, 4> tasks;
};


class Landscape : public Urho3D::Object {
public:
    using LandscapeData = std::unordered_map<Urho3D::IntVector2,
                                             std::pair<Urho3D::Node*,
                                                       SingleModelLoadWorkerData>,
                                             hash_int_vector2>;

    struct LandscapeLevelData {
        LandscapeManager manager;
        LandscapeData positions_data;

        LandscapeLevelData () = default;

        // disallow copies, as inner data is not designed to be copied
        LandscapeLevelData& operator= (LandscapeLevelData&) = delete;
        LandscapeLevelData (const LandscapeLevelData& /*pOther*/) = delete;

        // move is fine
        LandscapeLevelData (LandscapeLevelData&& pOther) = default;
        LandscapeLevelData& operator= (LandscapeLevelData&&) = default;
    };

    URHO3D_OBJECT(Landscape, Urho3D::Object);

    explicit Landscape (Urho3D::Context *pContext);
    void init (Settings *pSettings,
               Settings2 *pWorldConfig,
               Urho3D::Node *pCameraNode);
    void initSettings ();
    void landscapeSettingUpdated ();
    void debugDrawSettingUpdated ();

    bool upgradeTilesAreStillNeeded (const LandscapeLevelData &pLd,
                                     const MultiModelLoadWorkerData *pDt,
                                     const size_t pTileSize) const;

    void loadTileUpgrade (const MultiModelLoadWorkerData *pDt,
                          const size_t pTileSize,
                          const size_t pLevel);

    void processUpdateEvent (Urho3D::StringHash eventType,
                             Urho3D::VariantMap& eventData);

    void fireUpTileUpgradeTasks ();

    void removeTilesOutsideRange ();

    void updateCurrentWorksTiles ();

    void startJobsForNeededTiles ();

    SingleModelLoadWorkerData*
    allocateSingleModelLoadWorkerData (Urho3D::Context* pContext,
                                       const Urho3D::IntVector2 &pos_i,
                                       const unsigned pSize,
                                       const unsigned pResolution);

    void createSingleWorkItem (const Urho3D::IntVector2& pos_i,
                               const unsigned pSize,
                               const unsigned pResolution,
                               const unsigned priority);

    void processWorkItemCompleted (Urho3D::StringHash eventType,
                                   Urho3D::VariantMap& eventData);

    float getHeightAt (const float x, const float y) {
        return _nz.get_noize(x, y);
    }

    void resetNumTilesLoadedCounters () {
        _numTilesLoadedCounter = 0;
    }

    size_t getNumTilesLoadedCounter () {
        return _numTilesLoadedCounter;
    }

private:
    bool isUpdateNeeded ();

    void unloadTile (const size_t pLevel,
                     const Urho3D::IntVector2 &pWorldPos,
                     const bool pUpdateLandscapeManager = false);

    SingleModelLoadWorkerData*
    moveWorkItemToPositionsList (SingleModelLoadWorkerData* pWorkItem,
                                 const size_t pLevel);

    Urho3D::Node* loadTile (SingleModelLoadWorkerData *dt,
                            const size_t pLevel);
    Urho3D::Node* loadOuterTile (SingleModelLoadWorkerData *dt,
                                 const size_t pLevel);
    void eraseCurrentWork (const ModelLoadWorkerData *dt);

    // this should lie upward of the _currentWorks
    DataCache<SimpleVector<Urho3D::Vector3>> cachePhysicsVertex;

    Settings *_cfg = nullptr;
    Settings2 *_worldCfg = nullptr;
    Urho3D::Node *_cameraNode = nullptr;

    std::unordered_multimap<Urho3D::IntVector2,
                            SingleModelLoadWorkerData,
                            hash_int_vector2> _currentWorksSingle;

    // TODO try vector too
    std::deque<SingleModelLoadWorkerData> _freeWorksSingle;
    std::deque<SingleModelLoadWorkerData> _freeWorksSingleWithPhysics;

    std::unordered_multimap<Urho3D::IntVector2,
                            MultiModelLoadWorkerData,
                            hash_int_vector2> _currentWorksMulti;

    std::unordered_multimap<Urho3D::IntVector2,
                            ModelLoadWorkerData,
                            hash_int_vector2> _currentWorks;

    Noizes _nz;

    std::vector<LandscapeLevelData> _landscapeData;

    std::unordered_map<Urho3D::IntVector2,
                       std::unique_ptr<UrhoworldsPhysicsData>,
                       hash_int_vector2> _landscapePhysicsData;

    Urho3D::SharedPtr<Urho3D::Impostor> _tree_imp;

    size_t _mapTileSize = 0; // cached from settings, so that we don't gather
                         // it from settings every time;

    size_t _numTilesLoadedCounter = 0;

    float start_load_time = 0;
    float first_load_time = 0;

    bool _waitingToFinishTasksBeforeRestart = false;
};
