// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include "UrhoworldsDebug.hpp"

namespace UrhoworldsDebug {
struct UrhoworldsDebugData {
    static size_t num_simple_vector_reallocations;
};

size_t UrhoworldsDebugData::num_simple_vector_reallocations = 0;

void record_simple_vector_reallocations () {
    ++UrhoworldsDebugData::num_simple_vector_reallocations;
}

size_t get_simple_vector_reallocations () {
    return UrhoworldsDebugData::num_simple_vector_reallocations;
}

}
