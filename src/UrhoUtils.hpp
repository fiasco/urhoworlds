#pragma once

#include <Urho3D/UI/UIElement.h>
#include <Urho3D/UI/Button.h>
#include <Urho3D/UI/Text.h>
#include <Urho3D/Container/Str.h>
#include <Urho3D/UI/Text3D.h>
#include <Urho3D/UI/Font.h>


namespace UrhoUtils {

using namespace Urho3D;

inline Button* createButton (UIElement *pParent,
                             const ea::string &pLabelText) {
    auto button = pParent->CreateChild<Button>();
    auto label = button->CreateChild<Text>();

    button->SetMinSize(0, 24);
    button->SetVerticalAlignment(VA_TOP);
    button->SetStyleAuto();

    label->SetText(pLabelText);
    label->SetStyleAuto();
    label->SetAlignment(HA_CENTER, VA_CENTER);

    return button;
}

inline void displayTextOnNode (Node *pNode,
                               const ea::string &pText,
                               const ea::string &pNodeName = EMPTY_STRING) {
    auto text_node = pNode->CreateChild(pNodeName);
    text_node->SetPosition(Vector3(0.5, 2.5, 0.5));
    auto text = text_node->CreateComponent<Text3D>();
    text->SetText(pText);
    text->SetFont(pNode->GetSubsystem<ResourceCache>()
                  ->GetResource<Font>("Fonts/Anonymous Pro.ttf"), 15);
    text->SetFaceCameraMode(FC_ROTATE_XYZ);
    text->SetFixedScreenSize(true);
}

}
