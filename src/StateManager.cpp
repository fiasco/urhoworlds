// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com
#include  "StateManager.hpp"

#include <Urho3D/IO/Log.h>
#include <Urho3D/Container/Str.h>


void StateManager::pushState (IState *pState) {
    _stateToPush = pState;
}

void StateManager::popState () {
    _popRequested = true;
}

void StateManager::_doPushState () {
    URHO3D_LOGINFO("pushing state "
                   + ea::string(_stateToPush->getName().c_str()));
    _states.push(_stateToPush);
    _stateToPush->load();
    _stateToPush = nullptr;
    _topIsFrozen = false;
}

bool StateManager::update () {
    //if (_statesToPush.empty()) && !_popRequested && !_popAllRequested
    if (_stateToPush) {
        if (_popRequested) { throw 200; }
        if (_states.size()) {
            if (_topIsFrozen) {
                _doPushState();
            } else {
                URHO3D_LOGINFO("freezing state "
                               + ea::string(_states.top()->getName().c_str()));
                _states.top()->freeze(_stateToPush);
                _topIsFrozen = true;
            }
        } else {
            _doPushState();
        }
    } else if (_popRequested) {
        URHO3D_LOGINFO("unloading state " +
                       ea::string(_states.top()->getName().c_str()));
        _states.top()->unload();
        _states.pop();
        _popRequested = false;
        if (_states.size()) {
            _topIsFrozen = true;
        } else {
            return false;
        }
    }

    if (_topIsFrozen && !_stateToPush) {
        URHO3D_LOGINFO("resuming state "
                       + ea::string(_states.top()->getName().c_str()));
        _states.top()->resume();
        _topIsFrozen = false;
    }

    return true;
}
