// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com
#include "CameraController.hpp"


#include <Urho3D/Math/StringHash.h>
#include <Urho3D/Core/Context.h>
#include <Urho3D/Math/Vector3.h>
#include <Urho3D/Input/Input.h>
#include <Urho3D/Math/Vector2.h>
#include <Urho3D/Core/CoreEvents.h>
#include <Urho3D/Scene/Node.h>



using namespace Urho3D;


CameraController::CameraController (Context *pContext)
    : Object(pContext) {
}

void CameraController::init (Urho3D::Node *pCameraNode,
                             Settings *pSettings) {
    _cameraNode = pCameraNode;
    _settings = pSettings;
    _moveSpeed = _settings->getSetting("_camera_move_speed").getFloat();
    _settings->getSetting("_camera_move_speed")
        .addUpdateHandler(this,
                          std::bind(&CameraController::cameraMoveSpeedSettingUpdated,
                                    this));
}

void CameraController::cameraMoveSpeedSettingUpdated () {
    _moveSpeed = _settings->getSetting("_camera_move_speed").getFloat();
}

void CameraController::activate () {
    SubscribeToEvent(E_UPDATE, URHO3D_HANDLER(CameraController, handleUpdate));
}

void CameraController::stop () {
    UnsubscribeFromAllEvents();
}
void CameraController::moveCamera (float timeStep) {
    Input* input = GetSubsystem<Input>();

    // Mouse sensitivity as degrees per pixel
    const float MOUSE_SENSITIVITY = 0.1f;

    IntVector2 mouseMove = input->GetMouseMove();
    yaw_ += MOUSE_SENSITIVITY * mouseMove.x_;
    pitch_ += MOUSE_SENSITIVITY * mouseMove.y_;
    pitch_ = Clamp(pitch_, -90.0f, 90.0f);

    _cameraNode->SetRotation(Quaternion(pitch_, yaw_, 0.0f));

    if (input->GetKeyDown(KEY_Z))
        _cameraNode->Translate(Vector3::UP * _moveSpeed * timeStep, TS_WORLD);
    if (input->GetKeyDown(KEY_X))
        _cameraNode->Translate(Vector3::DOWN * _moveSpeed * timeStep, TS_WORLD);
    if (input->GetKeyDown(KEY_W))
        _cameraNode->Translate(Vector3::FORWARD * _moveSpeed * timeStep);
    if (input->GetKeyDown(KEY_S))
        _cameraNode->Translate(Vector3::BACK * _moveSpeed * timeStep);
    if (input->GetKeyDown(KEY_A))
        _cameraNode->Translate(Vector3::LEFT * _moveSpeed * timeStep);
    if (input->GetKeyDown(KEY_D))
        _cameraNode->Translate(Vector3::RIGHT * _moveSpeed * timeStep);
}


void CameraController::handleUpdate (StringHash /*eventType*/,
                                     VariantMap& eventData) {
    using namespace Update;

    float timeStep = eventData[P_TIMESTEP].GetFloat();

    moveCamera(timeStep);
}
