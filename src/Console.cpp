// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com
#include "Console.hpp"


#include <Urho3D/Engine/EngineEvents.h>
#include <Urho3D/Engine/Engine.h>
#include <Urho3D/IO/Log.h>


#include "Settings/Settings.hpp"
#include "Settings/SettingsException.hpp"


using namespace Urho3D;


UrhoConsole::UrhoConsole (Context *pContext)
    : Object(pContext) {
}

void UrhoConsole::setSettingsPtr (Settings *pSettings) {
    _cfg = pSettings;
}

void UrhoConsole::start () {
    _console = GetSubsystem<Console>();

    // TODO: commented due to migration to rbfx
    //_console->SetNumBufferedRows(2 * _console->GetNumRows());
    _console->SetCommandInterpreter(GetTypeName());
    // TODO: completion commented due to rbfx
    //_console->AddAutoComplete("help");
    //_console->AddAutoComplete("quit");
    //_console->AddAutoComplete("set");
    //_console->AddAutoComplete("run");
   // _console->AddAutoComplete("benchmark");

    SubscribeToEvent(E_CONSOLECOMMAND, URHO3D_HANDLER(UrhoConsole, handleConsoleCommand));

}

void UrhoConsole::handleConsoleCommand (StringHash /*eventType*/, VariantMap& eventData) {
    using namespace ConsoleCommand;
    if (eventData[P_ID].GetString() == GetTypeName())
        handleConsoleInput(eventData[P_COMMAND].GetString());
}

void UrhoConsole::handleConsoleInput (const ea::string& pInput) {
    const auto args = pInput.split(' ');
    const auto command = args[0];
    const ea::string command_lower = command.to_lower();

    printToConsole(command_lower + ":");
    if (command.empty()) {
        printToConsole("Empty input given!");
        return;
    }

    if (command == "quit") {
        GetSubsystem<Urho3D::Engine>()->Exit();
    } else if (command == "help") {
        printToConsole("help -- print this message\n\
run <script_filename> -- run a script\n\
set [option [value]]\n\
quit -- exit application");

        return;
    } else if (command == "set") {
        if(args.size() == 1) {
            auto it = _cfg->getSettingsIterator();
            while(it.isElement()) {
                printToConsole(ea::string(it.getCurrent().getName().c_str())
                               + " " + it.getCurrent().getAsString().c_str());
                it.peekNext();
            }
            return;
        }

        if (!_cfg->hasSetting(args[1].c_str())) {
            printToConsole("no such setting");
            return;
        }

        if (args.size() > 2) {
            try {
                _cfg->getSetting(args[1].c_str()).setByString(args[2].c_str());
            }
            catch (const SettingsException &exc) {
                printToConsole(exc.getFullDescription().c_str());
            }
        } else {
            printToConsole(_cfg->getSetting(args[1].c_str()).getAsString().c_str());
        }


        // } else if (command == "r" || command == "run") {
        //   auto res_cache = GetSubsystem<ResourceCache>();
        //   ScriptFile* file = nullptr;
        //   if (args.Size() > 1) {
        //     auto file_name = args[1];
        //     auto file_full_path = String("Scripts/") + file_name + ".as";
        //     if (res_cache->Exists(file_full_path)) {
        //       file = GetSubsystem<ResourceCache>()->GetResource<ScriptFile>(file_full_path);
        //     } else {
        //       PrintToConsole(String("file not found: ") + file_full_path);
        //       return;
        //     }
        //   } else {

        //     PrintToConsole("syntax: run <script_filename> (with '.as' ommited)");
        //     return;
        //   }
        //   GetSubsystem<ResourceCache>()->ReloadResource(file);
        //   VariantVector parameters;
        //   file->Execute("void urho_script_main()", parameters); // Execute
    } else if (command == "_campos") {
        URHO3D_LOGINFO("Camera position: " + _cameraNode->GetPosition().ToString());
        URHO3D_LOGINFO("Camera direction: " + _cameraNode->GetDirection().ToString());
        URHO3D_LOGINFO("Camera rotation: " + _cameraNode->GetRotation().ToString());
        return;
    } else if (command == "_goto") {
        if (args.size() > 2) {
            Vector3 dest { };
            if (args.size() > 3) {
                dest.y_ = ToFloat(args[3]);
            } else {
                dest.y_ = _cameraNode->GetPosition().y_;
            }
            dest.x_ = ToFloat(args[1]);
            dest.z_ = ToFloat(args[2]);
            _cameraNode->SetPosition(dest);
            return;
        }
    } else {
        printToConsole("unknown command");
    }
}

void UrhoConsole::setCameraNodePtr (Urho3D::Node *pCameraNode) {
    _cameraNode = pCameraNode;
}

void UrhoConsole::printToConsole (const ea::string& pOutput) {
    // Logging appears both in the engine console and stdout
    URHO3D_LOGINFO(pOutput + "\n");
}
