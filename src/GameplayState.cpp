// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com
#include "GameplayState.hpp"

#include <Urho3D/Core/CoreEvents.h>
#include <Urho3D/SystemUI/Console.h>
#include <Urho3D/Engine/EngineEvents.h>
#include <Urho3D/Graphics/StaticModel.h>
#include <Urho3D/Graphics/Material.h>
#include <Urho3D/IO/Log.h>
#include <Urho3D/Input/Input.h>
#include <Urho3D/Input/InputEvents.h>
#include <Urho3D/Math/Vector2.h>
#include <Urho3D/Math/Vector3.h>
#include <Urho3D/Physics/RigidBody.h>
#include <Urho3D/Resource/ResourceCache.h>
#include <Urho3D/UI/UI.h>
#include <Urho3D/UI/Text.h>
#include <Urho3D/UI/Window.h>
#include <Urho3D/UI/LineEdit.h>
#include <Urho3D/IO/FileSystem.h>

#include "Impostor.hpp"
#include "Settings/Settings.hpp"
#include "TreeGenerator.hpp"
#include "UrhoUtils.hpp"


using namespace Urho3D;

GameplayState::GameplayState (Urho3D::Context* pContext,
               StateManager *pStateManager)
    : Urho3D::Object (pContext),
      _stateManager (pStateManager),
      _landscape (pContext),
      _camCtl(pContext),
      _treeSettingsGui (pContext, _stateManager, &_tree),
      _settingsGui (pContext, _stateManager),
      _worldEdit(pContext, _stateManager) {
}

void GameplayState::init (Settings *pSettings,
                          Settings2 *pWorldSettings,
                          Urho3D::Node *pCameraNode) {
    _settings = pSettings;
    _worldSettings = pWorldSettings;
    _cameraNode = pCameraNode;
    _camCtl.init(_cameraNode, _settings);
}

void GameplayState::load () {
    SubscribeToEvent(E_UPDATE, URHO3D_HANDLER(GameplayState, handleUpdate));
    SubscribeToEvent(E_KEYDOWN, URHO3D_HANDLER(GameplayState,
                                               handleKeyDown));

    GetSubsystem<Input>()->SetMouseVisible(false);

    if (!_settings->get<bool>("dbg.nolscp")) {
        _landscape.init(_settings, _worldSettings, _cameraNode);
    }

    loadModel("WoodenHouse2.mdl",
              "Wood.xml",
              { 20.f, 20.f });

    _treeSettingsGui.init(_tree.getConfig(), _settings);
    _treeSettingsGui.loadPreset();
    _settingsGui.init(_settings);
    _worldEdit.init(_worldSettings);
    _tree.init(context_,
               _cameraNode->GetScene(),
               GetSubsystem<ResourceCache>(),
               true);


    // TODO10: add dbg_noimp setting to disable impostors
    // some simple test items
    auto a_mdl = loadModel("TestItems/Mushroom.mdl",
                           "TestItems/Mushroom.xml",
                           { -13, -13 });
    //auto imp = a_mdl->CreateComponent<Impostor>();

    //imp->OnMarkedDirty(a_mdl);
    (void) a_mdl;


    auto b_mdl = loadModel("TestItems/Kachujin.mdl",
                           "TestItems/Kachujin.xml",
                           { -20, -20 });
    //auto imp2 = b_mdl->CreateComponent<Impostor>();

    //imp2->OnMarkedDirty(b_mdl);
    (void) b_mdl;

    auto c_mdl = loadModel("TestItems/Kachujin.mdl",
                           "TestItems/Kachujin.xml",
                           { -27, -27 });
    c_mdl->Yaw(180);
    //auto imp3 = c_mdl->CreateComponent<Impostor>();

    //imp3->OnMarkedDirty(c_mdl);


    auto d_mdl = loadModel("Misc/ColorCube.mdl",
                           "Misc/ColorCube.xml",
                           { -35, -35 });
    d_mdl->Yaw(180);
    //auto imp4 = d_mdl->CreateComponent<Impostor>();

    //imp4->OnMarkedDirty(d_mdl);


    _cameraNode->SetPosition(Vector3(0.f, 20.f, 0.f));
    _cameraNode->SetDirection(Vector3(1.f, -0.7f, 1.f));
    _camCtl.activate();
}

Node* GameplayState::loadModel (const ea::string &pModelName,
                                const ea::string&pMaterialName,
                                const Vector2 &pPos) {
    const float pos_y = _landscape.getHeightAt(pPos.x_, pPos.y_);
    auto a_node = _cameraNode->GetScene()->CreateChild();
    auto modl_component = a_node->CreateComponent<StaticModel>();
    modl_component->SetModel(
        GetSubsystem<ResourceCache>()->GetResource<Model>(pModelName));
    modl_component->SetMaterial(
        GetSubsystem<ResourceCache>()->GetResource<Material>(pMaterialName));
    a_node->SetPosition(Vector3(pPos.x_, pos_y + 1, pPos.y_));

    return a_node;
}

void GameplayState::unload () {
    _camCtl.stop();
    UnsubscribeFromAllEvents();
}

void GameplayState::freeze (IState * /*pPreviousState*/) {
    _camCtl.stop();
    UnsubscribeFromAllEvents();
    SubscribeToEvent(E_CONSOLECOMMAND, URHO3D_HANDLER(GameplayState,
                                                      handleConsoleCommand));
}

void GameplayState::resume () {
    if (!_isBenchmarking) {
        _camCtl.activate();
    }

    SubscribeToEvent(E_KEYDOWN, URHO3D_HANDLER(GameplayState,
                                               handleKeyDown));
    GetSubsystem<Input>()->SetMouseVisible(false);

    SubscribeToEvent(E_UPDATE, URHO3D_HANDLER(GameplayState, handleUpdate));
}

void GameplayState::handleConsoleCommand (StringHash /*eventType*/,
                                          VariantMap& eventData) {
    using namespace ConsoleCommand;
    if (eventData[P_COMMAND].GetString() == "benchmark") {
        if (!_isBenchmarking) {
            _isBenchmarking = true;
            _camCtl.stop();
            _cameraNode->SetPosition(Vector3(0.f, 50.f, 0.f));
            _cameraNode->SetDirection(Vector3(0.f, 0.f, 0.f));
            _landscape.resetNumTilesLoadedCounters();
            _landscape.landscapeSettingUpdated();

            // the 'if' is probably redundant, but in case there will ever be
            // other way to start benchmark not from console...
            if (_stateManager->currentState()->getName() == "ConsoleState") {
                _stateManager->popState();
            }
        } else {
            URHO3D_LOGWARNING("already benchmarking");
        }
    }
}

void GameplayState::handleUpdate (StringHash /*eventType*/,
                                  VariantMap& eventData) {
    using namespace Update;

    const float timeStep = eventData[P_TIMESTEP].GetFloat();

    if (_isBenchmarking) {
        ++_benchmarkResultsFramesNum;
        _benchmarkTimePassed += timeStep;
        _cameraNode->Translate(Vector3(0.f, 0.f, 2000.f * timeStep));
        if (_benchmarkTimePassed >= 40.f) {
            URHO3D_LOGINFO("================");
            auto result = _landscape.getNumTilesLoadedCounter();

            URHO3D_LOGINFO("num tiles loaded: " + ea::to_string(result));
            URHO3D_LOGINFO("     average fps: "
                           + ea::to_string(float(_benchmarkResultsFramesNum)
                                           / _benchmarkTimePassed));
            //TODO URHO3D_LOGINFO("num relocations: " + String(UrhoworldsMemory<int>::getNumRelocations()));

            _isBenchmarking = false;
            _camCtl.activate();
            _benchmarkTimePassed = 0.f;
            _benchmarkResultsFramesNum = 0;
        }
    }

    if (GetSubsystem<Input>()->GetMouseButtonPress(MOUSEB_LEFT)) {
        spawnObject();
    }
}

void GameplayState::spawnObject () {
    ResourceCache* cache = GetSubsystem<ResourceCache>();

    // Create a smaller box at camera position
    Node* boxNode = _cameraNode->GetScene()->CreateChild("SmallBox");
    boxNode->SetPosition(_cameraNode->GetPosition());
    boxNode->SetRotation(_cameraNode->GetRotation());
    boxNode->SetScale(0.25f);
    StaticModel* boxObject = boxNode->CreateComponent<StaticModel>();
    boxObject->SetModel(cache->GetResource<Model>("Misc/Box.mdl"));
    //boxObject->SetMaterial(cache->GetResource<Material>("Materials/StoneEnvMapSmall.xml"));
    boxObject->SetCastShadows(true);

    // Create physics components, use a smaller mass also
    RigidBody* body = boxNode->CreateComponent<RigidBody>();
    body->SetMass(0.25f);
    body->SetFriction(0.75f);
    CollisionShape* shape = boxNode->CreateComponent<CollisionShape>();
    shape->SetBox(Vector3::ONE);

    const float OBJECT_VELOCITY = 3.0f;

    // Set initial velocity for the RigidBody based on camera forward vector. Add also a slight up component
    // to overcome gravity better
    body->SetLinearVelocity(_cameraNode->GetRotation() * Vector3(0.0f, 0.25f, 1.0f) * OBJECT_VELOCITY);
}


void GameplayState::handleKeyDown (StringHash /*eventType*/, VariantMap& eventData) {
    int key = eventData[KeyUp::P_KEY].GetInt();

    if (_isBenchmarking)
        return;

    if (key == KEY_ESCAPE) {
        _stateManager->popState();
    } else if (key == KEY_E) {
        _treeSettingsGui.setAlignment(HA_RIGHT, VA_CENTER);
        _stateManager->pushState(&_treeSettingsGui);
    } else if (key == KEY_L) {
        _settingsGui.setAlignment(HA_RIGHT, VA_CENTER);
        _stateManager->pushState(&_settingsGui);
    } else if (key == KEY_K) {
        _stateManager->pushState(&_worldEdit);
    }
}

void TreeEditState::scanPresets () {
    loadTreePresetsList(context_, _presets);
}

void TreeEditState::loadPreset () {
    scanPresets();
    auto cfg_preset = ea::string(_presetSetting->getString().c_str());
    bool configured_preset_found = false;

    if (_uiList) {
        _uiList->RemoveAllItems();
    }
    for (auto el : _presets) {
        auto lbl = new Urho3D::Text (context_);
        lbl->SetStyleAuto();
        lbl->SetText(el);
        if (_uiList) {
            _uiList->AddItem(lbl);
        }
        if (el == cfg_preset) {
            configured_preset_found = true;
            reloadPreset();

            if (_uiList) {
                _uiList->SetSelection(_uiList->GetItems().size() - 1);
            }
        }
    }

    if (!configured_preset_found && _presets.size()) {
        _presetSetting->setString(_presets[0].c_str());
        reloadPreset();
    }
}

void TreeEditState::forkButtonPressed (StringHash /*type*/, VariantMap& /*args*/) {
    auto window = _uiList->GetRoot()->CreateChild<Window>();

    window->SetStyleAuto();
    window->SetMinWidth(384);
    window->SetLayout(LM_VERTICAL, 6, IntRect(6, 6, 6, 6));
    window->SetAlignment(HA_CENTER, VA_CENTER);
    window->SetMaxHeight(700);

    auto text = window->CreateChild<Text>();
    text->SetText("Enter name for new preset:");
    text->SetStyleAuto();

    auto edit = window->CreateChild<LineEdit>();
    edit->SetMinHeight(20);
    edit->SetStyleAuto();
    SubscribeToEvent(edit,
                     E_TEXTFINISHED,
                     URHO3D_HANDLER(TreeEditState, presetNameEntered));
}

void TreeEditState::presetNameEntered (StringHash /*eventType*/,
                                       VariantMap& eventData) {
    using namespace TextFinished;
    ea::string text = eventData[P_TEXT].GetString();
    _treeCfg->setFilename(text.c_str() + std::string(".tree.ini"));
    _treeCfg->writeFile();
    ((UIElement*)eventData[P_ELEMENT].GetPtr())->GetParent()->Remove();
    loadPreset();
}


void TreeEditState::load () {
    scanPresets();

    SettingsGuiState::load();
    auto cont = _gui.getUiWindow()->CreateChild<Urho3D::UIElement>("", 1);
    cont->SetLayoutMode(LM_HORIZONTAL);
    cont->SetStyleAuto();
    cont->SetMaxHeight(24);

    auto lbl = cont->CreateChild<Urho3D::Text>();
    lbl->SetStyleAuto();
    lbl->SetText("Preset:");

    _uiList = cont->CreateChild<Urho3D::DropDownList>();
    _uiList->SetStyleAuto();

    auto fork = UrhoUtils::createButton(cont, "Fork");

    SubscribeToEvent(fork, E_RELEASED, URHO3D_HANDLER(TreeEditState,
                                                      forkButtonPressed));

    SubscribeToEvent(_uiList,
                     E_ITEMSELECTED,
                     URHO3D_HANDLER(TreeEditState,
                                    presetSelected));
    loadPreset();
}

void TreeEditState::presetSelected (Urho3D::StringHash /*eventType*/,
                                    Urho3D::VariantMap& eventData) {
    const unsigned selection =
        eventData[Urho3D::ItemSelected::P_SELECTION].GetUInt();
    const std::string preset = std::string(_presets[selection].c_str());
    if (preset != _presetSetting->getString()) {
        _presetSetting->setString(preset);
        reloadPreset();
    }
}

void TreeEditState::reloadPreset () {
    _tree->pauseUpdates(true);
    _treeCfg->writeFile();
    _treeCfg->loadFromFile (_presetSetting->getString());
    _tree->pauseUpdates(false);
    _tree->settingUpdated();
}

