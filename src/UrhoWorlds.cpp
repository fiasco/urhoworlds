// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com
#include "UrhoWorlds.hpp"

#include <Urho3D/SystemUI/Console.h>
#include <Urho3D/Core/CoreEvents.h>
#include <Urho3D/Core/ProcessUtils.h>
#include <Urho3D/Core/StringUtils.h>
#include <Urho3D/DebugNew.h>
#include <Urho3D/Engine/Engine.h>
#include <Urho3D/Engine/EngineEvents.h>
#include <Urho3D/Graphics/Camera.h>
#include <Urho3D/Graphics/Graphics.h>
#include <Urho3D/Graphics/Material.h>
#include <Urho3D/Graphics/Model.h>
#include <Urho3D/Graphics/Octree.h>
#include <Urho3D/Graphics/Renderer.h>
#include <Urho3D/Graphics/RenderPath.h>
#include <Urho3D/Graphics/StaticModel.h>
#include <Urho3D/Graphics/GraphicsEvents.h>
#include <Urho3D/Graphics/Zone.h>
#include <Urho3D/Input/Input.h>
#include <Urho3D/Resource/ResourceCache.h>
#include <Urho3D/Scene/Scene.h>
#include <Urho3D/UI/Font.h>
#include <Urho3D/UI/Text.h>
#include <Urho3D/UI/UI.h>
#include <Urho3D/UI/UI.h>
#include <Urho3D/UI/Text3D.h>
#include <Urho3D/Graphics/DebugRenderer.h>
#include <Urho3D/Core/WorkQueue.h>
#include <Urho3D/Urho2D/Drawable2D.h>


#include "LandscapeGenerator.hpp"
#include "TreeGenerator.hpp"
#include "SettingsGui.hpp"
#include "Impostor.hpp"
#include "UrhoworldsDebug.hpp"

#include <cmath>
#include <algorithm>
#include <vector>
#include <deque>
#include <random>

#undef min
#include <limits>

URHO3D_DEFINE_APPLICATION_MAIN(UrhoWorlds)

// function hex_corner(center, size, i) {
//     var angle_deg = 60 * i   + 30;
//     var angle_rad = PI / 180 * angle_deg;
//     return Point(center.x + size * cos(angle_rad), center.y + size * sin(angle_rad));
// }

UrhoWorlds::UrhoWorlds (Context* context) 
    : Sample(context),
      _cfg("config.ini"),
      _console(context),
      _frameTimeGraph(context),
      _mainMenu (context, &_stateManager),
      _consoleState (context, &_stateManager),
    _appSettingsState (context, &_stateManager),
    _gameplayState (context, &_stateManager),
    _worldSettings ("WorldSettings.ini") {


    initSettings();

    _worldSettings.load();

    context_->RegisterSubsystem(new WorkQueue(context_));
    Impostor::RegisterObject(context);
    GetSubsystem<WorkQueue>()->CreateThreads(3);

    SimpleVector<int>::set_static_grow_callback(
        UrhoworldsDebug::record_simple_vector_reallocations);
}

void UrhoWorlds::Start () {
    Sample::Start();

    createScene();

    createInstructions();

    setupViewport();

    subscribeToEvents();

    //    Sample::InitMouseMode(MM_RELATIVE);

    _console.setSettingsPtr(&_cfg);
    _console.setCameraNodePtr(cameraNode_);
    _console.start();

    _appSettingsState.init(&_cfg);
    _gameplayState.init(&_cfg, &_worldSettings, cameraNode_);
    _mainMenu.addMenuElement("Play", &_gameplayState);
    _mainMenu.addMenuElement("Settings", &_appSettingsState);
    _stateManager.pushState(&_mainMenu);


    _cfg.getSetting("dbg.ft_chart")
        .addUpdateHandler(this,
                          std::bind(&UrhoWorlds::debugFrameTimeChartSettingUpdated,
                                    this));

    _frameTimeGraph.init(_orthoCameraNode, _cfg.getSetting("dbg.ft_chart").getBool());

    _fpsUiElement = GetSubsystem<UI>()->GetRoot()->CreateChild<Text>();
    _fpsUiElement->SetFont(GetSubsystem<ResourceCache>()->GetResource<Font>("Fonts/Anonymous Pro.ttf"), 15);

    _fpsUiElement->SetHorizontalAlignment(HA_LEFT);
    _fpsUiElement->SetVerticalAlignment(VA_TOP);
    _fpsUiElement->SetPosition(0, 0);
}

void UrhoWorlds::debugFrameTimeChartSettingUpdated () {
    _frameTimeGraph.setEnabled(_cfg.getSetting("dbg.ft_chart").getBool());
}


void UrhoWorlds::createScene () {
    scene_ = new Scene(context_);

    scene_->CreateComponent<Octree>();
    auto physics_world = scene_->CreateComponent<PhysicsWorld>();
    physics_world->SetFps(240);
    //scene_->CreateComponent<DebugRenderer>();


    Node* lightNode = scene_->CreateChild("DirectionalLight");
    lightNode->SetDirection(Vector3(0.f, -0.3f, -1.f)); // The direction vector does not need to be normalized

    Light* light = lightNode->CreateComponent<Light>();
    light->SetLightType(LIGHT_DIRECTIONAL);


    cameraNode_ = scene_->CreateChild("Camera");
    cameraNode_->CreateComponent<Camera>();

    cameraNode_->SetPosition(Vector3(0.0f, 3, 0));

    auto zone = scene_->GetOrCreateComponent<Zone>();

    zone->SetBoundingBox(Urho3D::BoundingBox(-100000, 100000));
    //const auto maxfloat = std::numeric_limits<float>::max();
    //zone->SetBoundingBox(BoundingBox(-maxfloat, maxfloat));
    zone->SetFogStart(5000);
    zone->SetFogEnd(100000);
    zone->SetFogColor(Color(0.7, 0.7, 0.7, 0.0));

    cameraNode_->GetComponent<Camera>()->SetFarClip(1000000.f);

    // second viewport
    Renderer* renderer = GetSubsystem<Renderer>();
    ResourceCache* cache = GetSubsystem<ResourceCache>();
    cache->SetAutoReloadResources(true);

    auto scene2 = new Scene(context_);
    scene2->CreateComponent<Octree>();
    scene2->CreateComponent<DebugRenderer>();
    _orthoCameraNode = scene2->CreateChild("cameraNode");

    auto camera2 = _orthoCameraNode->CreateComponent<Camera>();
    cameraNode_->SetPosition(Vector3(0.0f, 0.0f, -10.0f));

    camera2->SetOrthographic(true);
    camera2->SetProjectionOffset(Vector2(-0.5f, -0.5f));

    adjustOverlayLayerScreenSize();
    URHO3D_LOGINFO("Ortho camera projection offset: " + camera2->GetProjectionOffset().ToString());

    SubscribeToEvent(GetSubsystem<Graphics>(),
                     E_SCREENMODE,
                     URHO3D_HANDLER(UrhoWorlds, handleScreenResize));

    auto viewport2 = new Viewport(context_, scene2, camera2);

    auto overlayRenderPath = new RenderPath();
    overlayRenderPath->Load(cache->GetResource<XMLFile>("RenderPaths/ForwardOverlay.xml"));
    viewport2->SetRenderPath(overlayRenderPath);

    renderer->SetNumViewports(2);
    renderer->SetViewport(1, viewport2);
}

void UrhoWorlds::createInstructions () {
    // ResourceCache* cache = GetSubsystem<ResourceCache>();
    // UI* ui = GetSubsystem<UI>();

    // Text* instructionText = ui->GetRoot()->CreateChild<Text>();
    // instructionText->SetName("StatusLine");
    // instructionText->SetText("Use WASD keys and mouse/touch to move");
    // instructionText->SetFont(cache->GetResource<Font>("Fonts/Anonymous Pro.ttf"), 15);

    // instructionText->SetHorizontalAlignment(HA_CENTER);
    // instructionText->SetVerticalAlignment(VA_BOTTOM);
    // instructionText->SetPosition(0, 0);
}


void UrhoWorlds::setupViewport () {
    Renderer* renderer = GetSubsystem<Renderer>();

    SharedPtr<Viewport> viewport(new Viewport(context_, scene_, cameraNode_->GetComponent<Camera>()));
    ResourceCache* cache = GetSubsystem<ResourceCache>();
    auto rp = new RenderPath();
    rp->Load(cache->GetResource<XMLFile>("RenderPaths/ForwardDepth.xml"));
    viewport->SetRenderPath(rp);
    renderer->SetViewport(0, viewport);

    _shaderParameterConfigurator.setRenderPath(rp);
    //    _shaderParameterConfigurator.addSetting(
    //        _worldSettings.getSetting("sky.spot_brightness"), "SpotBrightness");
    //    _shaderParameterConfigurator.addSetting(
    //        _worldSettings.getSetting("sky.scatter_strength"), "ScatterStrength");
    for (auto &el : _worldSettings.getSection("sky")) {
        //_shaderParameterConfigurator.addSetting();
        auto dotpos = el.first.find('.') + 1;
        auto name = el.first.substr(dotpos, std::string::npos);
        auto &val = *el.second;
        _shaderParameterConfigurator.addSetting(val, name);
    }
}


void UrhoWorlds::subscribeToEvents () {
    SubscribeToEvent(E_UPDATE, URHO3D_HANDLER(UrhoWorlds, handleUpdate));
    SubscribeToEvent(E_KEYDOWN, URHO3D_HANDLER(UrhoWorlds, handleKeyDown));
}

void UrhoWorlds::handleKeyDown (StringHash tp, VariantMap& eventData) {
    int key = eventData[KeyUp::P_KEY].GetInt();

    if (key == KEY_F1) {
        if (_stateManager.currentState() != &_consoleState) {
            _stateManager.pushState(&_consoleState);
        }
    }

    Sample::HandleKeyDown(tp, eventData);
}

void UrhoWorlds::handleUpdate (StringHash /*eventType*/, VariantMap& eventData) {
    if (!_stateManager.update()) {
        engine_->Exit();
    }

    _fpsCounter += 1;
    _fpsTimer += eventData[Update::P_TIMESTEP].GetFloat();
    if (_fpsTimer >= 0.5f) {
        _fpsUiElement->SetText(ea::to_string(((float)_fpsCounter) / _fpsTimer));
        _fpsTimer = 0.0f;
        _fpsCounter = 0;
    }

}



void UrhoWorlds::initSettings () {
    _worldSettings.appendSetting(
        "sky.RayleighBrightness",
        V2::FloatValue(3.3f, V2::ArithmeticConstraint(0.f, 500.f)));
    _worldSettings.appendSetting(
        "sky.MieBrightness",
        V2::FloatValue(.1f, V2::ArithmeticConstraint(0.f, 500.f)));
    _worldSettings.appendSetting(
        "sky.SpotBrightness",
        V2::FloatValue(.50f, V2::ArithmeticConstraint(0.f, 500.f)));
    _worldSettings.appendSetting(
        "sky.ScatterStrength",
        V2::FloatValue(.028f, V2::ArithmeticConstraint(0.000f, 500.f)));

    _worldSettings.appendSetting(
        "sky.RayleighStrength",
        V2::FloatValue(.139f, V2::ArithmeticConstraint(0.000f, 500.f)));
    _worldSettings.appendSetting(
        "sky.MieStrength",
        V2::FloatValue(.264f, V2::ArithmeticConstraint(0.000f, 500.f)));
    _worldSettings.appendSetting(
        "sky.RayleighCollectionPower",
        V2::FloatValue(.81f, V2::ArithmeticConstraint(0.000f, 500.f)));
    _worldSettings.appendSetting(
        "sky.MieCollectionPower",
        V2::FloatValue(.39f, V2::ArithmeticConstraint(0.000f, 500.f)));
    _worldSettings.appendSetting(
        "sky.MieDistribution",
        V2::FloatValue(.028f, V2::ArithmeticConstraint(0.000f, 500.f)));
    _worldSettings.appendSetting(
        "sky.Intensity",
        V2::FloatValue(1.5f, V2::ArithmeticConstraint(0.000f, 500.f)));

    const auto v3_constraint_0_to_1 =
        V2::GenericArithmeticConstraint(Urho3D::Vector3(0.f, 0.f, 0.f),
            Urho3D::Vector3(1.f, 1.f, 1.f));

    _worldSettings.appendSetting(
        "sky.Kr",
        Vector3Value(Vector3(0.18, 0.49, 0.66),
            v3_constraint_0_to_1));

    // TODO3: cleanup
    _worldSettings.appendSetting(
        "test.subgroup.test",
        Vector3Value(Vector3(0.18, 0.49, 0.66),
            v3_constraint_0_to_1));

    // TODO20: add setLongName method to Setting2
    using V2::SettingsUtils::createArithmetic;
    using V2::SettingsUtils::create;
    using V2::SettingsUtils::createEnum;
    createArithmetic<V2::IntValue>(_worldSettings, "lscp.tile_size", 5, 255, 55);
    // .setLongName("Landscape tile size");

    createArithmetic<V2::IntValue>(_worldSettings, "lscp.tiles_num", 3, 50, 5);
    // .setLongName("Landscape number of tiles around to render");

    createArithmetic<V2::IntValue>(
        _worldSettings, "lscp.outer_tiles_num", 3, 50, 10);

    createArithmetic<V2::IntValue>(
        _worldSettings, "lscp.outer_levels_num", 3, 50, 10);

    create<V2::BoolValue>(_worldSettings, "lscp.debug_map", false);
    // .setLongName("Draw debug map for landscape");

    createArithmetic<V2::FloatValue>(
        _worldSettings, "lscp.color_dither_amt", 0.f, 1.f, 0.05f);
    createArithmetic<V2::FloatValue>(
        _worldSettings, "lscp.color_noise_amt", 0.f, 1.f, 0.05f);

    for (const std::string& set_prefix : { "lscp.noize1",
                                           "lscp.noize2",
                                           "lscp.noize3",
                                           "lscp.noize_color1",
                                           "lscp.noize_color2",
                                           "lscp.noize_color3", }) {
        createArithmetic<V2::FloatValue>(
            _worldSettings, set_prefix + "_freq", .000001f, 1.f, 0.01f);
        // .setLongName("Landscape generator noize frequency");

        createEnum<V2::EnumStringValue>(
            _worldSettings,
            set_prefix + "_type",
            { "Value",
              "ValueFractal",
              "Perlin",
              "PerlinFractal",
              "Simplex",
              "SimplexFractal",
              "Cellular",
              "WhiteNoise",
              "Cubic",
              "CubicFractal" },
            "Value");
        // .setLongName("Noise type");
        createEnum<V2::EnumStringValue>(
            _worldSettings,
            set_prefix + "_interp",
            { "Linear", "Hermite", "Quintic" },
            "Hermite");
        // .setLongName("Noise interpolation type");
        createEnum<V2::EnumStringValue>(
            _worldSettings,
            set_prefix + "_frac_type",
            { "FBM", "Billow", "RigidMulti" },
            "FBM");
        // .setLongName("Noise fractal type");

        createArithmetic<V2::IntValue>(
            _worldSettings, set_prefix + "_frac_octaves", 0, 9, 2);
        // .setLongName("Noise fractal octaves");

        createArithmetic<V2::FloatValue>(
            _worldSettings, set_prefix + "_frac_lacunarity", .01f, 100.f, 2.f);
        // .setLongName("Noise fractal octaves");

        createArithmetic<V2::FloatValue>(
            _worldSettings, set_prefix + "_frac_gain", .001f, 20.f, 0.5f);
        // .setLongName("Noise fractal gain");

        if (set_prefix != "lscp.noize_color") {
            createArithmetic<V2::IntValue>(
                _worldSettings, set_prefix + "_exponent", 1, 10, 1);
            // .setLongName("Noise exponent");

            createArithmetic<V2::FloatValue>(
                _worldSettings, set_prefix + "_ampl", 0.f, 5000.f, 5.f);
            // .setLongName("Noise amplitude");
        }
    }

    _cfg.appendSetting(Setting("debug_draw", static_cast<bool>(false)))
        .setLongName("World debug draw");

    _cfg.appendSetting(Setting("dbg.ft_chart", static_cast<bool>(false)))
        .setLongName("Draw frame times chart");


    _cfg.appendSetting(Setting("_camera_move_speed", 1.f, 2000.f, 20.f))
        .setLongName("Camera move speed");

    _cfg.appendSetting(Setting("_tree.selected_preset", std::string()))
        .setLongName("Tree preset");

    _cfg.appendSetting(Setting("dbg.nolscp", static_cast<bool>(false)))
        .setLongName("Do not load landscape");

    _cfg.appendSetting(Setting("dbg.notrees", static_cast<bool>(false)))
        .setLongName("Do not load trees");

    _cfg.load();
}


void UrhoWorlds::adjustOverlayLayerScreenSize () {
    IntVector2 szi = GetSubsystem<Graphics>()->GetSize();
    Vector2 sz = Vector2(float(szi.x_) * PIXEL_SIZE, float(szi.y_) * PIXEL_SIZE);

    _orthoCameraNode->GetComponent<Camera>()->SetOrthoSize(sz);
}


void UrhoWorlds::handleScreenResize (StringHash /*eventType*/,
                                     VariantMap& /*eventData*/) {
    adjustOverlayLayerScreenSize();
}
