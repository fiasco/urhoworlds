#pragma once

#include <tuple>
#include <memory>

#include <Urho3D/Scene/Component.h>
#include <Urho3D/Scene/Node.h>
#include <Urho3D/Graphics/RenderPath.h>

namespace Urho3D
{

class Texture2D;
class RenderPath;
class Model;
class Material;
class Viewport;
class Image;
class ImpostorBaker;

class Impostor : public Component
{
    URHO3D_OBJECT (Impostor, Component);
public:
    explicit Impostor(Context* context);
    virtual ~Impostor();

    static void RegisterObject(Context* context);

    virtual void OnMarkedDirty(Node* node) override;
    virtual void OnNodeSet(Node* node) override;

    Material* GetMaterial();

    Impostor* Clone();

protected:
    friend class ImpostorsCurator;
    //accessed by ImpostorsCurator;

    void CleanUp ();
    void SetNodeStaticModelsAutoHide ();

    Impostor(const Impostor &);
    Impostor& operator=(const Impostor& other) = delete;

    void HandleNodeRemoved(StringHash, VariantMap& evtData);

    void SetupScreen();

    std::shared_ptr<ImpostorBaker> baker_;

    Node* sceneCamera_ = nullptr;
    Node* screenNode_ = nullptr;

    int distance_ = 20;
    bool amCopied_ = false;
};

} // namespace Urho3D
