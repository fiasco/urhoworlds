// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com
#include "TreeGenerator.hpp"

using namespace Urho3D;


#include <algorithm>
#include <cmath>
#include <tuple>



#include <Urho3D/IO/Log.h>
#include <Urho3D/Core/Context.h>
#include <Urho3D/Scene/Scene.h>
#include <Urho3D/Math/Quaternion.h>
#include <Urho3D/Graphics/StaticModel.h>
#include <Urho3D/Resource/ResourceCache.h>
#include <Urho3D/Graphics/Material.h>
#include <Urho3D/Graphics/Viewport.h>
#include <Urho3D/Graphics/Camera.h>
#include <Urho3D/Graphics/Texture2D.h>
#include <Urho3D/Graphics/Graphics.h>
#include <Urho3D/Graphics/Technique.h>
#include <Urho3D/Graphics/Octree.h>
#include <Urho3D/Graphics/RenderPath.h>
#include <Urho3D/UI/UI.h>


#include "LandscapeGenerator.hpp"
#include "Impostor.hpp"
#include "VcppBits/MathUtils/MathUtils.hpp"
#include "Utils/SimpleVector.hpp"

using namespace VcppBits::MathUtils;

static constexpr int _seed = 1337;


void tree_generator_create_circle_vertices
        (SimpleVector<UrhoworldsTexturedVertexElement> &vertices,
         const Vector3 &center,
         const Vector3 &normal,
         const float radius,
         const int resolution,
         const float l /* distance to branch root */) {

    Quaternion orientation;
    orientation.FromRotationTo(Vector3::UP, normal);
    Vector3 p = orientation * Vector3(radius, 0, 0) + center;

    int i = 0;
    vertices.push_back({ p, (p-center).Normalized(), {0.f, l }});

    const float incr = 1.f / resolution;
    for (i += 1; i <= resolution; ++i) {
        const float angle = (float)i / (float)resolution * 360.0f;
        Vector3 v(radius * Cos(angle), 0, radius * Sin(angle));
        p = orientation * v + center;

        vertices.push_back({ p,
                             (p-center).Normalized(),
                             {incr * (float)i, l}});
    }

}
void tree_generator_create_segment (SimpleVector<UrhoworldsIndexElement> &indexes,
                                    SimpleVector<UrhoworldsTexturedVertexElement> &vertices,
                                    const Vector3 &root,
                                    const float root_radius,
                                    const Vector3 &tip,
                                    const float tip_radius,
                                    const int resolution,
                                    const Vector3 &end_normal,
                                    const float l /*distance to branch root*/) {
    const unsigned starting_index = unsigned(vertices.size());

    Vector3 normal = (tip - root).Normalized();
    // Vector3 side_ = side.Normalized();
    // Vector3 side2 = normal.CrossProduct(side_).Normalized();

    // vertices.push_back({ root, Vector3{0,0,1}, {255, 255, 255, 255}});
    // vertices.push_back({ normal, Vector3{0,0,1}, {255, 255, 255, 255}});
    // vertices.push_back({ root, Vector3{0,0,1}, {255, 255, 255, 255}});

    // vertices.push_back({ root, Vector3{0,0,1}, {255, 255, 255, 255}});
    // vertices.push_back({ side_, Vector3{0,0,1}, {255, 255, 255, 255}});
    // vertices.push_back({ root, Vector3{0,0,1}, {255, 255, 255, 255}});

    // vertices.push_back({ root, Vector3{0,0,1}, {255, 255, 255, 255}});
    // vertices.push_back({ side2, Vector3{0,0,1}, {250, 250, 250, 250}});
    // vertices.push_back({ root, Vector3{0,0,1}, {255, 255, 255, 255}});

    // circle 1
    tree_generator_create_circle_vertices
        (vertices, root, normal, root_radius, resolution, l);

    // circle 2
    tree_generator_create_circle_vertices
        (vertices, tip, end_normal, tip_radius, resolution, l + (tip - root).Length());

    //    for (int i = 0; i + 2 < vertices.size(); i+=3) {
    //        indexes.push_back({i, i+1, i+2});
    //    }

    for (unsigned j = starting_index; j < unsigned(resolution) + starting_index; ++j) {
        unsigned short point0_num = j;
        unsigned short point1_num = point0_num + resolution + 1;
        unsigned short point2_num = point0_num + 1;

        indexes.push_back({point0_num, point1_num, point2_num});

        unsigned short point5_num = point1_num + 1;

        indexes.push_back({point2_num, point1_num, point5_num});
    }
}

void tree_generator_continue_segment (SimpleVector<UrhoworldsIndexElement> &indexes,
                                      SimpleVector<UrhoworldsTexturedVertexElement> &vertices,
                                      const Vector3 &tip,
                                      const float tip_radius,
                                      const int resolution,
                                      const Vector3 &end_normal,
                                      const float l /*distance to branch root*/) {
    const unsigned starting_index = unsigned(vertices.size()) - unsigned(resolution) - 1;

    // circle 2
    tree_generator_create_circle_vertices
        (vertices, tip, end_normal, tip_radius, resolution, l);

    for (unsigned j = starting_index; j < unsigned(resolution) + starting_index; ++j) {
        unsigned short point0_num = j;
        unsigned short point1_num = point0_num + resolution + 1;
        unsigned short point2_num = point0_num + 1;

        indexes.push_back({point0_num, point1_num, point2_num});

        unsigned short point5_num = point1_num + 1;

        indexes.push_back({point2_num, point1_num, point5_num});
    }
}

void tree_generator_create_leaf (SimpleVector<UrhoworldsIndexElement> &indexes,
                                 SimpleVector<UrhoworldsTexturedVertexElement> &vertices,
                                 const TreeGenerator::Leaf &lf,
                                 const Urho3D::Vector3 &normal) {
    // root tri
    const auto sideways_vec = lf.vec.CrossProduct(lf.norm);

    float texcoord_divider = 3.f;

    const auto leaf_len = lf.vec.Length();
    const auto leaf_volume = leaf_len * 0.2f;

    const unsigned short i =
        (unsigned short) std::min(
            vertices.size(),
            size_t(std::numeric_limits<unsigned short>::max() - 3));

    vertices.push_back({ lf.pos, lf.norm, {0.5f / texcoord_divider, .0f} }); // root
    auto v2 = lf.pos + sideways_vec /3
        + lf.vec * 0.3f + lf.norm * leaf_volume;
    auto v3 = lf.pos - sideways_vec / 3
        + lf.vec * 0.3f + lf.norm * leaf_volume;

    auto v2_tex = Vector2(1.f, 1.f/3.f) / texcoord_divider;
    auto v3_tex = Vector2(0.f, 1/3.f) / texcoord_divider;;

    vertices.push_back({ v2, normal, v2_tex });
    vertices.push_back({ v3, normal, v3_tex });

    vertices.push_back({ lf.pos
                         + lf.vec,
                         normal,
                         {0.5f / texcoord_divider, 1.f / texcoord_divider } });



    indexes.push_back({ i,
                        static_cast<unsigned short>(i + 3),
                        static_cast<unsigned short>(i + 1)});
    indexes.push_back({ static_cast<unsigned short>(i),
                        static_cast<unsigned short>(i + 3),
                        static_cast<unsigned short>(i + 2)});
}


TreeGenerator::TreeGenerator ()
    : _mt (_seed),
      _mt_lf (_seed),
      _cfg () {
    _cfg.appendSetting(Setting("levels", 1, 5, 2));
    _cfg.appendSetting(Setting("branch_curvature_threshold", 0.f, 20.f, 5.f));
    _cfg.appendSetting(Setting("root_length", 0.1f, 200.f, 8.f));
    _cfg.appendSetting(Setting("smoothing_iterations", 0, 3, 2));
    _cfg.appendSetting(Setting("wireframe", static_cast<bool>(false)));
    _cfg.appendSetting(Setting("root_radius", 0.01f, 100.f, 0.2f));
    _cfg.appendSetting(Setting("leaves_level", 0, 4, 1));
    _cfg.appendSetting(Setting("leaves_size", 0.01f, 10.f, 0.1f));
    _cfg.appendSetting(Setting("leaves_normals_outwards", 0.0f, 1.f, 0.5f));
    _cfg.appendSetting(Setting("leaves_per_meter", 0, 100, 5));
    _cfg.appendSetting(Setting("leaves_offset", 0.f, .99f, 0.2f));


    for (int i = 0; i < 6; ++i) {
        const auto i_str = std::to_string(i);
        _cfg.appendSetting(Setting(i_str + ".acacia", 0.f, 1.f, 0.f));
        _cfg.appendSetting(Setting(i_str + ".segments", 1, 20, 10 - i));
        _cfg.appendSetting(Setting(i_str + ".endpoint_radius_multiplier", .001f, 0.99f, .1f));
        _cfg.appendSetting(Setting(i_str + ".branching_radius_multiplier", .1f, 1.f, .8f));
        _cfg.appendSetting(Setting(i_str + ".children", 1, 100, 5));
        _cfg.appendSetting(Setting(i_str + ".offset", 0.f, 1.f, 0.25f));

        _cfg.appendSetting(Setting(i_str + ".direction_outward", 0.f, 1.f, 0.9f));
        _cfg.appendSetting(Setting(i_str + ".direction_Zward", -1.f, 1.f, 0.3f));
        _cfg.appendSetting(Setting(i_str + ".direction_deviation", 0.f, 1.f, 0.0f));
        _cfg.appendSetting(Setting(i_str + ".direction_parent", -1.f, 1.f, 0.3f));
        _cfg.appendSetting(Setting(i_str + ".endpoint_direction_random", 0.f, 1.f, 0.0f));
        _cfg.appendSetting(Setting(i_str + ".endpoint_direction_outward", 0.f, 1.f, 0.9f));
        _cfg.appendSetting(Setting(i_str + ".endpoint_direction_Zward", -1.f, 1.f, 0.3f));
        _cfg.appendSetting(Setting(i_str + ".endpoint_influence", 0.f, 10.f, 0.0f));

        _cfg.appendSetting(Setting(i_str + ".growth_direction_deviation", 0.f, 1.f, 0.1f));
        _cfg.appendSetting(Setting(i_str + ".children_length_multiplier", 0.f, 10.f, 0.5f));
        _cfg.appendSetting(Setting(i_str + ".children_length_distribution_circle", 0.f, 1.f, 0.8f));
        _cfg.appendSetting(Setting(i_str + ".children_length_distribution_cone", -1.f, 1.f, 0.0f));
        _cfg.appendSetting(Setting(i_str + ".children_length_distribution_flame", 0.f, 1.f, 0.0f));

        _cfg.appendSetting(Setting(i_str + ".leaf_direction_outward", 0.f, 1.f, 0.5f));
        _cfg.appendSetting(Setting(i_str + ".leaf_direction_Zward", -1.f, 1.f, -0.1f));
        _cfg.appendSetting(Setting(i_str + ".leaf_direction_random", 0.f, 1.f, 0.5f));
        _cfg.appendSetting(Setting(i_str + ".leaf_direction_parent", -1.f, 1.f, 0.5f));
        _cfg.appendSetting(Setting(i_str + ".leaf_direction_influence", 0.f, 1.f, 0.5f));
    }


    auto it = _cfg.getSettingsIterator();
    while (it.isElement()) {
        it.getCurrent().addUpdateHandler(this, std::bind(&TreeGenerator::settingUpdated, this));

        it.peekNext();
    }


    _cfg.load();
}

void TreeGenerator::settingUpdated () {
    if (_context && !_pauseUpdates) {
        generate();
    }
}

Vector3 get_outward_direction (const Vector3 &vec,
                               const float pOutwardAngle) {
    Quaternion orientation;
    orientation.FromRotationTo(Vector3::UP, vec);


    Vector3 v(1 * Cos(pOutwardAngle), 0, Sin(pOutwardAngle));
    return orientation * v;
}

Vector3 TreeGenerator::getBranchInitialDirection (const int level,
                                                  const Vector3 &parent_dir,
                                                  const float pOutwardAngle) {
    const std::string lvl = std::to_string(level);
    std::uniform_real_distribution<float> dist (-1.0f , 1.0f);

    const Vector3 outward = get_outward_direction(parent_dir, pOutwardAngle).Normalized();
    const float outward_k = _cfg.get<float>(lvl + ".direction_outward");
    const float zward_k = _cfg.get<float>(lvl + ".direction_Zward");
    const float parent_k = _cfg.get<float>(lvl + ".direction_parent");

    const Vector3 deviation = Vector3(dist(_mt), dist(_mt), dist(_mt));
    const float deviation_k = _cfg.get<float>(lvl + ".direction_deviation");

    return (outward * outward_k
            + Vector3::UP * zward_k
            + deviation * deviation_k
            + parent_dir * parent_k).Normalized();
}

Vector3 TreeGenerator::find_child_vec (const Branch * pParent,
                                       const float pPosition,
                                       const float pOutwardAngle,
                                       const int pParentLevel) {
    const float ith = float(pParent->segments.size()) * pPosition;
    const size_t number = size_t(ith);

    auto vec = pParent->segments[number].vec.Normalized();
    vec = getBranchInitialDirection(pParentLevel + 1, vec, pOutwardAngle);

    return vec;
}

std::tuple<Vector3, Vector3, float>
TreeGenerator::find_child_pos (const Branch * pParent, const float pPosition) const {
    Vector3 pos = pParent->rootPos;
    const float ith = static_cast<float>(pParent->segments.size()) * pPosition;
    const size_t number = size_t(ith);
    const float remainder = ith - number;
    float radius = 0;
    for (size_t i = 0; i < number; ++i) {
        pos += pParent->segments[i].vec;
        radius = pParent->segments[i].endRadius;
    }
    if (number < pParent->segments.size()) {
        const auto segm = pParent->segments[number];
        pos += segm.vec * remainder;
        radius = radius * (1 - remainder) + segm.endRadius * remainder;
    }

    return std::make_tuple(pos, pParent->segments[number].vec, radius);
}




TreeGenerator::Branch* TreeGenerator::createBranch (const Branch * pParent,
                                                    const float pPosition,
                                                    const float pLength,
                                                    const float pOutwardAngle,
                                                    const int pParentLevel) {
    const auto lvl = std::to_string(pParentLevel + 1);
    tree_data.push_back(Branch{Vector3(0, 0, 0),
                               _cfg.get<float>("root_radius"),
                               {}});
    Branch &br = tree_data.back();
    Vector3 normal = Vector3::UP;
    Vector3 parent_direction;

    if (pParent) {
        std::tie(br.rootPos, parent_direction, br.rootRadius)
            = find_child_pos(pParent, pPosition);
        normal = find_child_vec(pParent, pPosition, pOutwardAngle, pParentLevel);
        br.rootRadius *= _cfg.get<float>(lvl + ".branching_radius_multiplier");
    }

    bb.Merge(br.rootPos);

    Vector3 prev_normal = normal;
    float radius = br.rootRadius;

    const float endpoint_outward_k =
        _cfg.get<float>(lvl + ".endpoint_direction_outward");
    const float endpoint_Zward_k =
        _cfg.get<float>(lvl + ".endpoint_direction_Zward");
    const float endpoint_random_k =
        _cfg.get<float>(lvl + ".endpoint_direction_random");
    const float endpoint_influence =
        _cfg.get<float>(lvl + ".endpoint_influence");
    const float radius_multi =
        _cfg.get<float>(lvl + ".endpoint_radius_multiplier"); // TODO deviate?

    std::uniform_real_distribution<float> dist(-endpoint_random_k,
                                               endpoint_random_k);
    Vector3 endpoint_outward = get_outward_direction(normal, pOutwardAngle);
    Vector3 endpoint_random = Vector3(dist(_mt), dist(_mt), dist(_mt));


    Vector3 endpoint_vec =
        (endpoint_outward * endpoint_outward_k
         + Vector3::UP * endpoint_Zward_k
         // create endpoint_parent
         + endpoint_random).Normalized();

    const int num_segments = _cfg.get<int>(lvl + ".segments");
    const float actual_segment_length = pLength / (float)num_segments;
    const float end_radius = br.rootRadius * radius_multi;

    Vector3 total_branch_vec;
    for (int i = 0; i < num_segments; ++i) {
        const float k = (float)i / (float) num_segments * endpoint_influence;
        const float inv_k = std::max(1.f - k, 0.f);
        const float growth_dev =
            _cfg.get<float>(lvl + ".growth_direction_deviation");
        std::uniform_real_distribution<float> gr_dist (-growth_dev, growth_dev);

        const Vector3 new_normal =
            (prev_normal + Vector3(gr_dist(_mt),
                                   gr_dist(_mt),
                                   gr_dist(_mt))).Normalized();

        const float kk = ((float)(i + 1)) / (float)num_segments;
        radius =  (kk) * end_radius + (1 - kk) * br.rootRadius;

        Vector3 res = (new_normal * inv_k + endpoint_vec * k).Normalized();

        br.segments.push_back({ res * actual_segment_length, radius});
        total_branch_vec += res * actual_segment_length;

        prev_normal = new_normal;
    }

    bb.Merge(total_branch_vec);

    for (int i = 0; i < _cfg.get<int>("smoothing_iterations"); ++i) {
        smoothBranch(&br);
    }
    return &br;
}

TreeGenerator::Leaf* TreeGenerator::createLeaf (const Branch * pParent,
                                                const float pPosition,
                                                const float pOutwardAngle,
                                                const int pParentLevel) {
    std::string lvl = std::to_string(pParentLevel);

    leaf_data.push_back(Leaf{Vector3(0, 0, 0),
                             Vector3(0, 0, 0),
                             Vector3(0, 0, 0)});

    Leaf &lf = leaf_data.back();
    //Vector3 normal = Vector3::UP;
    float ignore;

    Vector3 parent_direction;

    std::tie(lf.pos, parent_direction, ignore)
        = find_child_pos(pParent, pPosition);
    parent_direction.Normalize();

    auto outw = get_outward_direction(parent_direction, pOutwardAngle).Normalized();
    const float endpoint_outward_k =
        _cfg.get<float>(lvl + ".leaf_direction_outward");
    const float endpoint_Zward_k =
        _cfg.get<float>(lvl + ".leaf_direction_Zward");
    const float endpoint_random_k =
        _cfg.get<float>(lvl + ".leaf_direction_random");
    const float endpoint_parent_k =
        _cfg.get<float>(lvl + ".leaf_direction_parent");

    std::uniform_real_distribution<float> dist(-1, 1);
    Vector3 endpoint_random =
        Vector3(dist(_mt_lf), dist(_mt_lf), dist(_mt_lf)).Normalized();

    lf.vec =
        (outw * endpoint_outward_k
         + Vector3::UP * endpoint_Zward_k
         + parent_direction * endpoint_parent_k
         + endpoint_random * endpoint_random_k)
        .Normalized() * _cfg.get<float>("leaves_size");

    lf.norm =
        parent_direction.CrossProduct(lf.vec).CrossProduct(lf.vec).Normalized();


    return &lf;
}


std::vector<float> spread_angles(const size_t pNumAngles) {
    std::vector<float> outward_angles;

    outward_angles.reserve(pNumAngles);
    for (size_t i = 0; i < pNumAngles; ++i) {
        outward_angles.push_back(float((i * 140) % 360));
    }

    return outward_angles;
}


void TreeGenerator::fillLeaves (const Branch * pBranch, const float pLength, const int pParentLevel) {
    const float number_of_leaves = _cfg.get<int>("leaves_per_meter") * pLength;
    const float leaves_offset = _cfg.get<float>("leaves_offset");

    const size_t num_children =
        size_t(number_of_leaves)
        + ((number_of_leaves - float(size_t(number_of_leaves))) > 0.5f ? 1 : 0);

    if (!num_children) {
        return;
    }

    std::vector<float> outward_angles = spread_angles(num_children);

    float incr = (1 - leaves_offset) / num_children;
    std::uniform_real_distribution<float>
        dist (-incr/4, incr/4); //TODO make configurable impact on the increment

    for (size_t i = 0; i < num_children; ++i) {
        float outward_angle = outward_angles[i];

        const float pos = // TODO improve these calculations to get rid of clamp?
            clamp(leaves_offset + i * incr + dist(_mt_lf), 0.f, 1.f);

        createLeaf(pBranch, pos, outward_angle, pParentLevel);
    }
}

float TreeGenerator::calcBranchLength (const int pLevel,
                                       const float pParentLength,
                                       const float pChildPosition) const {
    std::string lvl = std::to_string(pLevel);
    const float length_multiplier
        = _cfg.get<float>(lvl + ".children_length_multiplier");

    const float circle_function
        = (-((pChildPosition*2 - 1)*(pChildPosition*2 - 1)) + 1);
    const float circle_function_k
        = _cfg.get<float>(lvl + ".children_length_distribution_circle");
    const float circle_part = circle_function * circle_function_k + 1 * (1 - circle_function_k);

    const float conical_cfg
        = _cfg.get<float>(lvl + ".children_length_distribution_cone");
    const float conical_k = std::fabs(conical_cfg);
    const float conical_function =
        (conical_cfg > 0)
        ? (1 - pChildPosition)
        : (pChildPosition);
    const float conic_part = conical_function * conical_k + 1 * (1 - conical_k);


    const float flame_k = _cfg.get<float>(lvl + ".children_length_distribution_flame");
    const float flame_function =
        (pChildPosition <= 0.7f)
        ? (0.5f + 0.5f * pChildPosition / 0.7f)
        : (0.5f + 0.5f * (1.f - pChildPosition) / 0.3f);
    const float flame_part = flame_function * flame_k + (1 - flame_k);

    return pParentLength * length_multiplier * circle_part * conic_part * flame_part;
}

// supposed to be called with a root branch
void TreeGenerator::fillChildren (const Branch *pBranch,
                                  const float pParentLength,
                                  const int pParentLevel) {
    const int curr_level = pParentLevel + 1;
    const std::string lvl = std::to_string(pParentLevel);

	int num_children_int = _cfg.get<int>(lvl + ".children");
    const float num_children = float(num_children_int);
    float incr = (1 - _cfg.get<float>(lvl + ".offset")) / num_children;

    std::uniform_real_distribution<float>
        dist (-incr/2, incr/2); //TODO make configurable impact on the increment

    std::vector<float> outward_angles = spread_angles(size_t(num_children));

    float acacia_pos_sum = 0.f;
    std::vector<std::tuple<Branch*, float, float>> children;
    children.reserve(size_t(num_children));

    for (size_t i = 0; i < size_t(num_children_int); ++i) {
        float outward_angle = outward_angles[i];
        const float pos =
            _cfg.get<float>(lvl + ".offset") + i * (incr) + dist(_mt);
        float branch_length
            = calcBranchLength (pParentLevel, pParentLength, pos);
        if (branch_length < 0) {
            continue;
        }
        auto *br = createBranch(pBranch,
                                pos,
                                branch_length,
                                outward_angle,
                                pParentLevel);
        children.push_back({br, branch_length, 0.f});

        // prepare data for acacia
        if (_cfg.get<float>(lvl + ".acacia") > 0.f) {
            std::get<2>(children.back()) = br->rootPos.y_;
            for (Segment &s : br->segments) {
                std::get<2>(children.back()) += s.vec.y_;
            }
            acacia_pos_sum += std::get<2>(children.back());
        }
    }

    // reusing the variable of same type heh :/
    acacia_pos_sum /= float(num_children);

    for (auto &el: children) {
        Branch* br = std::get<0>(el);
        const float& br_length = std::get<1>(el);
        const float& br_z_end_pos = std::get<2>(el);

        if (_cfg.get<float>(lvl + ".acacia") > 0.f) {
            const float &avg_pos = acacia_pos_sum;
            const float diff = br_z_end_pos - avg_pos;
            const float increment = diff / float(br->segments.size());
            for (Segment &s : br->segments) {
                s.vec.y_ -= increment * _cfg.get<float>(lvl + ".acacia");
            }
        }

        if (curr_level < _cfg.get<int>("levels")) {
            fillChildren(br, br_length, curr_level);
        }

        const auto levels = _cfg.get<int>("levels");
        if ((levels - pParentLevel) <= _cfg.get<int>("leaves_level")) {
            // if (br->level < (levels - _cfg.get<int>("leaves_level"))) {
            fillLeaves(br, br_length, pParentLevel);
        }
    }
}

void TreeGenerator::init (Context *pContext,
                          Scene* pScene,
                          ResourceCache *pCache,
                          const bool pDebugRender = false) {
    _context = pContext;
    _scene = pScene;
    _cache = pCache;
    _debugRender = pDebugRender;
    _isInitialized = true;

    generate();
}

void TreeGenerator::smoothBranch (TreeGenerator::Branch* pBranch) {
    std::vector<TreeGenerator::Segment> new_;
    if (pBranch->segments.size() < 2) {
        return;
    }

    float _r1 = pBranch->rootRadius + (pBranch->segments[0].endRadius - pBranch->rootRadius) * 3.f / 4.f;
    new_.push_back({pBranch->segments[0].vec * 3.f / 4.f,
                    _r1});

    for (size_t i = 1; i < pBranch->segments.size(); ++i) {
        auto segment1 = pBranch->segments[i-1];
        auto segment2 = pBranch->segments[i];
        float _r2 = segment1.endRadius + (segment2.endRadius - segment1.endRadius) / 4.f;
        new_.push_back({(segment1.vec + segment2.vec) / 4.f, _r2});

        if (i + 1 < pBranch->segments.size()) {
            float _r3 = segment1.endRadius + (segment2.endRadius - segment1.endRadius) * 3.f / 4.f;
            new_.push_back({segment2.vec / 2.f, _r3});
        } else {
            new_.push_back({segment2.vec * 3.f / 4.f,
                            segment2.endRadius});
        }
    }
    pBranch->segments = new_;
}

void TreeGenerator::generate () {
    bb = BoundingBox();
    Timer tm;
    UrhoworldsPlaneConstructionData<UrhoworldsTexturedVertexElement> dt;
    UrhoworldsPlaneConstructionData<UrhoworldsTexturedVertexElement> dt_leaf;

    tree_data.clear();
    leaf_data.clear();
    _mt.seed(_seed);
    _mt_lf.seed(_seed);

    const float root_len = _cfg.get<float>("root_length");
    const float offs = _cfg.get<float>("0.offset");

    auto root = createBranch(nullptr, 0, root_len, 0, -1);

    // TODO: consider configurable or just some other crown center posiition
    auto crown_pos_ = find_child_pos(root, (1.f - offs)/2.f + offs);

    _treeCrownCenter = std::get<0>(crown_pos_);

    fillChildren(root, root_len, 0);

    for (auto &branch : tree_data) {
        // assuming there's atleast one segment
        auto seg1 = branch.segments[0];
        Vector3 end_normal =
            // if there is a continuation, then use average of current and next
            // normals:
            (branch.segments.size() > 2)
            ? (branch.segments[1].vec.Normalized()
               + branch.segments[0].vec.Normalized()).Normalized()
            : branch.segments[0].vec.Normalized();

        tree_generator_create_segment(*dt.index_data,
                                      *dt.vertex_data,
                                      branch.rootPos,
                                      branch.rootRadius,
                                      branch.rootPos + seg1.vec,
                                      seg1.endRadius,
                                      8,
                                      end_normal,
                                      0.f);
        auto prev_root_pos = branch.rootPos + seg1.vec;
        float l = seg1.vec.Length();


        for (size_t i = 1; i <  branch.segments.size(); ++i) {
            bool isnt_last = i + 1 < branch.segments.size();
            const auto &segment = branch.segments[i];

            if (isnt_last) {
                auto &next_segment = branch.segments[i + 1];

                const float angle = segment.vec.Angle(next_segment.vec);
                if (angle < _cfg.get<float>("branch_curvature_threshold")) {
                    next_segment.vec += segment.vec;
                    continue;
                }
            }

            const Vector3 continue_normal =
                // if there is a continuation, then use average of current and next
                // normals:
                (isnt_last)
                ? (branch.segments[i].vec.Normalized() + branch.segments[i + 1].vec.Normalized()).Normalized()
                : branch.segments[i].vec.Normalized();
            l += segment.vec.Length();
            tree_generator_continue_segment(*dt.index_data,
                                            *dt.vertex_data,
                                            prev_root_pos + segment.vec,
                                            segment.endRadius,
                                            8,
                                            continue_normal,
                                            l);
            prev_root_pos = prev_root_pos + segment.vec;
        }
    }

    for (auto lf : leaf_data) {
        const float normals_outw_k = _cfg.get<float>("leaves_normals_outwards");
        auto norm = normals_outw_k * (lf.pos - _treeCrownCenter).Normalized()
            + (1 - normals_outw_k) * lf.norm;

        tree_generator_create_leaf(*dt_leaf.index_data,
                                   *dt_leaf.vertex_data,
                                   lf,
                                   norm);
    }

    auto op = _cfg.get<bool>("wireframe")
        ? Urho3D::LINE_LIST : Urho3D::TRIANGLE_LIST;

    _model = create_1lod_plane(_context,
                               dt,
                               bb.min_ * 1.05f,
                               bb.max_ * 1.05f,
                               op);

    _leafModel = create_1lod_plane(_context,
                                        dt_leaf,
                                        bb.min_ * 1.05f,
                                        bb.max_ * 1.05f,
                                        op);

    URHO3D_LOGINFO("Tree generated in "
                   + ea::to_string(((float)tm.GetMSec(false))) + "milliseconds");
    URHO3D_LOGINFO("indexes: " + ea::to_string(dt.index_data->size()) + "elements, tris:"
                   + ea::to_string(float(dt.index_data->size()) / 3));
    URHO3D_LOGINFO("vertices: " + ea::to_string(dt.vertex_data->size()));

    URHO3D_LOGINFO("leaf indexes: " + ea::to_string(dt_leaf.index_data->size()) + "elements, tris:"
                   + ea::to_string(float(dt_leaf.index_data->size()) / 3));
    URHO3D_LOGINFO("leaf vertices: " + ea::to_string(dt_leaf.vertex_data->size()));

    if (!_debugRender) {
        return;
    }

    if (!_sceneNode) {
        _sceneNode = _scene->CreateChild("a tree");
        _sceneNode->SetPosition(Vector3(0, 10, 0));
        modl =
            _sceneNode->CreateComponent<StaticModel>();
        leaf_modl =
            _sceneNode->CreateComponent<StaticModel>();
    }

    modl->SetModel(_model);
    modl->SetMaterial(_cache->GetResource<Material>("Tree/Bark.xml"));
    leaf_modl->SetModel(_leafModel);
    auto leaf_mat = _cache->GetResource<Material>("Tree/Leaf.xml");
    leaf_modl->SetMaterial(leaf_mat);

    if (!_sceneNode->HasComponent<Impostor>()) {
        auto imp = _sceneNode->CreateComponent<Impostor>();
        imp->GetMaterial()->SetShaderParameter(
            "AmbientColor",
            leaf_mat->GetShaderParameter("AmbientColor"));
        imp->GetMaterial()->SetShaderParameter(
            "MatSpecColor",
            leaf_mat->GetShaderParameter("MatSpecColor"));

        _sceneNode->GetComponent<Impostor>()->OnMarkedDirty(_sceneNode);
    } else {
        _sceneNode->GetComponent<Impostor>()->OnMarkedDirty(_sceneNode);
    }
}


std::tuple<Model*, Model*, Material*, Material*>
buildTree (Urho3D::Node *pNode) {
    // as it uses some Urho's capabilities, we can't make it static...
    static TreeGenerator *tree_gen = nullptr;
    if (!tree_gen) {
        tree_gen = new TreeGenerator;
        ea::vector<ea::string> presets;
        loadTreePresetsList(pNode->GetContext(), presets);
        if (presets.size()) {
            tree_gen->getConfig()->setFilename(presets[0].c_str());
            tree_gen->getConfig()->load();
        }
        tree_gen->init(pNode->GetContext(),
                       pNode->GetScene(),
                       pNode->GetSubsystem<ResourceCache>());
    }

    auto models = tree_gen->getModels();

    auto cache = pNode->GetSubsystem<ResourceCache>();

    return { models.first,
             models.second,
             cache->GetResource<Material>("Tree/Bark.xml"),
             cache->GetResource<Material>("Tree/Leaf.xml")
    };
}
