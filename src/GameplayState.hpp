#pragma once


#include <string>

#include <Urho3D/Container/Str.h>
#include <Urho3D/Core/Object.h>
#include <Urho3D/Math/StringHash.h>
#include <Urho3D/UI/Window.h>
#include <Urho3D/UI/DropDownList.h>


#include "StateManager.hpp"
#include "Landscape.hpp"
#include "SettingsGui.hpp"
#include "TreeGenerator.hpp"
#include "CameraController.hpp"
#include "WorldEditState.hpp"
#include "Settings.hpp"


namespace Urho3D {
    class Context;
    class Node;
}

class Settings;

class TreeEditState : public SettingsGuiState {
public:
    explicit TreeEditState (Urho3D::Context *pContext,
                            StateManager *pStateManager,
                            TreeGenerator *pTree)
        : SettingsGuiState (pContext, pStateManager),
          _tree (pTree) {
    }

    void init (Settings *pTreeSettings, Settings *pAppSettings) {
        SettingsGuiState::init(pTreeSettings);
        _appCfg = pAppSettings;
        _treeCfg = pTreeSettings;
        _presetSetting = &_appCfg->getSetting("_tree.selected_preset");
    }

    void load () override;

    void unload () override {
        SettingsGuiState::unload();
        _uiList = nullptr;
    }

    std::string getName () const override {
        return "TreeEditState";
    }

    void loadPreset ();

private:
    void forkButtonPressed (Urho3D::StringHash eventType,
                            Urho3D::VariantMap& eventData);
    void presetNameEntered (Urho3D::StringHash eventType,
                            Urho3D::VariantMap& eventData);
    void scanPresets ();
    void presetSelected (Urho3D::StringHash /*eventType*/,
                         Urho3D::VariantMap& eventData);
    void reloadPreset ();

    Settings *_treeCfg = nullptr;
    Settings *_appCfg = nullptr;
    Setting *_presetSetting = nullptr;
    ea::vector<ea::string> _presets;
    Urho3D::DropDownList *_uiList = nullptr;

    TreeGenerator *_tree;
};

class GameplayState : public IState,
                      public Urho3D::Object {
public:
    URHO3D_OBJECT(GameplayState, Urho3D::Object);

    GameplayState (Urho3D::Context* pContext,
                   StateManager *pStateManager);

    void init (Settings *pSettings,
               Settings2 *pWorldSettings,
               Urho3D::Node *pCameraNode);
    void load ();
    void unload ();

    void freeze (IState *pPreviouState) override;
    void resume ();

    void handleUpdate (Urho3D::StringHash eventType,
                       Urho3D::VariantMap& eventData);
    void spawnObject ();
    void handleKeyDown (Urho3D::StringHash eventType,
                        Urho3D::VariantMap& eventData);
    void handleConsoleCommand (Urho3D::StringHash eventType,
                               Urho3D::VariantMap& eventData);

    std::string getName () const { return "GameplayState"; }

private:
    Urho3D::Node* loadModel (const ea::string &pModelName,
                             const ea::string &pMaterial,
                             const Urho3D::Vector2 &pPos);

    Settings* _settings = nullptr;
    Settings2* _worldSettings = nullptr;
    Urho3D::Node *_cameraNode = nullptr;
    StateManager *_stateManager;

    Landscape _landscape;

    TreeGenerator _tree;
    CameraController _camCtl;

    TreeEditState _treeSettingsGui;
    SettingsGuiState _settingsGui;
    WorldEditState _worldEdit;
    ea::vector<ea::string> tree_presets;

    size_t _benchmarkResultsFramesNum = 0;
    float _benchmarkTimePassed = 0.f;
    bool _isBenchmarking = false;
};
