


#include <string>
#include <cmath>
#include <unordered_set>
#include <iterator>
#include <stdexcept>
#undef max
#include <limits>


#include <Urho3D/Math/Vector2.h>
#include <Urho3D/Math/Vector3.h>

namespace {
template<typename T>
T sqr (T pVal) {
    return pVal * pVal;
}


inline int landscape_position_divide (const float pVal, const int pMapTileSize) {
    if (pVal > 0) {
        return pVal / pMapTileSize + 1;
    }

    return pVal / pMapTileSize;
}


template<typename I, typename L, typename H>
auto _clamp (const I& pVal, const L &lo, const H &hi) ->decltype(pVal+lo+hi) {
    if (pVal < lo) {
        return lo;
    } else if (pVal > hi) {
        return hi;
    }
    return pVal;
}
}

struct hash_int_vector2 {
    size_t operator()(const Urho3D::IntVector2 &pVec) const noexcept {
        auto h1 = std::hash<int>()(pVec.x_);
        auto h2 = std::hash<int>()(pVec.y_);
        return ((h1 << (sizeof(size_t)/2)) | (h1 >> (sizeof(size_t)/2))) ^ h2;
    }
};


using SetIntVector2 = std::unordered_set<Urho3D::IntVector2, hash_int_vector2>;


class LandscapeManager {
public:
    LandscapeManager (const size_t pNumTilesToRender = 0,
                      const Urho3D::Vector3 &pCameraPosition = Urho3D::Vector3(),
                      const Urho3D::Vector3 &pCameraDirection = Urho3D::Vector3()) {
        _setCameraTransform(pCameraPosition, pCameraDirection);
        _setNumTilesToRender(pNumTilesToRender),
        reinit();
    }

    void setNumExcludedTilesToRender (const size_t pNum) {
        _numExcludedTilesToRender = pNum;
    }

    void reinit () {
        _positions.clear();
        _loadedPositions.clear();
        clearTilesToUnload();
        _clearExcludedTilesToUnload(); // TODO why does it have _?
        recompute();
    }

    void setCurrentCameraTransform (const Urho3D::Vector3 &pPosition,
                                    const Urho3D::Vector3 &pDirection) {
        _setCameraTransform(pPosition, pDirection);
        recompute();
    }

    /// e.g. 3 means => 1 tile exactly below the camera + 2 tiles on every
    /// direction totaling to 5by5 square
    void setNumTilesToRender (const size_t pNum) {
        _setNumTilesToRender(pNum);
        recompute();
    }

    void recompute () { // TODO make private
        _positions.clear();

        // add a tile at the odd edges, so that we can combine tiles with a
        // LandscapeManager containing twice bigger tiles
        auto edges = getEdges();

        for (int i = edges[0]; i <= edges[1]; ++i) {
            for (int j = edges[2]; j <= edges[3]; ++j) {
                auto new_pos = Urho3D::IntVector2(i, j) + _camPosition;
                if (!getTileIsLoaded(new_pos)
                    && (std::abs(i) >= _numExcludedTilesToRender
                        || std::abs(j) >= _numExcludedTilesToRender))
                    _positions.insert(new_pos);
            }
        }

        for (auto it = _loadedPositions.begin();
             it != _loadedPositions.end();) {
            const TileIsNeeded tile_need = getTileIsNeeded(*it);
            if (tile_need == TileIsNeeded::needed) {
                ++it;
            } else if (tile_need == TileIsNeeded::excluded) {
                _unneededExcludedPositions.insert(*it);
                _unneededPositions.insert(*it);
                it = _loadedPositions.erase(it);
            } else if (tile_need == TileIsNeeded::not_needed) {
                _unneededPositions.insert(*it);
                it = _loadedPositions.erase(it);
            }
        }
    }

    size_t getNumTilesToLoad () const {
        return _positions.size();
    }

    Urho3D::IntVector2 getNthTilePositionToLoad (const size_t n) const {
        auto it = _positions.begin();
        std::advance(it, n);
        return *it;
    }

    const SetIntVector2& getAllElementsToLoad () const {
        return _positions;
    }

    const SetIntVector2& getAllLoadedElements () const {
        return _loadedPositions;
    }

    const SetIntVector2& getAllElementsToUnload () const {
        return _unneededPositions;
    }

    const SetIntVector2& getAllExcludedElementsToUnload () const {
        return _unneededExcludedPositions;
    }


    void setTileIsLoaded (const Urho3D::IntVector2 &pTilePos) {
        auto found = _positions.find(pTilePos);
        if (found != std::end(_positions)) {
            _loadedPositions.insert(pTilePos);
            _positions.erase(found);
        } else {
            // already loaded, the caller should keep track, or manually check
            // which tiles are needed
            throw std::runtime_error("LandscapeManager::setTileIsLoaded(): "
                                     "tile is not needed");
        }
    }

    bool isOdd (const int &pNum) const {
        return (pNum > 0)
            ? (pNum % 2 == 0)
            : (abs(pNum) % 2 == 1);
    }

    std::vector<int> getEdges () const {
        if (_evenizeOurselves) {
            int edges = int(_numTilesToRender) - 1;
            int incr_x = 0;
            if ((_camPosition.x_) % 2 == 0) {
                incr_x = 1;
            }
            int incr_y = 0;
            if ((_camPosition.y_) % 2 == 0) {
                incr_y = 1;
            }

            const int const_incr = 1;

            return { -edges + const_incr +1- incr_x,
                     edges +const_incr +  - incr_x,
                     -edges +const_incr + 1 - incr_y,
                     edges +const_incr   - incr_y};
        } else {
            const int edges = int(_numTilesToRender) - 1;
            return { -edges, edges, -edges, edges };
        }
    }

    bool isTileExcluded (const Urho3D::IntVector2 &pTilePos) const {
         // int edges = int(_numExcludedTilesToRender - 1);
         // int const_incr_upper = -1;
         // int const_incr_lower = 1;
         // return ((pTilePos.x_ - _camPosition.x_ >= -edges + const_incr_lower
         //          && pTilePos.x_ - _camPosition.x_ <= edges + const_incr_upper)
         //         && (pTilePos.y_ - _camPosition.y_ >= -edges + const_incr_lower
         //              && pTilePos.y_ - _camPosition.y_ <= edges + const_incr_upper));

        // TODO: cleanup
        return !(abs(pTilePos.x_ - _camPosition.x_) >= _numExcludedTilesToRender
                 || abs(pTilePos.y_ - _camPosition.y_) >= _numExcludedTilesToRender);
    }

    enum class TileIsNeeded { not_needed, excluded, needed };

    TileIsNeeded getTileIsNeeded (const Urho3D::IntVector2 &pTilePos) const {
        auto edges = getEdges();
        if (isTileExcluded(pTilePos)) {
            return TileIsNeeded::excluded;
        }
        if ((pTilePos.x_ - _camPosition.x_) >= edges[0]
            && (pTilePos.x_ - _camPosition.x_) <= edges[1]
            && (pTilePos.y_ - _camPosition.y_) >= edges[2]
            && (pTilePos.y_ - _camPosition.y_) <= edges[3]) {
            return TileIsNeeded::needed;
        }
        return TileIsNeeded::not_needed;
    }

    bool getTileIsLoaded (const Urho3D::IntVector2 &pTilePos) const {
        return _loadedPositions.find(pTilePos) != std::end(_loadedPositions);
    }

    bool getTileIsOughtToBeLoaded (const Urho3D::IntVector2 &pTilePos) const {
        return _positions.find(pTilePos) != std::end(_positions);
    }

    // TODO u supposed to use clearAllTilesToUnload()
    void clearTilesToUnload () {
        _unneededPositions.clear();
    }

    // TODO u supposed to use clearAllTilesToUnload()
    void _clearExcludedTilesToUnload () {
        _unneededExcludedPositions.clear();
    }

    void clearAllTilesToUnload () {
        clearTilesToUnload();
        _clearExcludedTilesToUnload();
    }

    void setTileIsUnloaded (const Urho3D::IntVector2 &pTilePos) {
        // TODO remove when if it works fine?
        auto it_found = _unneededPositions.find(pTilePos);
        auto it_excluded_found = _unneededExcludedPositions.find(pTilePos);

        if (it_found == _unneededPositions.end()
            && it_excluded_found == _unneededExcludedPositions.end()) {
            throw std::runtime_error("LandscapeManager::setTileIsUnloaded(): "
                                     "unloading a not-loaded tile?");
        }

        {
            auto loaded_found = _loadedPositions.find(pTilePos);
            if (loaded_found != _loadedPositions.end()) {
                throw std::runtime_error(
                    "LandscapeManager::setTileIsUnloaded(): "
                    "fix your code? (unlikely internal error)");
            }
        }

        if (it_found != _unneededPositions.end()) {
            _unneededPositions.erase(it_found);
        }
        if (it_excluded_found != _unneededExcludedPositions.end()) {
            _unneededExcludedPositions.erase(it_excluded_found);
        }
    }

    void setEvenize (const bool pEvenize) {
        _evenizeOurselves = pEvenize;
    }

private:
    void _setCameraTransform (const Urho3D::Vector3 &pPosition,
                              const Urho3D::Vector3 &pDirection) {
        _camAbsolutePosition = pPosition;

        _camPosition = Urho3D::IntVector2(static_cast<int>(pPosition.x_),
                                          static_cast<int>(pPosition.z_));
        _camDirection = pDirection;
    }

    void _setNumTilesToRender (const size_t pNum) {
        _numTilesToRender = pNum;

        if (_numTilesToRender > size_t(std::numeric_limits<int>::max())) {
            throw std::runtime_error("LandscapeManager::_setNumTilesToRender(): "
                                     "requested more than int max");
        }
    }

    Urho3D::IntVector2 _camPosition;
    Urho3D::Vector3 _camAbsolutePosition;
    Urho3D::Vector3 _camDirection;
    SetIntVector2 _positions;
    SetIntVector2 _loadedPositions;
    SetIntVector2 _unneededPositions;
    SetIntVector2 _unneededExcludedPositions;

    size_t _numTilesToRender;
    size_t _numExcludedTilesToRender = 0;

    bool _evenizeOurselves = true; // whether add more tiles on outer edge so
                                   // that we can put twice bigger tiles manager
                                   // outside
};
