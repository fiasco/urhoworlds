#include <string>

#include <Urho3D/Graphics/RenderPath.h>
#include <Urho3D/Core/Variant.h>

#include "Settings.hpp"

class ShaderParameterConfigurator {
public:
    explicit ShaderParameterConfigurator (
        Urho3D::RenderPath *pRenderPath = nullptr)
        : _renderPath (pRenderPath) {
    }

    void setRenderPath (Urho3D::RenderPath* pRenderPath) {
        _renderPath = pRenderPath;
    }

    /// Note that shader parameter name and Setting name are totally different
    /// entities
    ///
    /// @param pSetting Setting to monitor
    /// @param pName shader parameter name
    void addSetting (Setting2& pSetting, const std::string &pName) {
        if (!_renderPath) {
            throw;
        }
        using namespace V2;
        switch (pSetting.getType()) {
        case SettingTypeEnum::BOOL:
            _establishAutoUpdate<BoolValue>(pSetting, pName);
            break;
        case SettingTypeEnum::INT:
            _establishAutoUpdate<IntValue>(pSetting, pName);
            break;
        case SettingTypeEnum::FLOAT:
            _establishAutoUpdate<FloatValue>(pSetting, pName);
            break;
        case SettingTypeEnum::ENUM_INT:
            _establishAutoUpdate<EnumIntValue>(pSetting, pName);
            break;
        case SettingTypeEnum::ENUM_FLOAT:
            _establishAutoUpdate<EnumFloatValue>(pSetting, pName);
            break;

        // TODO7: string parameters?
        case SettingTypeEnum::STRING:
            //_establishAutoUpdate<StringValue>(pSetting, pName);
            break;
        case SettingTypeEnum::ENUM_STRING:
            //_establishAutoUpdate<EnumStringValue>(pSetting, pName);
            break;
        case SettingTypeEnum::VECTOR3:
            _establishAutoUpdate<Vector3Value>(pSetting, pName);
            break;
        }
    }
private:
    template <typename T>
    void _establishAutoUpdate (Setting2 &pSetting,
                               const std::string pName) {
        pSetting.addUpdateHandler<T>(
            this,
            [this, pName] (const typename T::value_type& pNewVal) {
                _renderPath->SetShaderParameter(pName.c_str(), pNewVal);
            });
        _renderPath->SetShaderParameter(pName.c_str(), pSetting.get<T>());
    }

    Urho3D::RenderPath* _renderPath;
};
