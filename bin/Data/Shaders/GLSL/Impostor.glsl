#include "Uniforms.glsl"
#include "Samplers.glsl"
#include "Transform.glsl"
#include "ScreenPos.glsl"
#include "Lighting.glsl"
#include "Fog.glsl"
#include "Constants.glsl"

uniform int cImpostorResolution;
uniform float cImpostorDrawDistance;

varying vec4 vTexCoord;
varying vec4 vTangent;

varying vec3 vNormal;
varying vec4 vWorldPos;
#ifdef VERTEXCOLOR
    varying vec4 vColor;
#endif
#ifdef PERPIXEL
    #ifdef SHADOW
        #ifndef GL_ES
            varying vec4 vShadowPos[NUMCASCADES];
        #else
            varying highp vec4 vShadowPos[NUMCASCADES];
        #endif
    #endif
    #ifdef SPOTLIGHT
        varying vec4 vSpotPos;
    #endif
    #ifdef POINTLIGHT
        varying vec3 vCubeMaskVec;
    #endif
#else
    varying vec3 vVertexLight;
    varying vec4 vScreenPos;
    #ifdef ENVCUBEMAP
        varying vec3 vReflectionVec;
    #endif
    #if defined(LIGHTMAP) || defined(AO)
        varying vec2 vTexCoord2;
    #endif
#endif

float sqr (float x) {
    return x * x;
}

float calc_simple_square_stretching(float x, float y) {
    // https://arxiv.org/ftp/arxiv/papers/1509/1509.06344.pdf
    // page 3, Disc to square mapping:
    if (sqr(x) >= sqr(y)) {
        return sign(x) * (sqrt(sqr(x) + sqr(y)));
    }
    return sign(y) * x / y * (sqrt(sqr(x) + sqr(y)));
}

// returns -1..1 coordinates
vec2 PyramidUV(vec3 direction) {
    vec3 dir = normalize(direction);
    vec2 uv;

    uv.x = calc_simple_square_stretching(dir.x, dir.z);
    uv.y = calc_simple_square_stretching(dir.z, dir.x);

    return uv / (1 + dir.y);
}

vec2 GetCurrentUV(vec3 cam_dir)
{
    vec2 puv = PyramidUV(cam_dir);

    vec2 pyr = (puv + vec2(1, 1)) * 0.5f;

    return pyr;
}

float GetClosestCoord (const float coord,
                       const int impostorResolution) {
    float res = float(impostorResolution);
    float rounded = round(coord * res);
    return float(rounded) / res;
}

vec2 GetClosestCoord2 (const vec2 coord,
                       const int impostorResolution) {
    return vec2(GetClosestCoord(coord.x, impostorResolution),
                GetClosestCoord(coord.y, impostorResolution));
}

mat3 fromAxes (vec3 xAxis, vec3 yAxis, vec3 zAxis) {
    //mat3 m = mat3(
    //xAxis.x, yAxis.x, zAxis.x,
    //xAxis.y, yAxis.y, zAxis.y,
    //xAxis.z, yAxis.z, zAxis.z
    //);

     mat3 m = mat3(xAxis.xyz,
                   yAxis.xyz,
                   zAxis.xyz);

    return m;
}

mat3 directionMatrix(vec3 normal, vec3 direction) {
    vec3 V = normalize(cross(normal, direction));
    float phi = acos(dot(normal, direction));

    float rcos = cos(phi);
    float rsin = sin(phi);

    mat3 M;

    M[0][0] =        rcos + V.x * V.x * (1.0 - rcos);
    M[1][0] =  V.z * rsin + V.y * V.x * (1.0 - rcos);
    M[2][0] = -V.y * rsin + V.z * V.x * (1.0 - rcos);


    M[0][1] = -V.z * rsin + V.x * V.y * (1.0 - rcos);
    M[1][1] =        rcos + V.y * V.y * (1.0 - rcos);
    M[2][1] = -V.x * rsin + V.z * V.y * (1.0 - rcos);


    M[0][2] =  V.y * rsin + V.x * V.z * (1.0 - rcos);
    M[1][2] = -V.x * rsin + V.y * V.z * (1.0 - rcos);
    M[2][2] =        rcos + V.z * V.z * (1.0 - rcos);


    return M;
}

mat3 rotationFromLookDir (vec3 direction, vec3 up) {
    mat3 ret;
    vec3 forward = normalize(direction);

    vec3 v = cross(forward, up);
    // If direction & up are parallel and crossproduct becomes zero, use FromRotationTo() fallback
    if (sqr(length(v)) >= M_EPSILON) {
        v = normalize(v);
        vec3 upp = cross(v, forward);
        vec3 right = cross(upp, forward);
        ret = fromAxes(right, upp, forward);
    }
    else { // TODO: fallback workaround
        ret = directionMatrix(vec3(0, 0, -1), forward);
    }
    return ret;
}

// TODO inout might be expensive?
void assign_rotation (inout mat4 dest, mat3 rot) {
    dest[0][0] = rot[0][0];
    dest[0][1] = rot[0][1];
    dest[0][2] = rot[0][2];

    dest[1][0] = rot[1][0];
    dest[1][1] = rot[1][1];
    dest[1][2] = rot[1][2];

    dest[2][0] = rot[2][0];
    dest[2][1] = rot[2][1];
    dest[2][2] = rot[2][2];
}

mat3 get_rot_from_mat4 (mat4 m) {
    mat3 m3 = transpose(mat3(m));

    // extract the scaling factors
    float scaling_x = length(m3[0]);
    float scaling_y = length(m3[1]);
    float scaling_z = length(m3[2]);

    // and remove all scaling from the matrix
    m3[0] /= scaling_x;
    m3[1] /= scaling_y;
    m3[2] /= scaling_z;
    return m3;
}

varying mat3 invrot;
varying float alpha_fade_in;

struct ImpostorUvData {
    vec2 offset1, offset2, offset3, offset4;
};

float get_next_coord (const float base,
                      const float offset,
                      const float imp_res) {
    return base + 1.0 / (imp_res) * (offset >= 0 ? 1.0 : -1.0);
}

ImpostorUvData GetCurrentUVs (const vec2 curr,
                              const int impostor_resolution) {
    vec2 base = GetClosestCoord2(curr, cImpostorResolution - 1);
    vec2 offset = curr - base;

    vec2 curr_amts = vec2(1.0 - abs(offset.x * (cImpostorResolution - 1)),
                          1.f - abs(offset.y * (cImpostorResolution - 1)));
    
    vec2 next_x =
        vec2(get_next_coord(base.x, offset.x, cImpostorResolution - 1),
             base.y);
    vec2 next_y =
        vec2(base.x,
             get_next_coord(base.y, offset.y, cImpostorResolution - 1));

    vec2 next_xy = vec2(next_x.x, next_y.y);

    float conv = (cImpostorResolution - 1.f) / cImpostorResolution;

    ImpostorUvData ret;
    ret.offset1 = base * conv;
    ret.offset2 = next_x * conv;
    ret.offset3 = next_y * conv;
    ret.offset4 = next_x * conv;

    return ret;
}

vec3 UVtoPyramid(vec2 uv) {
    float leftright_negation = 1.0 - abs(uv.x);
    float frontback_negation = 1.0 - abs(uv.y);

    float up = min(leftright_negation, frontback_negation);
    vec3 position = vec3(uv.x, up, uv.y);

    return normalize(position);
}

mat3 mat3_mix (mat3 m1, mat3 m2, float fac) {
    mat3 ret = mat3 (mix(m1[0][0], m2[0][0], fac),
                     mix(m1[0][1], m2[0][1], fac),
                     mix(m1[0][2], m2[0][2], fac),
                     
                     mix(m1[1][0], m2[1][0], fac),
                     mix(m1[1][1], m2[1][1], fac),
                     mix(m1[1][2], m2[1][2], fac),
                     
                     mix(m1[2][0], m2[2][0], fac),
                     mix(m1[2][1], m2[2][1], fac),
                     mix(m1[2][2], m2[2][2], fac));

    return ret;
}


void VS()
{
    mat4 modelMatrix = iModelMatrix;
    mat4 mm = iModelMatrix;

    vec3 up = vec3(0,1,0);

    float scale = sqrt(sqr(mm[0][0]) + sqr(mm[1][0]) + sqr(mm[2][0]));

    vec3 loc = vec3(modelMatrix[0][3], modelMatrix[1][3], modelMatrix[2][3]);
    //loc.y += 10;
    vec3 cam_dir = (cCameraPos - loc);
    float distance = length(cam_dir);
    alpha_fade_in = 1;
    // TODO3: fade at the last 5% of the distacne
    if (distance < cImpostorDrawDistance) {
        float min_distance = distance * 0.95;
        alpha_fade_in = (distance-min_distance)/(cImpostorDrawDistance-min_distance);
        alpha_fade_in = clamp(alpha_fade_in, 0, 1);
    }

    cam_dir.y = max(0, cam_dir.y);

    cam_dir = normalize(cam_dir);

    invrot = inverse(get_rot_from_mat4(iModelMatrix));
    vec2 pyr = GetCurrentUV(invrot * cam_dir);

    vec2 ImpostorUvOffset =
        GetClosestCoord2(pyr * (float(cImpostorResolution)
                                / (float(cImpostorResolution + 1))),
                         cImpostorResolution);


    // rotate object itself
    mat3 scale_mat = mat3(scale);

    //// a variant that points at the same direction it was rendered at
    vec2 back_val = ImpostorUvOffset  * (float(cImpostorResolution) / (float(cImpostorResolution) - 1.0)) * 2.0 - vec2(1.0, 1.0);
    vec3 uv_to_pyr = UVtoPyramid(-back_val);
    uv_to_pyr.y = -uv_to_pyr.y;
    //vec3 upp = mat3(cView) * vec3(0,1,0);
    mat3 the_rot = rotationFromLookDir(uv_to_pyr, up);

    mat3 nr1 = scale_mat * inverse(the_rot) * invrot;

    //// a variant where we behave as usual billboard
    mat3 nr2 = scale_mat * inverse(rotationFromLookDir(-cam_dir, up));


    //mat3 nr = mat3_mix(nr1, nr2, 0.3);
    mat3 nr = nr1;


    assign_rotation(mm, nr);

    vec3 worldPos = (iPos * mm).xyz;
    //vec3 worldPos = GetWorldPos(modelMatrix);
    gl_Position = GetClipPos(worldPos);

    vNormal = GetWorldNormal(mm);
    vWorldPos = vec4(worldPos, GetDepth(gl_Position));



    vec4 tangent = GetWorldTangent(mm);
    vec3 bitangent = cross(tangent.xyz, vNormal) * iTangent.w;
    vTexCoord = vec4(GetTexCoord(iTexCoord), bitangent.xy);
    vTangent = vec4(tangent.xyz, bitangent.z);

    vTexCoord.xy = vTexCoord.xy / cImpostorResolution + ImpostorUvOffset;


    #ifdef VERTEXCOLOR
    vColor = iColor;
    #endif



    #ifdef PERPIXEL
        // Per-pixel forward lighting
        vec4 projWorldPos = vec4(worldPos, 1.0);

        #ifdef SHADOW
            // Shadow projection: transform from world space to shadow space
            for (int i = 0; i < NUMCASCADES; i++)
                vShadowPos[i] = GetShadowPos(i, vNormal, projWorldPos);
        #endif

        #ifdef SPOTLIGHT
            // Spotlight projection: transform from world space to projector texture coordinates
            vSpotPos = projWorldPos * cLightMatrices[0];
        #endif
    
        #ifdef POINTLIGHT
            vCubeMaskVec = (worldPos - cLightPos.xyz) * mat3(cLightMatrices[0][0].xyz, cLightMatrices[0][1].xyz, cLightMatrices[0][2].xyz);
        #endif
    #else
        // Ambient & per-vertex lighting
        #if defined(LIGHTMAP) || defined(AO)
            // If using lightmap, disregard zone ambient light
            // If using AO, calculate ambient in the PS
            vVertexLight = vec3(0.0, 0.0, 0.0);
            vTexCoord2 = iTexCoord1;
        #else
            vVertexLight = GetAmbient(GetZonePos(worldPos));
        #endif
        
        #ifdef NUMVERTEXLIGHTS
            for (int i = 0; i < NUMVERTEXLIGHTS; ++i)
                vVertexLight += GetVertexLight(i, worldPos, vNormal) * cVertexLights[i * 3].rgb;
        #endif
        
        vScreenPos = GetScreenPos(gl_Position);

        #ifdef ENVCUBEMAP
            vReflectionVec = worldPos - cCameraPos;
        #endif
    #endif
}


vec2 parallax_mapping (sampler2D pSampler, vec2 texCoords, vec3 viewDir)
{
    float height_scale = 0.1;

#define parallax_v2
#ifdef parallax_v1
    float height =  texture2D(pSampler, texCoords).r * 2.0 - 1.0;
    vec2 p = viewDir.xy / viewDir.z * (height * height_scale);
    return texCoords - p;
#endif //  parallax_v1
#ifdef parallax_v2
    // количество слоев глубины
    float numLayers = 10;

    if (true) {
        const float minLayers = 8.0;
        const float maxLayers = 32.0;
        numLayers = mix(maxLayers, minLayers, abs(dot(vec3(0.0, 0.0, 1.0), viewDir)));
    }

    // размер каждого слоя
    float layerDepth = 1.0 / numLayers;
    // глубина текущего слоя
    float currentLayerDepth = 0.0;
    // величина шага смещения текстурных координат на каждом слое
    // расчитывается на основе вектора P
    vec2 P = viewDir.xy * height_scale; 
    vec2 deltaTexCoords = P / numLayers;

    vec2  currentTexCoords     = texCoords;
    float currentDepthMapValue = texture2D(pSampler, currentTexCoords).r * 2.0 - 1.0;

    while(currentLayerDepth < currentDepthMapValue) {
        // смещаем текстурные координаты вдоль вектора P
        currentTexCoords -= deltaTexCoords;

        // делаем выборку из карты глубин в текущих текстурных координатах
        currentDepthMapValue = texture(pSampler, currentTexCoords).r * 2.0 - 1.0;
        // рассчитываем глубину следующего слоя
        currentLayerDepth += layerDepth;
    }

    return currentTexCoords;
#endif // parallax_v2

#ifdef parallax_v3
    // количество слоев глубины
    float numLayers = 10;

    if (true) {
        const float minLayers = 8.0;
        const float maxLayers = 32.0;
        numLayers = mix(maxLayers, minLayers, abs(dot(vec3(0.0, 0.0, 1.0), viewDir)));
    }

    // размер каждого слоя
    float layerDepth = 1.0 / numLayers;
    // глубина текущего слоя
    float currentLayerDepth = 0.0;
    // величина шага смещения текстурных координат на каждом слое
    // расчитывается на основе вектора P
    vec2 P = viewDir.xy * height_scale; 
    vec2 deltaTexCoords = P / numLayers;

    vec2  currentTexCoords     = texCoords;
    float currentDepthMapValue = texture(pSampler, currentTexCoords).r * 2.0 - 1.0;

    while(currentLayerDepth < currentDepthMapValue) {
        // смещаем текстурные координаты вдоль вектора P
        currentTexCoords -= deltaTexCoords;

        // делаем выборку из карты глубин в текущих текстурных координатах
        currentDepthMapValue = texture(pSampler, currentTexCoords).r * 2.0 - 1.0;
        // рассчитываем глубину следующего слоя
        currentLayerDepth += layerDepth;
    }


    // находим текстурные координаты перед найденной точкой пересечения,
    // т.е. делаем "шаг назад"
    vec2 prevTexCoords = currentTexCoords + deltaTexCoords;

    // находим значения глубин до и после нахождения пересечения 
    // для использования в линейной интерполяции
    float afterDepth  = currentDepthMapValue - currentLayerDepth;
    float beforeDepth = texture(pSampler, prevTexCoords).r - currentLayerDepth + layerDepth;
 
    // интерполяция текстурных координат 
    float weight = afterDepth / (afterDepth - beforeDepth);
    vec2 finalTexCoords = prevTexCoords * weight + currentTexCoords * (1.0 - weight);

    return finalTexCoords;
#endif //parallax_v3
}

void PS()
{
    float tex_bias = -1.5;

    // parallax...
    vec4 tex_coord = vTexCoord;
		mat3 tbn = mat3(vTangent.xyz, vec3(tex_coord.zw, vTangent.w), vNormal);

    vec3 camVec = -normalize(vWorldPos.xyz - cCameraPosPS);
		camVec = transpose(tbn) * camVec;
		camVec = normalize(camVec);
		camVec.y *= -1.;

    tex_coord.xy = parallax_mapping(sEmissiveMap, tex_coord.xy, camVec);

#define GetDisplacement ((texture2D(sEmissiveMap, tex_coord.xy).r) * 2.0 - 1.0)
    float cParallaxDisplacement = -0.9;
    float cParallaxIters = 100;
    float cOffsetLimit = 1;
    float parallax_iters = 10;

    for(float i = 0.0; i<parallax_iters; i+= 1.0) {
        // original stuff from some Urho3D dude
        //tex_coord.xy -= camVec.xy*cParallaxDisplacement*(GetDisplacement)/
        //( max(-camVec.z,cOffsetLimit)*cParallaxIters);

        //tex_coord.xy -= camVec.xy*cParallaxDisplacement*(GetDisplacement)/
        //(-camVec.z*cParallaxIters);
    }


    // Get material diffuse albedo
    if (alpha_fade_in < 0.01) {
        discard;
    }
    #ifdef DIFFMAP
    vec4 diffInput = texture2D(sDiffMap, tex_coord.xy, tex_bias);

        diffInput.a *= alpha_fade_in;
        #ifdef ALPHAMASK
            if (diffInput.a < 0.5)
                discard;
        #endif
        vec4 diffColor = cMatDiffColor * diffInput;
    #else
        vec4 diffColor = cMatDiffColor;
    #endif

    #ifdef VERTEXCOLOR
        diffColor *= vColor;
    #endif
    
    // Get material specular albedo
    #ifdef SPECMAP
        vec3 specColor = cMatSpecColor.rgb * texture2D(sSpecMap, tex_coord.xy).rgb;
    #else
        vec3 specColor = cMatSpecColor.rgb;
    #endif

    // Get normal
    vec3 nrm1 = texture2D(sNormalMap, tex_coord.xy, tex_bias).rgb * 2 - vec3(1,1,1);
    vec3 normal = normalize(nrm1 * invrot);
    //normal = vNormal;



    // Get fog factor
    #ifdef HEIGHTFOG
        float fogFactor = GetHeightFogFactor(vWorldPos.w, vWorldPos.y);
    #else
        float fogFactor = GetFogFactor(vWorldPos.w);
    #endif

    #if defined(PERPIXEL)
        // Per-pixel forward lighting
        vec3 lightColor;
        vec3 lightDir;
        vec3 finalColor;

        float diff = GetDiffuse(normal, vWorldPos.xyz, lightDir);

        #ifdef SHADOW
            diff *= GetShadow(vShadowPos, vWorldPos.w);
        #endif
    
        #if defined(SPOTLIGHT)
            lightColor = vSpotPos.w > 0.0 ? texture2DProj(sLightSpotMap, vSpotPos).rgb * cLightColor.rgb : vec3(0.0, 0.0, 0.0);
        #elif defined(CUBEMASK)
            lightColor = textureCube(sLightCubeMap, vCubeMaskVec).rgb * cLightColor.rgb;
        #else
            lightColor = cLightColor.rgb;
        #endif
    
        #ifdef SPECULAR
            float spec = GetSpecular(normal, cCameraPosPS - vWorldPos.xyz, lightDir, cMatSpecColor.a);

            finalColor = diff * lightColor * (diffColor.rgb + spec * specColor * cLightColor.a);
        #else
            finalColor = diff * lightColor * diffColor.rgb;
        #endif

        #ifdef AMBIENT
            finalColor += cAmbientColor.rgb * diffColor.rgb;
            finalColor += cMatEmissiveColor;
            gl_FragColor = vec4(GetFog(finalColor, fogFactor), diffColor.a);
        #else
            gl_FragColor = vec4(GetLitFog(finalColor, fogFactor), diffColor.a);
        #endif
    #elif defined(PREPASS)
        // Fill light pre-pass G-Buffer
        float specPower = cMatSpecColor.a / 255.0;

        gl_FragData[0] = vec4(normal * 0.5 + 0.5, specPower);
        gl_FragData[1] = vec4(EncodeDepth(vWorldPos.w), 0.0);
    #elif defined(DEFERRED)
        // Fill deferred G-buffer
        float specIntensity = specColor.g;
        float specPower = cMatSpecColor.a / 255.0;

        vec3 finalColor = vVertexLight * diffColor.rgb;
        #ifdef AO
            // If using AO, the vertex light ambient is black, calculate occluded ambient here
            finalColor += texture2D(sEmissiveMap, vTexCoord2).rgb * cAmbientColor.rgb * diffColor.rgb;
        #endif

        #ifdef ENVCUBEMAP
            finalColor += cMatEnvMapColor * textureCube(sEnvCubeMap, reflect(vReflectionVec, normal)).rgb;
        #endif
        #ifdef LIGHTMAP
            finalColor += texture2D(sEmissiveMap, vTexCoord2).rgb * diffColor.rgb;
        #endif
        #ifdef EMISSIVEMAP
            finalColor += cMatEmissiveColor * texture2D(sEmissiveMap, tex_coord.xy).rgb;
        #else
            finalColor += cMatEmissiveColor;
        #endif

        gl_FragData[0] = vec4(GetFog(finalColor, fogFactor), 1.0);
        gl_FragData[1] = fogFactor * vec4(diffColor.rgb, specIntensity);
        gl_FragData[2] = vec4(normal * 0.5 + 0.5, specPower);
        gl_FragData[3] = vec4(EncodeDepth(vWorldPos.w), 0.0);
    #else
        // Ambient & per-vertex lighting
        vec3 finalColor = vVertexLight * diffColor.rgb;
        #ifdef AO
            // If using AO, the vertex light ambient is black, calculate occluded ambient here
            finalColor += texture2D(sEmissiveMap, vTexCoord2).rgb * cAmbientColor.rgb * diffColor.rgb;
        #endif
        
        #ifdef MATERIAL
            // Add light pre-pass accumulation result
            // Lights are accumulated at half intensity. Bring back to full intensity now
            vec4 lightInput = 2.0 * texture2DProj(sLightBuffer, vScreenPos);
            vec3 lightSpecColor = lightInput.a * lightInput.rgb / max(GetIntensity(lightInput.rgb), 0.001);

            finalColor += lightInput.rgb * diffColor.rgb + lightSpecColor * specColor;
        #endif

        #ifdef ENVCUBEMAP
            finalColor += cMatEnvMapColor * textureCube(sEnvCubeMap, reflect(vReflectionVec, normal)).rgb;
        #endif
        #ifdef LIGHTMAP
            finalColor += texture2D(sEmissiveMap, vTexCoord2).rgb * diffColor.rgb;
        #endif
        #ifdef EMISSIVEMAP
            finalColor += cMatEmissiveColor * texture2D(sEmissiveMap, vTexCoord.xy).rgb;
        #else
            finalColor += cMatEmissiveColor;
        #endif

        gl_FragColor = vec4(GetFog(finalColor, fogFactor), diffColor.a);
    #endif
}
