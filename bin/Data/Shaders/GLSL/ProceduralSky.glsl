//  @class ProcSky
//  @brief Procedural Sky component for Urho3D
//  @author carnalis <carnalis.j@gmail.com>
//  @license MIT License
//  @copyright
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#include "Uniforms.glsl"
#include "Samplers.glsl"
#include "Transform.glsl"
#include "ScreenPos.glsl"
#include "PostProcess.glsl"
#line 29

varying vec2 vTexCoord;

varying vec3 vFarRay;
varying highp vec2 vScreenPos;

void VS() {
    mat4 modelMatrix = iModelMatrix;
    vec3 worldPos = GetWorldPos(modelMatrix);
    gl_Position = GetClipPos(worldPos);
    vTexCoord = GetQuadTexCoord(gl_Position);
    vFarRay = normalize(GetFarRay(gl_Position));
    vScreenPos = GetScreenPosPreDiv(gl_Position);
}


#ifdef COMPILEPS

uniform vec3 cLightDir;
uniform vec3 cKr;// = vec3(0.18, 0.49, 0.66) ;


uniform float cRayleighBrightness;// = 3.3;
uniform float cMieBrightness;// = 0.1;
uniform float cSpotBrightness;// = 50;
uniform float cScatterStrength;// = 0.028;
uniform float cRayleighStrength;// = 0.139;
uniform float cMieStrength;// = 0.264;
uniform float cRayleighCollectionPower;// = 0.81;
uniform float cMieCollectionPower;// = 0.39;
uniform float cMieDistribution;// = 0.63;
uniform float cIntensity;// = 1.5;

float surfaceHeight = 0.99; // < 1
const int stepCount = 8;


float AtmosphericDepth(vec3 pos, vec3 dir) {
  float a = dot(dir, dir);
  float b = 2.0 * dot(dir, pos);
  float c = dot(pos, pos) - 1.0;
  float det = b * b - 4.0 * a * c;
  float detSqrt = sqrt(det);
  float q = (-b - detSqrt) / 2.0;
  float t1 = c / q;
  return t1;
}

float Phase(float alpha, float g) {
  float a = 3.0 * (1.0 - g * g);
  float b = 2.0 * (2.0 + g * g);
  float c = 1.0 + alpha * alpha;
  float d = pow(1.0 + g * g - 2.0 * g * alpha, 1.3);
  return (a / b) * (c / d);
}

float HorizonExtinction(vec3 pos, vec3 dir, float radius) {
  float u = dot(dir, -pos);
  if(u < 0.0) {
    return 1.0;
  }
  vec3 near = pos + u * dir;
  if(length(near) < radius) {
    return 0.0;
  } else {
    vec3 v2 = normalize(near) * radius - pos;
    float diff = acos(dot(normalize(v2), dir));
    return smoothstep(0.0, 1.0, pow(diff * 2.0, 3.0));
  }
}

vec3 Absorb(float dist, vec3 color, float factor) {
  return color - color * pow(cKr, vec3(factor / dist));
}

vec3 getSkyImpl (vec3 pEyeDir, vec3 lightDir, float pSpotMultiplier) {
    lightDir.y *= -1.0; // Invert world Z for Urho.
    //    pEyeDir.y *= -1.0; // Invert world Z for Urho.
  float alpha = max(dot(pEyeDir, lightDir), 0);
  float rayleighFactor = Phase(alpha, -0.01) * cRayleighBrightness;
  float mieFactor = Phase(alpha, cMieDistribution) * cMieBrightness;
  float spot = smoothstep(0.0, 15.0, Phase(alpha, 0.9995)) * cSpotBrightness * pSpotMultiplier;
  vec3 eyePos = vec3(0.0, surfaceHeight, 0.0);
  float eyeDepth = AtmosphericDepth(eyePos, pEyeDir);
  float stepLength = eyeDepth / float(stepCount);
  float eyeExtinction = HorizonExtinction(eyePos, pEyeDir, surfaceHeight -2);

  vec3 rayleighCollected = vec3(0.0, 0.0, 0.0);
  vec3 mieCollected = vec3(0.0, 0.0, 0.0);

  for(int i = 0; i < stepCount; ++i) {
    float sampleDistance = stepLength * float(i);
    vec3 pos = eyePos + pEyeDir * sampleDistance;
    float extinction = HorizonExtinction(pos, lightDir, surfaceHeight - 0.35);
    float sampleDepth = AtmosphericDepth(pos, lightDir);
    vec3 influx = Absorb(sampleDepth, vec3(cIntensity), cScatterStrength) * extinction;
    rayleighCollected += Absorb(sampleDistance, cKr * influx, cRayleighStrength);
    mieCollected += Absorb(sampleDistance, influx, cMieStrength);
  }

  rayleighCollected = (rayleighCollected * eyeExtinction * pow(eyeDepth, cRayleighCollectionPower)) / float(stepCount);
  mieCollected = (mieCollected * eyeExtinction * pow(eyeDepth, cMieCollectionPower)) / float(stepCount);

  return mieFactor * mieCollected + spot * mieCollected + rayleighFactor * rayleighCollected;
}
#endif

float ran (vec2 st) {
    return fract(sin(dot(st.xy,
                         vec2(12.9898,78.233)))*
                 43758.5453123);
}

//uniform vec2 cGBufferInvSize;

void PS() {
    vec3 sun_dir = normalize(vec3(0.0, -0.3, 1));

    // TODO4: consider some dithering
    // denominator is supposed to be current screen resolution
    // vec2 st = vScreenPos/vec2(0.22, 0.1);
    // float rnd = ran(st);

    vec3 pixel_dir = normalize(vFarRay);

    float depth = DecodeDepth(texture2D(sEmissiveMap, vTexCoord).rgb) * 20;
    depth = clamp(depth, 0.0, 1);

    gl_FragColor = vec4(getSkyImpl(pixel_dir, sun_dir, pow(depth,50)), depth);
}
