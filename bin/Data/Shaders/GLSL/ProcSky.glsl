//  @class ProcSky
//  @brief Procedural Sky component for Urho3D
//  @author carnalis <carnalis.j@gmail.com>
//  @license MIT License
//  @copyright
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#include "Uniforms.glsl"
#include "ScreenPos.glsl"
#include "Transform.glsl"
#line 27

varying vec2 vTexCoord;
varying vec3 vNormal; // TODO cleanup?
varying vec3 vWorldPos; // TODO cleanup?
varying vec3 vFarRay;

void VS() {
    mat4 modelMatrix = iModelMatrix;
    vec3 worldPos = GetWorldPos(modelMatrix);
    gl_Position = GetClipPos(worldPos);
    gl_Position.z = gl_Position.w; // THIS
    vTexCoord = GetQuadTexCoord(gl_Position); // THIS
    // Flip GLSL texture coord vertically.
    vTexCoord.y = 1.0 - vTexCoord.y; // AND THIS

    vNormal = GetWorldNormal(modelMatrix); // my shit
    vWorldPos = normalize(worldPos);
    vFarRay = GetFarRay(gl_Position);
}

#include "ProcSkyCommon.glsl"
#line 43

#if defined COMPILEPS
vec3 GetWorldNormal() {
    // take texture coord, which is 0..1
    vec2 fragCoord = vTexCoord;
    // turn it to -1..1
    fragCoord = (fragCoord - 0.5) * 2.0;
    // put it to -1..1, -1..1, 0, 1
    vec4 deviceNormal = vec4(fragCoord, 0.0, 1.0);
    // multiplying by inverse of projection  matrix?
    vec3 eyeNormal = normalize((cInvProj * deviceNormal).xyz);
    // multiplying by inverse of rotation matrix?
    vec3 worldNormal = normalize(cInvViewRot * eyeNormal);

    return worldNormal;
}

#endif

void PS() {
    gl_FragColor = vec4(getSky(GetWorldNormal()), 1.0);
}
