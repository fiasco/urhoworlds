
#include "Uniforms.glsl"
#include "Samplers.glsl"
#include "Transform.glsl"
#include "ScreenPos.glsl"
#include "Lighting.glsl"
#include "Fog.glsl"

#line 9

varying vec2 vTexCoord;

#ifndef GL_ES
varying vec2 vDetailTexCoord;
#else
varying mediump vec2 vDetailTexCoord;
#endif

varying vec3 vNormal;
varying vec4 vWorldPos;


in vec4 aColor;

varying vec4 vColor1;
varying vec4 vColor2;
varying float vNoize;
varying vec4 vDebugg;

#ifdef PERPIXEL
    #ifdef SHADOW
        #ifndef GL_ES
            varying vec4 vShadowPos[NUMCASCADES];
        #else
            varying highp vec4 vShadowPos[NUMCASCADES];
        #endif
    #endif
    #ifdef SPOTLIGHT
        varying vec4 vSpotPos;
    #endif
    #ifdef POINTLIGHT
        varying vec3 vCubeMaskVec;
    #endif
#else
    varying vec3 vVertexLight;
    varying vec4 vScreenPos;
    #ifdef ENVCUBEMAP
        varying vec3 vReflectionVec;
    #endif
    #if defined(LIGHTMAP) || defined(AO)
        varying vec2 vTexCoord2;
    #endif
#endif

uniform sampler2D sDetailMap0;
uniform sampler2D sDetailMap1;
uniform sampler2D sDetailMap2;
uniform sampler2D sDetailMap3;
uniform sampler2D sDetailMap4;
uniform sampler2D sDetailMap5;
uniform sampler2D sDetailMap6;
uniform sampler2D sDetailMap7;

#ifndef GL_ES
uniform vec2 cDetailTiling;
#else
uniform mediump vec2 cDetailTiling;
#endif

void VS()
{
    int ir = int(aColor.r);
    int ig = int(aColor.g);
    int ib = int(aColor.b);
    int ia = int(aColor.a);

    float wei[7] = float[7](0.0,0.0,0.0,0.0,0.0,0.0,0.0);

    int tex_num1 = ir >> 4;
    int tex_num2 = ir & 0xF;
    int tex_num3 = ig >> 4;
    vNoize = clamp(float((ig & 0xF)) / 15.0, 0, 1);

    wei[tex_num1] = float((ib >> 4)) / 15.0;
    wei[tex_num2] = float((ib & 0xF)) / 15.0;
    wei[tex_num3] = 1.0 - (wei[tex_num1] + wei[tex_num2]);

    vDebugg = vec4 (wei[tex_num1] + wei[tex_num2] + wei[tex_num3], 0, 0, 1);

    vColor1 = vec4(wei[0], wei[1], wei[2], wei[3]);
    vColor2 = vec4(wei[4], wei[5], wei[6], 0);

    mat4 modelMatrix = iModelMatrix;
    vec3 worldPos = GetWorldPos(modelMatrix);
    gl_Position = GetClipPos(worldPos);
    vNormal = GetWorldNormal(modelMatrix);
    vWorldPos = vec4(worldPos, GetDepth(gl_Position));
    vTexCoord = GetTexCoord(iTexCoord);
    vDetailTexCoord = cDetailTiling * vTexCoord;

    #ifdef PERPIXEL
        // Per-pixel forward lighting
        vec4 projWorldPos = vec4(worldPos, 1.0);

        #ifdef SHADOW
            // Shadow projection: transform from world space to shadow space
            for (int i = 0; i < NUMCASCADES; i++)
                vShadowPos[i] = GetShadowPos(i, vNormal, projWorldPos);
        #endif

        #ifdef SPOTLIGHT
            // Spotlight projection: transform from world space to projector texture coordinates
            vSpotPos = projWorldPos * cLightMatrices[0];
        #endif
    
        #ifdef POINTLIGHT
            vCubeMaskVec = (worldPos - cLightPos.xyz) * mat3(cLightMatrices[0][0].xyz, cLightMatrices[0][1].xyz, cLightMatrices[0][2].xyz);
        #endif
    #else
        // Ambient & per-vertex lighting
        #if defined(LIGHTMAP) || defined(AO)
            // If using lightmap, disregard zone ambient light
            // If using AO, calculate ambient in the PS
            vVertexLight = vec3(0.0, 0.0, 0.0);
            vTexCoord2 = iTexCoord1;
        #else
            vVertexLight = GetAmbient(GetZonePos(worldPos));
        #endif
        
        #ifdef NUMVERTEXLIGHTS
            for (int i = 0; i < NUMVERTEXLIGHTS; ++i)
                vVertexLight += GetVertexLight(i, worldPos, vNormal) * cVertexLights[i * 3].rgb;
        #endif
        
        vScreenPos = GetScreenPos(gl_Position);

        #ifdef ENVCUBEMAP
            vReflectionVec = worldPos - cCameraPos;
        #endif
    #endif
}

float get_n (float pVal, int num,  int nth) {
    float nth_ = 1.f / (num - 1) * nth;
    return 1 - abs(pVal - nth_);
}

float getNoize(float nz, vec2 world_pos) {
    float scale = 1000;
    float nz1 = (texture2D(sDetailMap7, world_pos / scale / (1.0 / 17.0)).r) * get_n(nz, 3, 0);
    float nz2 = (texture2D(sDetailMap7, world_pos / scale / (7.0 / 17.0)).g) * get_n(nz, 3, 1);
    float nz3 = (texture2D(sDetailMap7, world_pos / scale / (3.0 / 17.0)).b) * get_n(nz, 3, 2);
    return 0.75 + (nz1 + nz2 + nz3) * 0.5; // so that it spans from 0.75 to 1.25
}

void PS()
{
    // Get material diffuse albedo

    // assuming we have everyting normalized already

    // 13 17 19 23 -- prime numbers

    vec4 diffColor = cMatDiffColor *
        (vColor1.r * texture2D(sDetailMap0, vWorldPos.xz / 5) +
         vColor1.g * texture2D(sDetailMap1, vWorldPos.xz / 5 / (13.0 / 17.0)) +
         vColor1.b * texture2D(sDetailMap2, vWorldPos.xz / 5 / (19.0 / 17.0)) +
         vColor1.a * texture2D(sDetailMap3, vWorldPos.xz / 5 / (23.0 / 17.0)) +
         vColor2.r * texture2D(sDetailMap4, vWorldPos.xz / 5) +
         vColor2.g * texture2D(sDetailMap5, vWorldPos.xz / 5 / (13.0 / 17.0)) +
         vColor2.b * texture2D(sDetailMap6, vWorldPos.xz / 5 / (19.0 / 17.0))
         ) * getNoize(vNoize, vWorldPos.xz);

    // Get material specular albedo
    vec3 specColor = cMatSpecColor.rgb;

    // Get normal
    vec3 normal = normalize(vNormal);

    // Get fog factor
    #ifdef HEIGHTFOG
        float fogFactor = GetHeightFogFactor(vWorldPos.w, vWorldPos.y);
    #else
        float fogFactor = GetFogFactor(vWorldPos.w);
    #endif

    #if defined(PERPIXEL)
        // Per-pixel forward lighting
        vec3 lightColor;
        vec3 lightDir;
        vec3 finalColor;
        
        float diff = GetDiffuse(normal, vWorldPos.xyz, lightDir);

        #ifdef SHADOW
            diff *= GetShadow(vShadowPos, vWorldPos.w);
        #endif
    
        #if defined(SPOTLIGHT)
            lightColor = vSpotPos.w > 0.0 ? texture2DProj(sLightSpotMap, vSpotPos).rgb * cLightColor.rgb : vec3(0.0, 0.0, 0.0);
        #elif defined(CUBEMASK)
            lightColor = textureCube(sLightCubeMap, vCubeMaskVec).rgb * cLightColor.rgb;
        #else
            lightColor = cLightColor.rgb;
        #endif
    
        #ifdef SPECULAR
            float spec = GetSpecular(normal, cCameraPosPS - vWorldPos.xyz, lightDir, cMatSpecColor.a);
            finalColor = diff * lightColor * (diffColor.rgb + spec * specColor * cLightColor.a);
        #else
            finalColor = diff * lightColor * diffColor.rgb;
        #endif

        #ifdef AMBIENT
            finalColor += cAmbientColor.rgb * diffColor.rgb;
            finalColor += cMatEmissiveColor;
            gl_FragColor = vec4(GetFog(finalColor, fogFactor), diffColor.a);
        #else
            gl_FragColor = vec4(GetLitFog(finalColor, fogFactor), diffColor.a);
        #endif
    #elif defined(PREPASS)
        // Fill light pre-pass G-Buffer
        float specPower = cMatSpecColor.a / 255.0;

        gl_FragData[0] = vec4(normal * 0.5 + 0.5, specPower);
        gl_FragData[1] = vec4(EncodeDepth(vWorldPos.w), 0.0);
    #elif defined(DEFERRED)
        // Fill deferred G-buffer
        float specIntensity = specColor.g;
        float specPower = cMatSpecColor.a / 255.0;

        gl_FragData[0] = vec4(GetFog(vVertexLight * diffColor.rgb, fogFactor), 1.0);
        gl_FragData[1] = fogFactor * vec4(diffColor.rgb, specIntensity);
        gl_FragData[2] = vec4(normal * 0.5 + 0.5, specPower);
        gl_FragData[3] = vec4(EncodeDepth(vWorldPos.w), 0.0);
    #else
        // Ambient & per-vertex lighting
        vec3 finalColor = vVertexLight * diffColor.rgb;

        #ifdef MATERIAL
            // Add light pre-pass accumulation result
            // Lights are accumulated at half intensity. Bring back to full intensity now
            vec4 lightInput = 2.0 * texture2DProj(sLightBuffer, vScreenPos);
            vec3 lightSpecColor = lightInput.a * lightInput.rgb / max(GetIntensity(lightInput.rgb), 0.001);

            finalColor += lightInput.rgb * diffColor.rgb + lightSpecColor * specColor;
        #endif

        gl_FragColor = vec4(GetFog(finalColor, fogFactor), diffColor.a);
    #endif
}
