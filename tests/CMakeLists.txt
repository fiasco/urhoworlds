set(SRCS
  ${CMAKE_SOURCE_DIR}/tests/TestsMain.cpp
  ${CMAKE_SOURCE_DIR}/tests/ImpostorsTests.cpp
  ${CMAKE_SOURCE_DIR}/tests/UrhoworldsMemoryTests.cpp
  ${CMAKE_SOURCE_DIR}/tests/LandscapeManagerTests.cpp
  ${CMAKE_SOURCE_DIR}/src/LandscapeGenerator.cpp
  ${CMAKE_SOURCE_DIR}/contrib/FastNoise/FastNoise.cpp
  ${CMAKE_SOURCE_DIR}/src/Utils/SimpleVector.cpp
  )


add_executable(tests EXCLUDE_FROM_ALL "${SRCS}")

set_target_properties(tests PROPERTIES
  RUNTIME_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/bin"
  RUNTIME_OUTPUT_DIRECTORY_DEBUG "${CMAKE_BINARY_DIR}/bin"
  RUNTIME_OUTPUT_DIRECTORY_RELEASE "${CMAKE_BINARY_DIR}/bin"
  )



include_directories("${CMAKE_SOURCE_DIR}/contrib/FastNoise")
include_directories("${CMAKE_SOURCE_DIR}/contrib/ProcSky")
include_directories("${CMAKE_SOURCE_DIR}/contrib/catch2")
include_directories("${CMAKE_SOURCE_DIR}/src")

#set(CMAKE_MODULE_PATH "${CMAKE_MODULE_PATH};${CMAKE_SOURCE_DIR}/tests/")


target_link_libraries(tests Urho3D ${URHOWORLDS_ADDITIONAL_LIBS})
if (WIN32)
  set_target_properties(tests PROPERTIES
    LINK_FLAGS /SUBSYSTEM:CONSOLE
    )
endif()

add_custom_target(test
  COMMAND ${CMAKE_BINARY_DIR}/bin/tests
  DEPENDS tests
  COMMENT "Running tests..."
  )

set(LANDSCAPE_MANAGER_BENCHMARKS_SRCS
  LandscapeManagerBenchmarks.cpp
  )

add_executable(landscape_manager_benchmarks
  EXCLUDE_FROM_ALL
  "${LANDSCAPE_MANAGER_BENCHMARKS_SRCS}")
target_link_libraries(landscape_manager_benchmarks Urho3D ${URHOWORLDS_ADDITIONAL_LIBS})

set_target_properties(landscape_manager_benchmarks PROPERTIES
  RUNTIME_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/bin"
  RUNTIME_OUTPUT_DIRECTORY_DEBUG "${CMAKE_BINARY_DIR}/bin"
  RUNTIME_OUTPUT_DIRECTORY_RELEASE "${CMAKE_BINARY_DIR}/bin"
  )


add_custom_target(benchmark
  COMMAND ${CMAKE_BINARY_DIR}/bin/landscape_manager_benchmarks
  DEPENDS landscape_manager_benchmarks
  COMMENT "Running tests..."
  )
