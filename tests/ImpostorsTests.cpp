// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com
#pragma once

#include "Impostor.hpp"
#include "Impostor.cpp"

#include <string>

using namespace Urho3D;

struct ApproxV2 {
    explicit ApproxV2 (const Vector2 &pVec)
        : _vec (pVec) {
    }

    Vector2 _vec;
};

struct ApproxV3 {
    explicit ApproxV3 (const Vector3 &pVec)
        : _vec (pVec) {
    }

    Vector3 _vec;
};



std::ostream& operator << ( std::ostream& os, Vector2 const& value ) {
    os << std::string(value.ToString().c_str());
    return os;
}

std::ostream& operator << ( std::ostream& os, ApproxV2 const& value ) {
    os << value._vec;
    return os;
}


std::ostream& operator << ( std::ostream& os, Vector3 const& value ) {
    os << std::string(value.ToString().c_str());
    return os;
}

std::ostream& operator << ( std::ostream& os, ApproxV3 const& value ) {
    os << value._vec;
    return os;
}


#include "CatchConfig.hpp"
#include "catch.hpp"

bool operator== (const ApproxV2 &pAppr, const Vector2 &pOther) {
    return pOther.x_ == Approx(pAppr._vec.x_)
        && pOther.y_ == Approx(pAppr._vec.y_);
}

bool operator== (const Vector2 &pOther, const ApproxV2 &pAppr) {
    return pAppr == pOther;
}

bool operator== (const ApproxV3 &pAppr, const Vector3 &pOther) {
    return pOther.x_ == Approx(pAppr._vec.x_)
        && pOther.y_ == Approx(pAppr._vec.y_)
        && pOther.z_ == Approx(pAppr._vec.z_);
}

bool operator== (const Vector3 &pOther, const ApproxV3 &pAppr) {
    return pAppr == pOther;
}


TEST_CASE( "calc_simple_square_stretching", "[Impostor]" ) {
    REQUIRE(calc_simple_square_stretching(Vector2(1, 1).Normalized())
            ==
            ApproxV2(Vector2(1, 1)));

    Vector2 vector_x1 { 1.f, 0.f };
    Vector2 vector_x1_stretched_to_circle = calc_simple_square_stretching(vector_x1);
    REQUIRE(vector_x1_stretched_to_circle == ApproxV2(vector_x1));

    REQUIRE(calc_simple_square_stretching(Vector2(0,0))
            ==
            ApproxV2(Vector2(0,0)));
    REQUIRE(calc_simple_square_stretching(Vector2(0, 0.5f))
            ==
            ApproxV2(Vector2(0.f, 0.5f)));
    REQUIRE(calc_simple_square_stretching(Vector2(0.5, 0.5f)).Length()
            >=
            0.999999f);

    REQUIRE(calc_simple_square_stretching(Vector2(0.5, 0.5f))
            ==
            ApproxV2(-calc_simple_square_stretching(Vector2(-0.5, -0.5f))));
}

TEST_CASE( "PyramidUV", "[Impostor]" ) {
    // TODO: sign inconsistency
    REQUIRE(PyramidUV({1, 0, 1}) == ApproxV2(Vector2{1, 1}));
    REQUIRE(PyramidUV({-1, 0, 1}) == ApproxV2(Vector2{-1, 1}));
    REQUIRE(PyramidUV({1, 0, -1}) == ApproxV2(Vector2{1, -1}));
    REQUIRE(PyramidUV({0, 0, -1}) == ApproxV2(Vector2{0, -1}));
    REQUIRE(PyramidUV({-1, 0, 0}) == ApproxV2(Vector2{-1, 0}));
    REQUIRE(PyramidUV({1, 0, 0}) == ApproxV2(Vector2{1, 0}));
    REQUIRE(PyramidUV({0, 0, 1}) == ApproxV2(Vector2{0, 1}));
    REQUIRE(PyramidUV({100, 0, 100}) == ApproxV2(Vector2{1, 1}));
    REQUIRE(PyramidUV({0, 1, 0}) == ApproxV2(Vector2{0, 0}));
    REQUIRE(PyramidUV({1, 1, 1}) == ApproxV2(Vector2{0.517638f, 0.517638f}));
}

TEST_CASE( "UVtoPyramid", "[Impostor]" ) {
    REQUIRE(UVtoPyramid(Vector2(1, 1)) == ApproxV3(Vector3{1, 0, 1}.Normalized()));
    REQUIRE(UVtoPyramid(Vector2(0, 0)) == ApproxV3(Vector3{0, 1, 0}));
    REQUIRE(UVtoPyramid(Vector2(0, 1)) == ApproxV3(Vector3{0, 0, 1}));
    REQUIRE(UVtoPyramid(Vector2(1, 0)) == ApproxV3(Vector3{1, 0, 0}));
    REQUIRE(UVtoPyramid(Vector2(0, -1)) == ApproxV3(Vector3{0, 0, -1}));
    REQUIRE(UVtoPyramid(Vector2(-1, 0)) == ApproxV3(Vector3{-1, 0, 0}));
    REQUIRE(UVtoPyramid(Vector2(-1, -1)) == ApproxV3(Vector3{-1, 0, -1}.Normalized()));
}

TEST_CASE( "UVtoPyramid<=>PyramidUV", "[Impostor]" ) {

    std::vector<Vector2> uvs {
        Vector2(0.f, -1.f),
        Vector2(-1.f, 0.f),
        Vector2(0.f, 1.f),
        Vector2(1.f, 0.f),
    };

    for (auto el : uvs) {
        Vector3 pyr = UVtoPyramid(el);
        Vector2 uv_ = PyramidUV(pyr);
        Vector3 pyr_ = UVtoPyramid(uv_);

        REQUIRE(el == uv_);
        REQUIRE(pyr == pyr_);
    }
}

TEST_CASE( "getUvCoordByNum", "[Impostor]" ) {
    std::vector<std::tuple<int, int, Vector2>> tst {
        { 0, 8, Vector2(0.f, 0.f) },
        { 1, 8, Vector2(0.f, 1.f/8.f) },
    };

    for (auto el : tst) {
        REQUIRE(getUvCoordByNum(std::get<0>(el), std::get<1>(el))
                == std::get<2>(el));
    }
}


TEST_CASE( "getClosestCoord", "[Impostor]" ) {
    std::vector<std::tuple<float, int, float>> tst {
        { 0.f,                    8, 0.f },
        { .44f,                   8, .5f },
        { .5f,                    8, .5f },
        { 4.f/8.f + 0.49999f/8.f, 8, .5f },
        { 1.f,                    8, 1.f },

        { -0.9f,   2, -1.f },
        { -0.74f,  2, -0.5f },
        { 0.74f,   2,  0.5f },
        { 0.1f,    2,  0.0f },
    };

    for (auto el : tst) {
        REQUIRE(getClosestCoord(std::get<0>(el), std::get<1>(el))
                == std::get<2>(el));
    }
}


// TODO3 actually test this
TEST_CASE( "GetCurrentUVs", "[Impostor]" ) {
    std::vector<std::tuple<Vector2, int>> dt =
        { { {       1.f,        1.f }, 5 },

            { { 1.f / 8.f,  1.f / 8.f }, 5 },
          { {       0.f,        0.f }, 5 },

          { {     .001f,      .001f }, 5 },
    };

    for (auto el : dt) {
        const auto res = GetCurrentUVs(std::get<0>(el), std::get<1>(el));
        REQUIRE(res.offset1.x_ != 2.f);
        auto factors_sum = res.factor1 + res.factor2 + res.factor3 + res.factor4;
        REQUIRE(abs(factors_sum - 1.f) < 0.0001);
    }
}
