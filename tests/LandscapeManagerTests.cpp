// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include <random>

#include <Urho3D/Container/Str.h>
#include <Urho3D/Math/Vector2.h>
#include <Urho3D/Math/Vector3.h>

#include "LandscapeManager.hpp"

using namespace Urho3D;


std::ostream& operator << ( std::ostream& os, IntVector2 const& value ) {
    os << std::string(value.ToString().c_str());
    return os;
}


template<typename T>
T uset_intersection (T& pSet1, T& pSet2) {
    T ret;
    for (auto el1 : pSet1) {
        for (auto el2 : pSet2) {
            if (el1 == el2) {
                ret.insert(el1);
            }
        }
    }

    return ret;
}

template<typename T>
T uset_difference (const T& pSet1, const T& pSet2) {
    T ret = pSet1;
    for (auto el1 : pSet1) {
        for (auto el2 : pSet2) {
            if (el1 == el2) {
                ret.erase(el1);
            }
        }
    }

    return ret;
}


template<typename T>
T uset_sum (const T& pSet1, const T& pSet2) {
    T ret;
    ret.insert(pSet1.begin(), pSet1.end());
    ret.insert(pSet2.begin(), pSet2.end());

    return ret;
}


#include "CatchConfig.hpp"
#include "catch.hpp"



TEST_CASE("UrhoworldsLandscapeManager", "[LandscapeManager]") {
    LandscapeManager mgr1;
    mgr1.setEvenize(false);

    SECTION("initialization") {
        // just check that the size of the 5x5 is 25
        mgr1.setNumTilesToRender(3);
        REQUIRE(mgr1.getNumTilesToLoad() == 25);
    }

    SECTION("initialization") {
        // same but 9x9
        mgr1.setNumTilesToRender(5);
        REQUIRE(mgr1.getNumTilesToLoad() == 81);

        REQUIRE(mgr1.getTileIsNeeded({ 5, -5 }) == LandscapeManager::TileIsNeeded::not_needed);
        REQUIRE(mgr1.getTileIsNeeded({ 4, -5 }) == LandscapeManager::TileIsNeeded::not_needed);
        REQUIRE(mgr1.getTileIsNeeded({ 5, -4 }) == LandscapeManager::TileIsNeeded::not_needed);

        REQUIRE(mgr1.getTileIsNeeded({ 4, -4 }) == LandscapeManager::TileIsNeeded::needed);
        REQUIRE(mgr1.getTileIsNeeded({ 0, 0 }) == LandscapeManager::TileIsNeeded::needed);
    }

    SECTION("initialization") {
        // compare that 3x3 landscape with hand-written array, ignoring the
        // order of elements
        mgr1.setNumTilesToRender(2);
        auto res = mgr1.getAllElementsToLoad();

        // handwritten values for 3x3 landscape
        SetIntVector2 expected_to_have{
            { -1, -1 }, { -1, 0 }, { -1, 1 },
            { 0, -1},   { 0, 0 },  { 0, 1 },
            { 1, -1 },  { 1, 0 },  { 1, 1 },
        };

        REQUIRE(expected_to_have == res);

        // camera angle, because we want to load the visible tiles first
        for (const auto& el : expected_to_have) {
            REQUIRE(mgr1.getTileIsNeeded(el) == LandscapeManager::TileIsNeeded::needed);
        }
    }

    SECTION("loading") {
        // after we fill the landscape
        mgr1.setNumTilesToRender(3);
        std::vector<IntVector2> res;
        // collect the array of positions that we need to render
        for (size_t i = 0; i < 9; ++i) {
            res.push_back(mgr1.getNthTilePositionToLoad(i));
        }

        std::random_device rd;
        std::mt19937 g(rd());

        std::shuffle(res.begin(), res.end(), g);

        // check that after notifying the LandscapeManager about loading of any
        // array, the Manager does not have that element in the list of "to be
        // loaded" ones
        for (size_t i = 0; i < res.size(); ++i) {
            auto last = res.back();

            mgr1.setTileIsLoaded(last);

            res.pop_back();

            REQUIRE(mgr1.getTileIsLoaded(last));
            REQUIRE(!mgr1.getTileIsOughtToBeLoaded(last));

            for (const auto& el : res) {
                REQUIRE(mgr1.getTileIsOughtToBeLoaded(el));
                REQUIRE(mgr1.getTileIsNeeded(el) == LandscapeManager::TileIsNeeded::needed);
            }
        }
    }

    SECTION("now let's try a different camera position") {
        LandscapeManager mgr2;
        mgr2.setEvenize(false);

        mgr1.setNumTilesToRender(5);
        mgr2.setNumTilesToRender(5);

        const Vector3 irrelevant; // direction (second parameter) is not
                                  // relevant here
        const Vector3 offset_3d{ 1.f, 0.f, 2.f };
        const IntVector2 offset_2d{ 1, 2 };

        // transitionint from { 0, 0, 0 } is should effectively be treated as
        // { 0, 0, 1 }, which in 2d is  { 0, 1 }

        mgr2.setCurrentCameraTransform(offset_3d, irrelevant);

        mgr2.reinit();

        auto mgr1_data = mgr1.getAllElementsToLoad();
        auto mgr2_data = mgr2.getAllElementsToLoad();

        decltype(mgr1_data) transformed_data;

        for (const auto& el : mgr1_data) {
            transformed_data.insert(el + offset_2d);
        }

        REQUIRE(mgr2_data == transformed_data);
    }

    SECTION("now let's try more camera positions") {
        mgr1.setNumTilesToRender(5);
        const Vector3 irrelevant; // direction (second parameter) is not
                                  // relevant for this test for now...
        mgr1.setCurrentCameraTransform({0, 0}, irrelevant);

        std::vector<std::pair<Vector3, IntVector2>> dt{
            { {1.f, 0.f, 0.f },
              {1, 0} },
            { {1.f, 0.f, 2.f },
              {1, 2} },
            { {-11.4f, 0.f, -19.6f },
              {-11, -19} },
            { {-112.9f, 0.f, -3.1415926f },
              {-112, -3} },
        };

        for (const auto el : dt) {
            LandscapeManager mgr2;
            mgr2.setEvenize(false);
            mgr2.setNumTilesToRender(5);

            mgr2.setCurrentCameraTransform(el.first, irrelevant);

            mgr2.reinit();

            auto mgr1_data = mgr1.getAllElementsToLoad();
            auto mgr2_data = mgr2.getAllElementsToLoad();

            decltype(mgr1_data) transformed_data;

            for (auto elll : mgr1_data) {
                transformed_data.insert(elll + el.second);
            }

            REQUIRE(transformed_data == mgr2_data);
        }
    }


    SECTION("let's try moving the camera around") {
        const size_t num_tiles = 20;
        LandscapeManager test_subject;

        test_subject.setNumTilesToRender(num_tiles);

        const float irrelevant = 0.f;

        std::vector<Vector3> camera_positions = {
            { 0, irrelevant, 1 },
            { 0, irrelevant, 1},
            { 1, irrelevant, 2},
            { 10, irrelevant, 10},
            { -100, 0, 1},
            { -110, 0, 11},
        };

        // we load everything before moving to next point
        SetIntVector2 prev_positions = test_subject.getAllElementsToLoad();

        for (auto el : prev_positions)
            test_subject.setTileIsLoaded(el);

        REQUIRE(test_subject.getNumTilesToLoad() == 0);

        for (const auto el : camera_positions) {
            LandscapeManager new_mgr;
            new_mgr.setNumTilesToRender(num_tiles);
            new_mgr.setCurrentCameraTransform(el, Vector3());
            new_mgr.reinit();
            test_subject.setCurrentCameraTransform(el, Vector3());

            SetIntVector2 new_new_positions = new_mgr.getAllElementsToLoad();

            SetIntVector2 test_subject_new_positions =
                test_subject.getAllElementsToLoad();

            // test if after we move, all the new tiles that we need to load are
            // within the set of ones we get if we create new manager there
            for (auto ell : test_subject_new_positions) {
                REQUIRE(new_new_positions.count(ell));
            }

            SetIntVector2 actual_ones_to_load = uset_difference(new_new_positions,
                prev_positions);

            actual_ones_to_load = uset_difference(actual_ones_to_load,
                test_subject.getAllLoadedElements());

            REQUIRE(actual_ones_to_load == test_subject_new_positions);

            //= new_new_positions - prev_positions;

            for (auto ell : actual_ones_to_load) {
                //REQUIRE(test_subject.getTileIsLoaded(ell));
                //REQUIRE(!test_subject.getTileIsOughtToBeLoaded(ell));
            }

            //std::uno

            // first test that all the

            prev_positions = test_subject_new_positions;

            for (auto el : prev_positions) {
                test_subject.setTileIsLoaded(el);
            }
        }
    }
}



SetIntVector2 load_some_positions (LandscapeManager &pManager) {
    std::mt19937 mt;
    std::uniform_int_distribution<int> dist(0, 1);


    SetIntVector2 to_load_positions = pManager.getAllElementsToLoad();
    SetIntVector2 not_loaded;

    const size_t need_to_load_tiles_num = pManager.getNumTilesToLoad();
    size_t loaded_tiles_num = 0;

    int i = 0;
    for (auto el : to_load_positions) {
        ++i;
        if (i % 2) {
            pManager.setTileIsLoaded(el);
            ++loaded_tiles_num;
        } else {
            not_loaded.insert(el);
        }
    }

    REQUIRE(pManager.getNumTilesToLoad()
            == need_to_load_tiles_num - loaded_tiles_num);

    return uset_difference(to_load_positions, not_loaded);
}

TEST_CASE("let's try moving the camera around, but don't "
          "load everything on each step",
          "[LandscapeManager]") {
    const size_t num_tiles = 20;
    LandscapeManager test_subject;

    test_subject.setNumTilesToRender(num_tiles);

    const float irrelevant = 0.f;

    std::vector<Vector3> camera_positions = {
                                             { 0, irrelevant, 1 },
                                             { 0, irrelevant, 1},
                                             { 1, irrelevant, 2},
                                             { 10, irrelevant, 10},
                                             { -100, 0, 1},
                                             { -110, 0, 11},
    };

    auto all_loaded_positions = load_some_positions(test_subject);

    SetIntVector2 all_unloaded_positions;

    for (const auto el : camera_positions) {
        LandscapeManager new_mgr;
        new_mgr.setNumTilesToRender(num_tiles);
        new_mgr.setCurrentCameraTransform(el, Vector3());
        new_mgr.reinit();
        test_subject.setCurrentCameraTransform(el, Vector3());

        SetIntVector2 new_positions = new_mgr.getAllElementsToLoad();

        SetIntVector2 test_subject_new_positions =
            test_subject.getAllElementsToLoad();

        SetIntVector2 test_subject_tiles_to_unload =
            test_subject.getAllElementsToUnload();

        SetIntVector2 expected_tiles_to_load = uset_difference(new_positions,
                                                               all_loaded_positions);


        REQUIRE(expected_tiles_to_load == test_subject_new_positions);

        SetIntVector2 expected_tiles_to_unload =
            uset_difference(uset_difference(all_loaded_positions,
                                            all_unloaded_positions),
                            new_positions);

        all_loaded_positions = uset_sum(all_loaded_positions,
                                        load_some_positions(test_subject));


        REQUIRE(expected_tiles_to_unload == test_subject_tiles_to_unload);

        all_unloaded_positions = uset_sum(all_unloaded_positions,
                                          expected_tiles_to_unload);
        test_subject.clearTilesToUnload();
    }
}

// TODO excludes are not tested
