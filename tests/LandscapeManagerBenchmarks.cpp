// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include <functional>
#include <chrono>
#include <numeric>
#include <random>
#include <iostream>
#include <algorithm>


#include "LandscapeManager.hpp"


unsigned long time_to_run (std::function<void()> func,
                           int num_times) {
    auto start = std::chrono::system_clock::now();

    for (int i = 0;  i < num_times; ++i) {
        func();
    }
    // record end time
    auto end = std::chrono::system_clock::now();
    auto diff = std::chrono::duration_cast<std::chrono::duration<unsigned long, std::micro>>(end-start);
    //auto diff = end - start;

    return diff.count();
}

int do_some_landscape_work (int pTilesNum) {
    LandscapeManager lscp(pTilesNum);

    for (int i = 0; i < 25; ++i) {
        lscp.setCurrentCameraTransform(Urho3D::Vector3(float(i), 0, float(i)), Urho3D::Vector3());
    }

    return int(lscp.getNumTilesToLoad());
}

void do_the_stuff_bkp() {
    double total = 0.;
    for (int i = 0; i < 5; ++i) {
        auto time = time_to_run([&]() { do_some_landscape_work(50); }, 1);
        total += double(time);
    }

    std::cout << std::fixed << "\t" << total / 5. << std::endl;
}

void do_the_stuff() {
    for (int i = 0; i < 1000; ++i) {
        auto time = time_to_run([&]() { do_some_landscape_work(i); }, 1);
        std::cout << std::fixed << time << std::endl;
    }

}



int main () {
    std::mt19937 mt;
    auto tm = std::chrono::system_clock::now().time_since_epoch();
    int seed = int(std::chrono::duration_cast<std::chrono::seconds>(tm).count());
    mt.seed(seed);

    std::uniform_int_distribution<int> dist(0, 1231);

    std::cout << "pTilesNum\ttime\n";

    do_the_stuff();

    return 0;
}
