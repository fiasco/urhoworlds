// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include "CatchConfig.hpp"
#include "catch.hpp"


#include "LandscapeGenerator.hpp"


TEST_CASE("SimpleVector", "[SimpleVector]") {
    SimpleVector<int> empty;
    SimpleVector<int> ten(10);

    SECTION("initial state") {
        REQUIRE(empty.size() == 0);
        REQUIRE(empty.capacity() == 0);
        REQUIRE_THROWS_AS(empty[0], std::out_of_range);
        REQUIRE_THROWS_AS(empty[100], std::out_of_range);
        REQUIRE_THROWS_AS(empty[10000000], std::out_of_range);
        REQUIRE(empty.data() == nullptr);

        REQUIRE(ten.size() == 0);
        REQUIRE(ten.capacity() == 10);
        REQUIRE(ten.data() != nullptr);
    }

    SECTION("adding elements to empty") {
        for (size_t i = 0; i < 2048; ++i) {
            REQUIRE(empty.size() == i);
            REQUIRE(empty.capacity() >= i);

            REQUIRE(ten.size() == i);
            REQUIRE(ten.capacity() >= i);

            empty.push_back(int(i));
            ten.push_back(int(i));

            REQUIRE(empty.size() == i + 1);
            REQUIRE(empty.capacity() >= i + 1);

            REQUIRE(ten.size() == i + 1);
            REQUIRE(ten.capacity() >= i + 1);
        }

        for (size_t i = 0; i < 2048; ++i) {
            REQUIRE(empty[i] == int(i));
            REQUIRE(ten[i] == int(i));
        }
    }


}
